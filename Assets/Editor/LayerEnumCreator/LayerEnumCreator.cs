﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class LayerEnumCreator
{
    private const string    COMMAND_NAME = "Tools/Create/LayerEnum/Create";

    /// <summary>
    /// エクスポート先フォルダ
    /// </summary>
    private const string    c_DefaultExportPath = "Assets/Scripts/Enum/";

    private const string c_ScriptName = "LayerEnum.cs";

    private const string    TAB = "    ";

    //  ファイル名（拡張子あり、無し）
    private static readonly string  FILENAME_WITHOUT_EXTENSION = Path.GetFileNameWithoutExtension(c_DefaultExportPath + c_ScriptName);

    /// <summary>
    /// オーディオのファイル名を定数で管理するクラスを作成
    /// </summary>
    [MenuItem(COMMAND_NAME)]
    public static void     Create()
    {
        if (!canCreate())   return;

        Init();
    }

    static void    Init()
    {
        CreateScript();
    }

    /// <summary>
    /// スクリプトを作成する
    /// </summary>
    public static void CreateScript()
    {
        StringBuilder a_Builder = new StringBuilder();

        a_Builder.AppendLine("using UnityEngine;");

        a_Builder.AppendLine("\t");

        a_Builder.AppendLine("/// <summary>");
        a_Builder.AppendLine("/// レイヤーを定数で管理するクラス");
        a_Builder.AppendLine("/// <summary>");
        a_Builder.AppendFormat("public class {0}", FILENAME_WITHOUT_EXTENSION).AppendLine();

        a_Builder.AppendLine("{");

//  enumの定義
        a_Builder.AppendLine("\t");

        a_Builder.AppendLine(TAB + "public enum eLayer");
        a_Builder.AppendLine(TAB + "{");

        a_Builder.AppendLine(TAB + TAB + "Dummy = -1,");

        a_Builder.AppendLine("\t");

        foreach (string a_LayerName in InternalEditorUtility.layers)
        {
            a_Builder.AppendLine(TAB + TAB + AllSpaceRemove(a_LayerName) + ",");
        }

        a_Builder.AppendLine(TAB + TAB + "Max,");

        a_Builder.AppendLine(TAB + "}");
//  enumの定義 end

        a_Builder.AppendLine("\t");

//  関数の定義
        a_Builder.AppendLine(TAB + "public static int  GetLayerNum(eLayer p_Layer, bool p_bForCullingMask)");
        a_Builder.AppendLine(TAB + "{");

        a_Builder.AppendLine(TAB + TAB + "if (p_Layer == eLayer.Dummy || p_Layer == eLayer.Max)     return -1;");

        a_Builder.AppendLine(TAB + TAB + "if(p_bForCullingMask)");
        a_Builder.AppendLine(TAB + TAB + "{");
        a_Builder.AppendLine(TAB + TAB + TAB + "return 1 << LayerMask.NameToLayer(p_Layer.ToString());");
        a_Builder.AppendLine(TAB + TAB + "}");
        a_Builder.AppendLine(TAB + TAB + "else");
        a_Builder.AppendLine(TAB + TAB + "{");
        a_Builder.AppendLine(TAB + TAB + TAB + "return  LayerMask.NameToLayer(p_Layer.ToString());");
        a_Builder.AppendLine(TAB + TAB + "}");

        a_Builder.AppendLine(TAB + "}");
//  関数の定義 end

        a_Builder.AppendLine("}");

        string a_ExportPath = c_DefaultExportPath + c_ScriptName;

        string a_DirectoryName = Path.GetDirectoryName(a_ExportPath);
        if(!Directory.Exists(a_DirectoryName))
        {
            Directory.CreateDirectory(a_DirectoryName);
        }

        File.WriteAllText(a_ExportPath, a_Builder.ToString(), Encoding.UTF8);
        AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
    }

    static string   AllSpaceRemove(string p_Str)
    {
        string a_NewStr = "";
        foreach(char a_Char in p_Str)
        {
            if(a_Char != ' ')
            {
                a_NewStr += a_Char;
            }
        }

        return a_NewStr;
    }

    [MenuItem(COMMAND_NAME, true)]
    private static bool canCreate()
    {
        return !EditorApplication.isPlaying && !Application.isPlaying && !EditorApplication.isCompiling;
    }
}
