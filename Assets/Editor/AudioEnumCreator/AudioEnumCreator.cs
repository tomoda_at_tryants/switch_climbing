﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class AudioEnumCreator : EditorWindow
{
    private const string    COMMAND_NAME = "Tools/Create/AudioEnum/Create";

    /// <summary>
    /// エクスポート先フォルダ
    /// </summary>
    private const string    c_DefaultExportPath = "Assets/Scripts/Enum/";

    private const string c_ScriptName = "AudioEnum.cs";

    /// <summary>
    /// BGMデータの参照先
    /// </summary>
    private const string    c_DefaultBgmPath   = "Sounds/BGM";
    /// <summary>
    /// SEデータの参照先
    /// </summary>
    private const string    c_DefaultSePath    = "Sounds/SE";

    private const string    TAB = "    ";

    //  ファイル名（拡張子あり、無し）
    private static readonly string  FILENAME_WITHOUT_EXTENSION = Path.GetFileNameWithoutExtension(c_DefaultExportPath + c_ScriptName);

    /// <summary>
    /// オーディオのファイル名を定数で管理するクラスを作成
    /// </summary>
    [MenuItem(COMMAND_NAME)]
    public static void     Create()
    {
        if (!canCreate())   return;

        Init();

        EditorWindow    a_Window = EditorWindow.GetWindow(typeof(AudioEnumCreator));
        a_Window.maxSize = new Vector2(640, 640);

//        EditorUtility.DisplayDialog(FILENAME, "作成が完了しました", "OK");
    }

    static string   m_InputBGMPath;
    static string   m_InputSEPath;
    static string   m_ExportPath;
    static void    Init()
    {
        m_InputBGMPath  = c_DefaultBgmPath;
        m_InputSEPath   = c_DefaultSePath;
        m_ExportPath    = c_DefaultExportPath;
    }

    void OnGUI()
    {
        string  a_ResourcesPath = "Assets/Resources/";

        EditorGUILayout.BeginVertical();
        {
            GUILayout.Space(20);

            EditorGUILayout.LabelField("BGMのパス", EditorStyles.boldLabel);
            EditorGUILayout.TextField(a_ResourcesPath, m_InputBGMPath, GUILayout.Width(500));

            GUILayout.Space(20);

            EditorGUILayout.LabelField("SEのパス", EditorStyles.boldLabel);
            EditorGUILayout.TextField(a_ResourcesPath, m_InputSEPath, GUILayout.Width(500));

            GUILayout.Space(20);

            EditorGUILayout.LabelField("エクスポート先パス", EditorStyles.boldLabel);
            EditorGUILayout.TextField("", m_ExportPath, GUILayout.Width(500));

            GUILayout.Space(20);
            EditorGUILayout.LabelField("スクリプト名：" + c_ScriptName);

            GUILayout.Space(50);

            if(GUILayout.Button("生成", GUILayout.Width(240), GUILayout.Height(40)))
            {
                CreateScript();
            }
        }
        EditorGUILayout.EndVertical();
    }

    /// <summary>
    /// スクリプトを作成する
    /// </summary>
    public static void CreateScript()
    {
        StringBuilder a_Builder = new StringBuilder();

        a_Builder.AppendLine("using System.Collections.Generic;");

        a_Builder.AppendLine("\t");

        a_Builder.AppendLine("/// <summary>");
        a_Builder.AppendLine("/// オーディオ名を定数で管理するクラス");
        a_Builder.AppendLine("/// <summary>");
        a_Builder.AppendFormat("public class {0}", FILENAME_WITHOUT_EXTENSION).AppendLine();

        a_Builder.AppendLine("{");

        //  指定したパスのリソースを全て取得
        object[]    a_BgmList = Resources.LoadAll(m_InputBGMPath);
        //  指定したパスのリソースを全て取得
        object[]    a_SeList = Resources.LoadAll(m_InputSEPath);

//  enumの定義
    //  BGM
        a_Builder.AppendLine("\t");

        a_Builder.AppendLine(TAB + "public enum eBGM");
        a_Builder.AppendLine(TAB + "{");

        a_Builder.AppendLine(TAB + TAB + "Dummy = -1,");

        a_Builder.AppendLine("\t");

        foreach (AudioClip a_BGM in a_BgmList)
        {
            a_Builder.AppendLine(TAB + TAB + a_BGM.name + ",");
        }

        a_Builder.AppendLine(TAB + TAB + "Max,");

        a_Builder.AppendLine(TAB + "}");
    //  BGM

        a_Builder.AppendLine("\t");

    //  SE
        a_Builder.AppendLine(TAB + "public enum eSE");
        a_Builder.AppendLine(TAB + "{");

        a_Builder.AppendLine(TAB + TAB + "Dummy = -1,");

        a_Builder.AppendLine("\t");

        foreach (AudioClip a_SE in a_SeList)
        {
            a_Builder.AppendLine(TAB + TAB + a_SE.name + ",");
        }

        a_Builder.AppendLine(TAB + TAB + "Max,");

        a_Builder.AppendLine(TAB + "}");

        a_Builder.AppendLine("\t");

        a_Builder.AppendLine("}");
    //  SE
//

        string  a_ExportPath = c_DefaultExportPath + c_ScriptName;

        string a_DirectoryName = Path.GetDirectoryName(a_ExportPath);
        if(!Directory.Exists(a_DirectoryName))
        {
            Directory.CreateDirectory(a_DirectoryName);
        }

        File.WriteAllText(a_ExportPath, a_Builder.ToString(), Encoding.UTF8);
        AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
    }

    [MenuItem(COMMAND_NAME, true)]
    private static bool canCreate()
    {
        return !EditorApplication.isPlaying && !Application.isPlaying && !EditorApplication.isCompiling;
    }
}
