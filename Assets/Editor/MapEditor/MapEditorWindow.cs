﻿using UnityEditor;
using UnityEngine;

public class MapEditorWindow : EditorWindow
{
    private void OnEnable()
    {
        MapEditor.Init();
    }

    private void Update()
    {
    }

    Vector2 m_ScrollPos_SelectObject = Vector2.zero;
    Vector2 m_ScrollPos_CreateObjectType = Vector2.zero;
    Vector2 m_ScrollPos_Total = Vector2.zero;

    int m_nSearchID = -1;

    private void OnGUI()
    {
        if (MapEditor.mapManager == null) return;

        if (EditorApplication.isPlaying || Application.isPlaying)
        {
            m_ScrollPos_Total = GUILayout.BeginScrollView(m_ScrollPos_Total, GUI.skin.box);
            {
                GUILayout.Space(10);

                //  ステージ情報
                {
                    GUILayout.Label("●ステージ情報");

                    GUILayout.BeginVertical(GUI.skin.box, GUILayout.Width(200));
                    {
                        //  ステージ番号
                        int a_nStageNum = MapEditor.mapManager.stageNum;
                        GUILayout.Label(string.Format(" ・stage_{0}", a_nStageNum.ToString().PadLeft(3, '0')));

                        //  width
                        GUILayout.BeginHorizontal(GUILayout.ExpandWidth(false));
                        {
                            GUILayout.Label(string.Format(" ・width：{0}", (int)MapEditor.mapManager.mapSize.width));

                            if(GUILayout.Button("＋"))
                            {
                                MapEditor.mapManager.mapSize.width++;
                            }
                            if (GUILayout.Button("－"))
                            {
                                MapEditor.mapManager.mapSize.width--;
                            }
                        }
                        GUILayout.EndHorizontal();

                        //  height
                        GUILayout.BeginHorizontal(GUILayout.ExpandWidth(false));
                        {
                            GUILayout.Label(string.Format(" ・height：{0}", (int)MapEditor.mapManager.mapSize.height));

                            if (GUILayout.Button("＋"))
                            {
                                MapEditor.mapManager.mapSize.height++;
                            }
                            if (GUILayout.Button("－"))
                            {
                                MapEditor.mapManager.mapSize.height--;
                            }
                        }
                        GUILayout.EndHorizontal();

                    }
                    GUILayout.EndVertical();
                }

                GUILayout.Space(20);

                //  オブジェクト指定用トグル
                {
                    GUILayout.Label("●作成するオブジェクトのタイプ");

                    if(MapEditor.mapManager.createType != (int)CatchObjectData.eType.None)
                    {
                        CatchObjectData a_Data = new CatchObjectData();
                        a_Data.type = (CatchObject.eType)MapEditor.mapManager.createType;
                        CatchObject a_CatchObj = CatchObject.Create(a_Data, null);
                        GUILayout.Box(a_CatchObj.image.mainTexture, GUILayout.Width(100), GUILayout.Height(100));

                        Destroy(a_CatchObj.gameObject);
                    }

                    //  スクロールビュー
                    m_ScrollPos_CreateObjectType = GUILayout.BeginScrollView(m_ScrollPos_CreateObjectType, GUI.skin.box, GUILayout.Width(150), GUILayout.Height(120));
                    {
                        string[] a_Caption = new string[(int)CatchObject.eType.Max];
                        for (int i = 0; i < (int)CatchObject.eType.Max; i++)
                        {
                            a_Caption[i] = ((CatchObject.eType)i).ToString();
                        }
                        MapEditor.mapManager.createType = GUILayout.SelectionGrid(MapEditor.mapManager.createType, a_Caption, 1, GUI.skin.toggle);
                    }
                    GUILayout.EndScrollView();
                }

                GUILayout.Space(20);

                //  選択されているオブジェクト数表示
                {
                    GUILayout.Label("●選択オブジェクト");

                    GUILayout.BeginVertical(GUI.skin.box);
                    {
                        GUILayout.Space(10);

                        //  ID検索
                        GUILayout.BeginHorizontal(GUI.skin.box);
                        {
                            //  IDの入力
                            GUILayout.Label("Search ID:", GUILayout.ExpandWidth(false));

                            m_nSearchID = EditorGUILayout.IntField(m_nSearchID, GUILayout.Width(40));   //  id

                            string a_HelpMsg = "";
                            MessageType a_MsgType = MessageType.None;

                            if (GUILayout.Button("検索", GUILayout.ExpandWidth(false)))
                            {
                                bool a_bNotFound = true;
                                foreach (CatchObject a_CheckObj in MapEditor.mapManager.catchObjMan.objList)
                                {
                                    //  検索IDと一致した
                                    if (m_nSearchID == a_CheckObj.id)
                                    {
                                        a_bNotFound = false;

                                        bool a_bAlready = false;
                                        foreach (CatchObject a_SelectObj in MapEditor.selectCatchObjList)
                                        {
                                            if (a_CheckObj.id == a_SelectObj.id)
                                            {
                                                a_bAlready = true;

                                                a_HelpMsg = "既に選択されています";
                                                a_MsgType = MessageType.Warning;

                                                break;
                                            }
                                        }

                                        if (!a_bAlready)
                                        {
                                            MapManager.getSelectObjectCallback(a_CheckObj);
                                            break;
                                        }
                                    }
                                }

                                if (a_bNotFound)
                                {
                                    a_HelpMsg = "指定したIDのオブジェクトは存在しません";
                                    a_MsgType = MessageType.Warning;
                                }
                            }

                            if (!string.IsNullOrEmpty(a_HelpMsg) && a_MsgType != MessageType.None)
                            {
                                EditorGUILayout.HelpBox(a_HelpMsg, a_MsgType);
                            }
                        }
                        GUILayout.EndHorizontal();

                        GUILayout.Space(10);

                        //  スクロールビュー
                        m_ScrollPos_SelectObject = GUILayout.BeginScrollView(m_ScrollPos_SelectObject, GUI.skin.scrollView);
                        {
                            CatchObject a_DestroyObj = null;

                            //  選択されているオブジェクトを全て表示
                            foreach(CatchObject a_CatchObject in MapEditor.selectCatchObjList)
                            {
                                if (!a_CatchObject.select) continue;

                                if (GUILayout.Button("×", GUILayout.ExpandWidth(false)))
                                {
                                    MapManager.getSelectObjectCallback(a_CatchObject);
                                }

                                GUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));
                                {
                                    string a_HelpMsg = "";
                                    MessageType a_MsgType = MessageType.None;

                                    GUILayout.BeginHorizontal(GUI.skin.box, GUILayout.ExpandWidth(false));
                                    {
                                        //  IDの入力
                                        GUILayout.Label("ID:", GUILayout.Width(30));

                                        GUILayout.Space(-10);

                                        //  ID
                                        {
                                            int a_nID = a_CatchObject.id;
                                            a_nID = EditorGUILayout.IntField(a_nID, GUILayout.Width(40));   //  id

                                            bool a_bExist = AlreadyExist_CatchObj_ID(a_CatchObject);
                                            if (a_bExist)
                                            {
                                                a_HelpMsg = "指定したIDは既に存在します";
                                                a_MsgType = MessageType.Warning;
                                            }

                                            a_CatchObject.id = a_nID;
                                        }

                                        GUILayout.Space(30);

                                        //  選択ボタン
                                        if (GUILayout.Button("Select", GUILayout.ExpandWidth(false)))
                                        {
                                            SelectOne(a_CatchObject);
                                        }

                                        GUILayout.Space(10);

                                        //  削除ボタン
                                        if (GUILayout.Button("Delete（完全に削除）", GUILayout.ExpandWidth(false)))
                                        {
                                            a_DestroyObj = a_CatchObject;
                                        }

                                        //  カメラをオブジェクトまで移動する
                                        if (GUILayout.Button("MoveCamera", GUILayout.ExpandWidth(false)))
                                        {
                                            Transform a_MapCamera = MapEditor.mapManager.canvasCamera.transform;
                                            if (!a_MapCamera.GetComponent<iTween>())
                                            {
                                                float   a_fDistance = Vector2.Distance(a_MapCamera.localPosition, a_CatchObject.transform.localPosition);
                                                iTween.MoveTo(a_MapCamera.gameObject, iTween.Hash("x", a_CatchObject.transform.localPosition.x, "y", a_CatchObject.transform.localPosition.y,
                                                              "islocal", true, "time", a_fDistance / 2000f, "easetype", iTween.EaseType.easeOutQuad));
                                            }
                                        }
                                    }
                                    GUILayout.EndHorizontal();

                                    //  ヘルプメッセージ表示
                                    if (!string.IsNullOrEmpty(a_HelpMsg) && a_MsgType != MessageType.None)
                                    {
                                        EditorGUILayout.HelpBox(a_HelpMsg, a_MsgType);
                                    }
                                }
                                GUILayout.EndVertical();

                                GUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));
                                {
#if false
                                    //  PreviousID_Right
                                    GUILayout.BeginHorizontal(GUILayout.Width(200));
                                    {
                                        GUILayout.Label("defaultScale　");      //  nextID_Left

                                        GUILayout.Label("x:");
                                        Vector2 a_DefaultScale = a_CatchObject.defaultScale;
                                        string a_DefaultScale_X = GUILayout.TextField(a_DefaultScale.x.ToString(), GUILayout.Width(60));
                                        if (float.TryParse(a_DefaultScale_X, out a_DefaultScale.x) && a_DefaultScale.x > 0)
                                        {
                                            a_CatchObject.defaultScale = a_DefaultScale;
                                        }

                                        GUILayout.Label("y:");
                                        string a_DefaultScale_Y = GUILayout.TextField(a_DefaultScale.y.ToString(), GUILayout.Width(60));
                                        if (float.TryParse(a_DefaultScale_Y, out a_DefaultScale.y) && a_DefaultScale.y > 0)
                                        {
                                            a_CatchObject.defaultScale = a_DefaultScale;
                                        }
                                    }
                                    GUILayout.EndHorizontal();

                                    //  PreviousID_Right
                                    GUILayout.BeginHorizontal(GUILayout.Width(200));
                                    {
                                        GUILayout.Label("defaultPos　");      //  nextID_Left

                                        GUILayout.Label("x:");
                                        Vector2 a_DefaultPos = a_CatchObject.defaultPos;
                                        string a_DefaultPos_X = GUILayout.TextField(a_DefaultPos.x.ToString(), GUILayout.Width(60));
                                        if (float.TryParse(a_DefaultPos_X, out a_DefaultPos.x) && a_DefaultPos.x > 0)
                                        {
                                            a_CatchObject.defaultPos = a_DefaultPos;
                                        }

                                        GUILayout.Label("y:");
                                        string a_DefaultPos_Y = GUILayout.TextField(a_DefaultPos.y.ToString(), GUILayout.Width(60));
                                        if (float.TryParse(a_DefaultPos_Y, out a_DefaultPos.y) && a_DefaultPos.y > 0)
                                        {
                                            a_CatchObject.defaultPos = a_DefaultPos;
                                        }
                                    }
                                    GUILayout.EndHorizontal();
#endif

                                    GUILayout.Label("type:" + a_CatchObject.type.ToString());   //  type

                                    //  defaultPos
                                    GUILayout.BeginHorizontal(GUI.skin.box);
                                    {
                                        Vector2 a_Pos = a_CatchObject.transform.localPosition;
                                        a_Pos = EditorGUILayout.Vector2Field("defaultPos", a_Pos, GUILayout.Width(200));

                                        a_CatchObject.transform.localPosition = a_Pos;
                                    }
                                    GUILayout.EndHorizontal();

                                    GUILayout.Space(5);

                                    //  defaultScale
                                    GUILayout.BeginHorizontal(GUI.skin.box);
                                    {
                                        Vector2 a_Scale = a_CatchObject.transform.localScale;
                                        a_Scale = EditorGUILayout.Vector2Field("defaultScale", a_Scale, GUILayout.Width(200));

                                        a_CatchObject.transform.localScale = a_Scale;
                                    }
                                    GUILayout.EndHorizontal();

                                    GUILayout.Space(5);

                                    //  defaultAngle
                                    GUILayout.BeginHorizontal(GUI.skin.box);
                                    {
                                        Vector3 a_Angle = a_CatchObject.transform.localEulerAngles;
                                        a_Angle = EditorGUILayout.Vector3Field("defaultAngle", a_Angle, GUILayout.Width(300));

                                        a_CatchObject.transform.localEulerAngles = a_Angle;
                                    }
                                    GUILayout.EndHorizontal();

                                    GUILayout.Space(5);

                                    //  startToCatch
                                    GUILayout.BeginVertical();
                                    {
                                        bool    a_bStartToCatch     = a_CatchObject.startToCatch;
                                        a_bStartToCatch = EditorGUILayout.Toggle("startToCatch:", a_bStartToCatch, GUILayout.ExpandWidth(false));

                                        a_CatchObject.startToCatch = a_bStartToCatch;

                                        string a_IDStr = "";
                                        //  複数ゴールが設定されていたら、文字列として取得しておく
                                        foreach (CatchObject a_CheckObj in MapEditor.mapManager.catchObjMan.objList)
                                        {
                                            if ((a_CatchObject.startToCatch && a_CheckObj.startToCatch) && (a_CatchObject.id != a_CheckObj.id))
                                            {
                                                a_IDStr += "ID:" + a_CheckObj.id.ToString() + ", ";

                                            }
                                        }
                                        //  複数選択されていた時はメッセージを表示
                                        if (!string.IsNullOrEmpty(a_IDStr))
                                        {
                                            EditorGUILayout.HelpBox("スタート時に掴まるオブジェクトが複数設定されています　" + a_IDStr, MessageType.Warning, true);
                                        }
                                    }
                                    GUILayout.EndVertical();

                                    //  goal
                                    GUILayout.BeginVertical();
                                    {
                                        bool a_bGoal = a_CatchObject.goal;
                                        a_bGoal = EditorGUILayout.Toggle("goal:", a_bGoal, GUILayout.ExpandWidth(false));

                                        a_CatchObject.goal = a_bGoal;

                                        string a_IDStr = "";
                                        //  複数ゴールが設定されていたら、文字列として取得しておく
                                        foreach(CatchObject a_CheckObj in MapEditor.mapManager.catchObjMan.objList)
                                        {
                                            if((a_CatchObject.goal && a_CheckObj.goal) && (a_CatchObject.id != a_CheckObj.id))
                                            {
                                                a_IDStr += "ID:" + a_CheckObj.id.ToString() + ", ";
                                                
                                            }
                                        }
                                        //  複数選択されていた時はメッセージを表示
                                        if(!string.IsNullOrEmpty(a_IDStr))
                                        {
                                            EditorGUILayout.HelpBox("ゴールが複数設定されています　" + a_IDStr, MessageType.Warning, true);
                                        }
                                    }
                                    GUILayout.EndVertical();

                                    //  PreviousID_Left
                                    GUILayout.BeginHorizontal(GUILayout.Width(150));
                                    {
                                        int a_nPreviousID_Left = a_CatchObject.dstID.l_Previous;
                                        a_nPreviousID_Left = EditorGUILayout.IntField("previousID_Left:", a_nPreviousID_Left, GUILayout.ExpandWidth(false));

                                        a_CatchObject.SetID(a_nPreviousID_Left, Switch_def.eHandle.Left, false);

                                        //  指定したIDのオブジェクトを一定時間ターゲット状態にするボタンを作成
                                        SetButton_View(a_nPreviousID_Left);
                                    }
                                    GUILayout.EndHorizontal();

                                    //  PreviousID_Right
                                    GUILayout.BeginHorizontal(GUILayout.Width(150));
                                    {
                                        int a_nPreviousID_Right = a_CatchObject.dstID.r_Previous;
                                        a_nPreviousID_Right = EditorGUILayout.IntField("previousID_Right:", a_nPreviousID_Right, GUILayout.ExpandWidth(false));

                                        a_CatchObject.SetID(a_nPreviousID_Right, Switch_def.eHandle.Right, false);

                                        //  指定したIDのオブジェクトを一定時間ターゲット状態にするボタンを作成
                                        SetButton_View(a_nPreviousID_Right);
                                    }
                                    GUILayout.EndHorizontal();

                                    //  NextID_Left
                                    GUILayout.BeginHorizontal(GUILayout.Width(150));
                                    {
                                        int a_nNextID_Left = a_CatchObject.dstID.l_next;
                                        a_nNextID_Left = EditorGUILayout.IntField("nextID_Left:", a_nNextID_Left, GUILayout.ExpandWidth(false));

                                        a_CatchObject.SetID(a_nNextID_Left, Switch_def.eHandle.Left, true);

                                        //  指定したIDのオブジェクトを一定時間ターゲット状態にするボタンを作成
                                        SetButton_View(a_nNextID_Left);
                                    }
                                    GUILayout.EndHorizontal();

                                    //  PreviousID_Right
                                    GUILayout.BeginHorizontal(GUILayout.Width(150));
                                    {
                                        int a_nNextID_Right = a_CatchObject.dstID.r_next;
                                        a_nNextID_Right = EditorGUILayout.IntField("nextID_Right:", a_nNextID_Right, GUILayout.ExpandWidth(false));

                                        a_CatchObject.SetID(a_nNextID_Right, Switch_def.eHandle.Right, true);

                                        //  指定したIDのオブジェクトを一定時間ターゲット状態にするボタンを作成
                                        SetButton_View(a_nNextID_Right);
                                    }
                                    GUILayout.EndHorizontal();
                                }
                                GUILayout.EndVertical();

                                GUILayout.Space(20);
                            }

                            //  オブジェクトを削除する
                            if (a_DestroyObj != null)
                            {
                                if(MapManager.destroyObjectCallback != null)
                                {
                                    MapManager.destroyObjectCallback(a_DestroyObj);
                                }

                                MapEditor.selectCatchObjList.Remove(a_DestroyObj);
                                Destroy(a_DestroyObj.gameObject);
                            }
                        }
                        GUILayout.EndScrollView();
                    }
                    GUILayout.EndVertical();
                }

                if(GUILayout.Button("Save", GUILayout.Height(40)))
                {
                    //  マップサイズを保存
                    MapEditor.mapManager.mapSize.SaveData(MapEditor.mapManager.stageNum);
                    //  ステージデータを保存
                    MapEditor.mapManager.SaveStageData();
                }
            }
            GUILayout.EndScrollView();
        }
    }

    static void    SetButton_View(int p_nID)
    {
        if (p_nID < 0) return;

        if (GUILayout.Button("View", GUILayout.ExpandWidth(false)))
        {
            foreach (CatchObject a_Obj in MapEditor.mapManager.catchObjMan.objList)
            {
                if (a_Obj.id == p_nID)
                {
                    a_Obj.TargetPosition_On(1f, true);  //  1秒間ターゲット状態にする
                }
            }
        }
    }

    /// <summary>
    /// 選択したオブジェクトのターゲットを表示する
    /// </summary>
    /// <param name="p_SelectObj"></param>
    public void SelectOne(CatchObject p_SelectObj)
    {
        foreach (CatchObject a_CatchObject in MapEditor.selectCatchObjList)
        {
            if(p_SelectObj == a_CatchObject)
            {
                if(!p_SelectObj.isTargetOn)
                {
                    a_CatchObject.TargetOn();
                }
                else
                {
                    a_CatchObject.TargetOff();
                }
                
            }
            else
            {
                a_CatchObject.TargetOff();
            }
        }
    }

    /// <summary>
    /// 渡したIDのオブジェクトが既に存在するか調べる
    /// </summary>
    public bool AlreadyExist_CatchObj_ID(CatchObject p_TargetObj)
    {
        if (p_TargetObj.id == 0 || p_TargetObj.id < 0)    return false;

        bool a_bExist = false;
        foreach(CatchObject a_CatchObj in MapEditor.mapManager.catchObjMan.objList)
        {
            if (p_TargetObj != a_CatchObj && p_TargetObj.id == a_CatchObj.id)
            {
                a_bExist = true;
                break;
            }
        }

        return  a_bExist;
    }
}
