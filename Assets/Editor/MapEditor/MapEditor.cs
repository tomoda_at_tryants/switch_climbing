﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class MapEditor
{
    static List<CatchObject> s_SelectCatchObjList;
    public static List<CatchObject> selectCatchObjList
    {
        get { return s_SelectCatchObjList; }
        set { s_SelectCatchObjList = value; }
    }

    static CatchObjectData s_CatchObjParam;
    public static CatchObjectData catchObjParam
    {
        get { return s_CatchObjParam; }
        set { s_CatchObjParam = value; }
    }

    static MapManager s_MapManager;
    public static MapManager mapManager
    {
        get { return s_MapManager; }
        set { s_MapManager = value; }
    }

    //メニューからウィンドウを表示
    [MenuItem("Tools/Open/MapEditorWindow")]
    public static void Open()
    {
        MapEditorWindow.GetWindow(typeof(MapEditorWindow));
    }

    public static void  Init()
    {
        s_SelectCatchObjList = new List<CatchObject>();

        s_CatchObjParam = new CatchObject();

        //  コールバックを設定
        MapManager.getMapManagerCallback += SetMapManager;
        MapManager.getSelectObjectCallback += SelectObjectCallback;
    }

    static void SetMapManager(MapManager p_MapManager)
    {
        if(s_MapManager == null)
        {
            s_MapManager = p_MapManager;
        }
    }

    /// <summary>
    /// 選択された時に呼ばれるコールバック
    /// </summary>
    /// <param name="p_SelectObj"></param>
    static void SelectObjectCallback(BaseObjectData p_SelectObj)
    {
        if (!EditorApplication.isPlaying || !Application.isPlaying)
        {
            return;
        }

        p_SelectObj.select ^= true;

        Image a_Image = null;
        if(p_SelectObj.GetComponent<CatchObject>())
        {
            a_Image = p_SelectObj.GetComponent<CatchObject>().image;
        }
        else
        if(p_SelectObj.GetComponent<StageObjectBase>())
        {
            a_Image = p_SelectObj.GetComponent<StageObjectBase>().image;
        }

        GameObject  a_Target = a_Image.gameObject;
        if (p_SelectObj.select)
        {
            iTween.ScaleTo(a_Target, iTween.Hash("x", 1.3f, "y", 1.3f, "time", 0.75f, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.linear));

            if(p_SelectObj.GetComponent<CatchObject>())
            {
                CatchObject a_CatchObj = p_SelectObj.GetComponent<CatchObject>();

                //  リストの先頭に追加
                List<CatchObject> a_NewList = new List<CatchObject>();
                a_NewList.Add(a_CatchObj);
                foreach (CatchObject a_Obj in s_SelectCatchObjList)
                {
                    if (a_Obj != p_SelectObj)
                    {
                        a_NewList.Add(a_Obj);
                    }
                }

                s_SelectCatchObjList = a_NewList;
            }
        }
        else
        {
            iTween.Stop(a_Target);
            a_Target.transform.localScale = Vector3.one;

            p_SelectObj.TargetOff();

            List<CatchObject> a_NewList = new List<CatchObject>();
            foreach(CatchObject a_CatchObj in s_SelectCatchObjList)
            {
                if(a_CatchObj != p_SelectObj)
                {
                    a_NewList.Add(a_CatchObj);
                }
            }

            s_SelectCatchObjList = a_NewList;
        }
    }

    public static void  SetParam()
    {
        for(int i = s_SelectCatchObjList.Count - 1 ; i >= 0; i--)
        {
            CatchObject a_CatchObj      = s_SelectCatchObjList[i];

/*
            a_CatchObj.enableCollider   = s_TileDataParam.enableCollider;
            a_CatchObj.initialPosition  = s_TileDataParam.initialPosition;
            a_CatchObj.brokenPoint      = s_TileDataParam.brokenPoint;
            a_CatchObj.goal             = s_TileDataParam.goal;

            a_CatchObj.SelectEvent();
*/
        }

        s_SelectCatchObjList = new List<CatchObject>();
        s_CatchObjParam = new CatchObjectData();
    }
}
