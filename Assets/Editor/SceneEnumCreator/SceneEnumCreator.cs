﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class SceneEnumCreator
{
    private const string    COMMAND_NAME = "Tools/Create/SceneEnum/Create";

    /// <summary>
    /// エクスポート先フォルダ
    /// </summary>
    private const string    c_DefaultExportPath = "Assets/Scripts/Enum/";

    private const string c_ScriptName = "SceneEnum.cs";

    private const string    TAB = "    ";

    //  ファイル名（拡張子あり、無し）
    private static readonly string  FILENAME_WITHOUT_EXTENSION = Path.GetFileNameWithoutExtension(c_DefaultExportPath + c_ScriptName);

    /// <summary>
    /// オーディオのファイル名を定数で管理するクラスを作成
    /// </summary>
    [MenuItem(COMMAND_NAME)]
    public static void     Create()
    {
        if (!canCreate())   return;

        Init();
    }

    static void    Init()
    {
        CreateScript();
    }

    /// <summary>
    /// スクリプトを作成する
    /// </summary>
    public static void CreateScript()
    {
        StringBuilder a_Builder = new StringBuilder();

        a_Builder.AppendLine("using UnityEngine;");

        a_Builder.AppendLine("\t");

        a_Builder.AppendLine("/// <summary>");
        a_Builder.AppendLine("/// シーン名を定数で管理するクラス");
        a_Builder.AppendLine("/// <summary>");
        a_Builder.AppendFormat("public class {0}", FILENAME_WITHOUT_EXTENSION).AppendLine();

        a_Builder.AppendLine("{");

//  enumの定義
        a_Builder.AppendLine("\t");

        a_Builder.AppendLine(TAB + "public enum eScene");
        a_Builder.AppendLine(TAB + "{");

        a_Builder.AppendLine(TAB + TAB + "Dummy = -1,");

        a_Builder.AppendLine("\t");

        foreach (EditorBuildSettingsScene a_Scene in EditorBuildSettings.scenes)
        {
            string  a_SceneName = Path.GetFileNameWithoutExtension(a_Scene.path);
            a_Builder.AppendLine(TAB + TAB + a_SceneName + ",");
        }

        a_Builder.AppendLine(TAB + TAB + "Max,");

        a_Builder.AppendLine(TAB + "}");
//  enumの定義 end

        a_Builder.AppendLine("}");

        string a_ExportPath = c_DefaultExportPath + c_ScriptName;

        string a_DirectoryName = Path.GetDirectoryName(a_ExportPath);
        if(!Directory.Exists(a_DirectoryName))
        {
            Directory.CreateDirectory(a_DirectoryName);
        }

        File.WriteAllText(a_ExportPath, a_Builder.ToString(), Encoding.UTF8);
        AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
    }

    [MenuItem(COMMAND_NAME, true)]
    private static bool canCreate()
    {
        return !EditorApplication.isPlaying && !Application.isPlaying && !EditorApplication.isCompiling;
    }
}
