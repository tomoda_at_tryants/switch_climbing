﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using nn.hid;
using System;

public class NpadManager : MonoBehaviour
{
    public const string eTAG = "NpadManager";

    private NpadStyle m_PreNPadStyle;

    private int m_nHandleCount;

    private SixAxisSensorHandle[] m_Handles;

    private SixAxisSensorState[] m_SensorStates;
    /// <summary>
    /// センサーステートを取得する
    /// </summary>
    /// <param name="p_Handle"></param>
    /// <param name="p_SensorState"></param>
    /// <returns></returns>
    public bool GetSensorState(Switch_def.eHandle p_Handle, ref SixAxisSensorState p_SensorState)
    {
        if (m_nHandleCount == 1) return false;

        p_SensorState = m_SensorStates[(int)p_Handle];
        return true;
    }

    /// <summary>
    /// 全ての軸に対する加速度の合計値を絶対値に変換して返す
    /// </summary>
    /// <returns></returns>
    public float GetAllAddAcceleration(Switch_def.eHandle p_Handle)
    {
        SixAxisSensorState a_State = m_SensorStates[(int)p_Handle];
        return Mathf.Abs(a_State.acceleration.x) + Mathf.Abs(a_State.acceleration.y) + Mathf.Abs(a_State.acceleration.z);
    }

    /// <summary>
    /// 加速度を取得する
    /// </summary>
    /// <param name="p_Handle"></param>
    /// <returns></returns>
    public Vector3 GetAcceleration(Switch_def.eHandle p_Handle)
    {
        SixAxisSensorState a_State = m_SensorStates[(int)p_Handle];
        return new Vector3(a_State.acceleration.x, a_State.acceleration.y, a_State.acceleration.z);
    }

    /// <summary>
    /// 指定したハンドルのジョイコンのクォータニオンを取得する
    /// </summary>
    /// <param name="p_Handle"></param>
    /// <returns></returns>
    public Quaternion GetJoyconQuaternion(Switch_def.eHandle p_Handle)
    {
        nn.util.Float4 a_Quaternion = new nn.util.Float4();
        m_SensorStates[(int)p_Handle].GetQuaternion(ref a_Quaternion);

        return new Quaternion(a_Quaternion.x, a_Quaternion.y, a_Quaternion.z, a_Quaternion.w);
    }

    private NpadId m_NPadID;
    public NpadId id { get { return m_NPadID; } }

    private NpadStyle m_NPadStyle;
    public NpadStyle style { get { return m_NPadStyle; } }

    private NpadState m_NpadState;

    /// <summary>
    /// 初期化が完了している
    /// </summary>
    private static bool m_bInitialized;

    /// <summary>
    /// 既に現在のフレーム内でパッドの状態を取得している
    /// </summary>
    private bool m_bGetPadState;

    private ulong m_nPushButtons_PreviousNum;
    /// <summary>
    /// 1フレーム前のボタンの入力状態
    /// </summary>
    private NpadButton m_PushButtons_Previous
    {
        get
        {
            return (NpadButton)m_nPushButtons_PreviousNum;
        }
    }

    /// <summary>
    /// 何らかのキーを入力している
    /// </summary>
    public bool isAnyButtonPush
    {
        get
        {
            return (m_NpadState.buttons != 0) ? true : false;
        }
    }

    /// <summary>
    /// どのキーも押されていない
    /// </summary>
    public bool isNotPress
    {
        get
        {
            return (m_NpadState.buttons == 0) ? true : false;
        }
    }


    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static NpadManager Create(Transform p_Parent, List<NpadId> p_Ids, NpadStyle p_Style)
    {
        GameObject a_Obj = new GameObject("NPadManager");
        a_Obj.transform.SetParent(p_Parent);

        NpadManager me = a_Obj.AddComponent<NpadManager>();
        me.Init(p_Ids, p_Style);

        return me;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    private void Init(List<NpadId> p_IdList, NpadStyle p_Style)
    {
        if (m_bInitialized) return;
        m_bInitialized = true;

        Npad.Initialize();

        //  パッドID設定
        if (p_IdList != null && p_IdList.Count > 0)
        {
            NpadId[] a_Ids = p_IdList.ToArray();

            int a_nCount = a_Ids.Length;
            Npad.SetSupportedIdType(a_Ids, a_nCount);
        }

        //  パッドスタイル設定
        m_NPadStyle = p_Style;
        m_PreNPadStyle = NpadStyle.None;

        m_nHandleCount = 0;

        m_Handles = new SixAxisSensorHandle[2];
        m_SensorStates = new SixAxisSensorState[2];

        Npad.SetSupportedStyleSet(m_NPadStyle);

        m_NpadState = new NpadState();
    }

    /// <summary>
    /// 指定したボタンが押されているか調べる
    /// </summary>
    /// <param name="p_Button"></param>
    /// <returns></returns>
    public bool PushButtonCheck(NpadButton p_Button)
    {
        //        Debug.Log(string.Format("before:{0}　after:{1}　before & p_Button:{2}　after & p_Button:{3}", pushButtons_previous, m_NpadState.buttons, (pushButtons_previous & p_Button) == 0, (m_NpadState.buttons & p_Button) != 0 ));

        //  デバッグ
        return (((m_PushButtons_Previous & p_Button) == 0) && ((m_NpadState.buttons & p_Button) != 0)) ? true : false;
    }

    /// <summary>
    /// 指定したボタンが押され続けているか調べる
    /// </summary>
    /// <returns></returns>
    public bool PushingButtonCheck(NpadButton p_Button)
    {
        return ((m_NpadState.buttons & p_Button) != 0) ? true : false;
    }

    /// <summary>
    /// 指定したボタンが離されたか調べる
    /// </summary>
    /// <param name="p_Button"></param>
    /// <returns></returns>
    public bool ReleaseButtonCheck(NpadButton p_Button)
    {
        return (((m_PushButtons_Previous & p_Button) != 0) && ((m_NpadState.buttons & p_Button) == 0)) ? true : false;
    }

    /// <summary>
    /// 指定したボタンが離され続けているか調べる
    /// </summary>
    /// <returns></returns>
    public bool ReleasingButtonCheck(NpadButton p_Button)
    {
        return ((m_NpadState.buttons & p_Button) == 0) ? true : false;
    }

    Vector2 analogStick_L
    {
        get { return new Vector2(m_NpadState.analogStickL.x, m_NpadState.analogStickL.y); }
    }

    Vector2 analogStick_R
    {
        get { return new Vector2(m_NpadState.analogStickR.x, m_NpadState.analogStickR.y); }
    }

    /// <summary>
    /// 指定したスティックを調べる（単位：％　０：全く倒されていない　１００：完全に倒されている）
    /// </summary>
    /// <returns></returns>
    public float  PushStickCheck(NpadButton p_Stick)
    {
        if ((p_Stick != NpadButton.StickLUp) && (p_Stick != NpadButton.StickLDown) && (p_Stick != NpadButton.StickLRight) && (p_Stick != NpadButton.StickLLeft) &&
            (p_Stick != NpadButton.StickRUp) && (p_Stick != NpadButton.StickRDown) && (p_Stick != NpadButton.StickRRight) && (p_Stick != NpadButton.StickRLeft))
        {
            Debug.Log(string.Format("{0}　The specified button is not a stick", eTAG));
            return 0;
        }

        int a_nRate = 0;

        //  左スティック・上
        if ((p_Stick == NpadButton.StickLUp) && analogStick_L.y > 0)
        {
            a_nRate = Mathf.CeilToInt((analogStick_L.y / AnalogStickState.Max) * 100f);
        }
        else
        //  左スティック・下
        if ((p_Stick == NpadButton.StickLDown) && analogStick_L.y < 0)
        {
            a_nRate = Mathf.CeilToInt((Mathf.Abs(analogStick_L.y) / AnalogStickState.Max) * 100f);
        }
        //  左スティック・左
        if ((p_Stick == NpadButton.StickLLeft) && analogStick_L.x < 0)
        {
            a_nRate = Mathf.CeilToInt((Mathf.Abs(analogStick_L.x) / AnalogStickState.Max) * 100f);
        }
        //  左スティック・右
        if ((p_Stick == NpadButton.StickLRight) && analogStick_L.x > 0)
        {
            a_nRate = Mathf.CeilToInt((analogStick_L.x / AnalogStickState.Max) * 100f);
        }

        //  右スティック・上
        if ((p_Stick == NpadButton.StickRUp) && analogStick_R.y > 0)
        {
            a_nRate = Mathf.CeilToInt((analogStick_R.y / AnalogStickState.Max) * 100f);
        }
        else
        //  右スティック・下
        if ((p_Stick == NpadButton.StickRDown) && analogStick_R.y < 0)
        {
            a_nRate = Mathf.CeilToInt((Mathf.Abs(analogStick_R.y) / AnalogStickState.Max) * 100f);
        }
        //  右スティック・左
        if ((p_Stick == NpadButton.StickRLeft) && analogStick_R.x < 0)
        {
            a_nRate = Mathf.CeilToInt((Mathf.Abs(analogStick_R.x) / AnalogStickState.Max) * 100f);
        }
        //  右スティック・右
        if ((p_Stick == NpadButton.StickRRight) && analogStick_R.x > 0)
        {
            a_nRate = Mathf.CeilToInt((analogStick_R.x / AnalogStickState.Max) * 100f);
        }

        if (a_nRate < 0)    a_nRate = 0;
        if (a_nRate > 100)  a_nRate = 100;

        return a_nRate;
    }

    /// <summary>
    /// パッドの状態を取得する（1フレームで１度だけ呼び出す）
    /// </summary>
    private void GetPadState()
    {
        //  まだパッドの状態を取得していない
        if (!m_bGetPadState && CheckPadState())
        {
            //  ボタンの状態を保存しておく
            m_nPushButtons_PreviousNum = (ulong)m_NpadState.buttons;

            //  状態取得
            Npad.GetState(ref m_NpadState, m_NPadID, m_NPadStyle);

            m_bGetPadState = true;
        }
    }

    /// <summary>
    /// キーパッドの状態を更新する
    /// </summary>
    /// <returns></returns>
    private bool    CheckPadState()
    {
        NpadId      a_ID    = NpadId.Handheld;
        NpadStyle   a_Style = Npad.GetStyleSet(a_ID);

        if(a_Style != NpadStyle.Handheld)
        {
            a_ID    = NpadId.No1;
            a_Style = Npad.GetStyleSet(a_ID);
        }

        if((a_Style != NpadStyle.None) && (a_Style != m_PreNPadStyle))
        {
            HandleStop();

            m_nHandleCount = SixAxisSensor.GetHandles(m_Handles, 2, a_ID, a_Style);

            HandleStart();

            m_PreNPadStyle = a_Style;
        }

        for(int i = 0 ; i < m_nHandleCount ; i++)
        {
            SixAxisSensorHandle a_SensorHandle  = m_Handles[i];

            //  センサーの状態を取得する
            SixAxisSensor.GetState(ref m_SensorStates[i], a_SensorHandle);
        }

        m_NPadID      = a_ID;
        m_NPadStyle   = a_Style;

        return true;
    }

    /// <summary>
    /// ハンドルを停止する
    /// </summary>
    void    HandleStop()
    {
        for (int i = 0; i < m_nHandleCount; i++)
        {
            SixAxisSensor.Stop(m_Handles[i]);
        }
    }

    /// <summary>
    /// ハンドルをスタートする
    /// </summary>
    void    HandleStart()
    {
        for (int i = 0; i < m_nHandleCount; i++)
        {
            SixAxisSensor.Start(m_Handles[i]);
        }
    }

    void Update()
    {
        StartCoroutine(Update_Coroutine());
    }

    public IEnumerator  Update_Coroutine()
    {
        //  パッドの状態を取得する
        GetPadState();
        
        yield return new WaitForEndOfFrame();

        m_bGetPadState = false;
    }
}
