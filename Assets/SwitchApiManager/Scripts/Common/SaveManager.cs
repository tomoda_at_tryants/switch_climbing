﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//  Switch
using nn.account;
using nn.fs;
using System;
using System.Reflection;

public class SaveManager : MonoBehaviour
{
    public const string eTAG = "SaveManager";

    /// <summary>
    /// マウント名
    /// </summary>
    private readonly string c_MountName = eTAG;

    public string GetFilePath(string p_FileName)
    {
        if (string.IsNullOrEmpty(p_FileName)) return "";

        string a_FilePath = string.Format("{0}:/{1}", c_MountName, p_FileName);
        return a_FilePath;
    }

    FileHandle m_FileHandle;
    /// <summary>
    /// ファイルハンドラ
    /// </summary>
    public FileHandle fileHandle
    {
        get { return m_FileHandle; }
    }

    /// <summary>
    /// パスが存在しない
    /// </summary>
    nn.Result result_PathNotFound
    {
        get
        {
            return  FileSystem.ResultPathNotFound;
        }
    }

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static SaveManager Create(Transform p_Parent)
    {
        GameObject a_Obj = new GameObject();
        SaveManager me = a_Obj.AddComponent<SaveManager>();

        a_Obj.transform.SetParent(p_Parent);

        me.Init();

        return me;
    }

    /// <summary>
    /// 初期化が完了している
    /// 「Account.Initialize()」用のフラグ
    /// ※初期化は初回時にしか行わないが、
    /// ネイティブからコールする処理は重いので極力呼ばないようにするための対策
    /// </summary>
    private static bool m_bInitialized;

    /// <summary>
    /// 初期化
    /// </summary>
    private void Init()
    {
        if (m_bInitialized) return;
        m_bInitialized = true;

        //  セーブデータを読み書き可能な状態に（マウント）する
        nn.Result a_Result = SaveData.Mount(c_MountName, SwitchApiManager.instance.accountMan.userID);
        //  成否テスト
        a_Result.abortUnlessSuccess();
    }

    /// <summary>
    /// ファイルを生成する
    /// </summary>
    /// <returns></returns>
    public nn.Result    CreateFile(string p_FilePath)
    {
        //  ファイルを生成する
        nn.Result   a_Result = nn.fs.File.Create(p_FilePath, Switch_def.s_nSaveDataSize);
        return  a_Result;
    }

    /// <summary>
    /// 保存する
    /// </summary>
    public void Save<T>(string p_FileName, T p_SaveData)
    {
        if (p_SaveData == null) return;

        //  クラスかどうか取得する
        bool    a_IsClass = GameUtils.GetClassType(p_SaveData);

        string a_Serialize = "";
        //  引数に指定したSaveData型がクラスである時はシリアライズを行う
        if(a_IsClass)
        {
            try
            {
                a_Serialize = JsonUtility.ToJson(p_SaveData);
            }
            catch
            {
                Debug.Log(string.Format("{0}は「JsonUtility.ToJson()」でエラーが起こりました"));
                return;
            }
            
            //  シリアライズ化できない場合はToStringで文字列に変換
            if(a_Serialize == "{}")
            {
                a_Serialize = p_SaveData.ToString();
            }
        }
        else
        {
            a_Serialize = p_SaveData.ToString();
        }

        byte[]  a_Data;
        //  メモリ上に確保されたバイト配列に対して読み書きを行う
        using (MemoryStream a_Stream = new MemoryStream(a_Serialize.Length))    //  セーブデータ用のサイズを指定
        {
            BinaryWriter    a_Writer = new BinaryWriter(a_Stream);
            //  文字列かしたクラス変数を書き込む
            a_Writer.Write(a_Serialize);
            a_Stream.Close();

            //  バイト配列を取得
            a_Data = a_Stream.GetBuffer();
        }

#if UNITY_SWITCH
        UnityEngine.Switch.Notification.EnterExitRequestHandlingSection();
#endif

        string  a_FilePath      = GetFilePath(p_FileName);
        bool    a_bCreatedFile  = false;

        nn.Result a_Result = nn.fs.File.Delete(a_FilePath);

        //  指定したパスにファイルが見つからない（存在していないという事なので、新しく生成する）
        if(a_Result == result_PathNotFound)
        {
            //  ファイルを生成する
            a_Result = CreateFile(a_FilePath);
            if (!a_Result.IsSuccess())
            {
                return;
            }
            else
            {
                a_bCreatedFile = true;
            }
        }
        else
        {
            a_Result.abortUnlessSuccess();
        }

        if(!a_bCreatedFile) a_Result = CreateFile(a_FilePath);

        //  ファイルを開く
        a_Result = nn.fs.File.Open(ref m_FileHandle, a_FilePath, OpenFileMode.Write);
        a_Result.abortUnlessSuccess();

        //  ファイルの書き込み
        a_Result = nn.fs.File.Write(m_FileHandle, 0, a_Data, a_Data.LongLength, nn.fs.WriteOption.Flush);
        a_Result.abortUnlessSuccess();

        //  ファイルを閉じる
        nn.fs.File.Close(m_FileHandle);

        //  書き込みを確定する
        a_Result = SaveData.Commit(c_MountName);
        a_Result.abortUnlessSuccess();

#if UNITY_SWITCH
        UnityEngine.Switch.Notification.LeaveExitRequestHandlingSection();
#endif
    }

    /// <summary>
    /// ロードを行う
    /// </summary>
    public void Load<T>(string p_FileName, ref T p_LoadData)
    {
        string  a_FilePath = GetFilePath(p_FileName);

        //  エントリの種類を取得する
        EntryType   a_EntryType = 0;
        nn.Result   a_Result    = FileSystem.GetEntryType(ref a_EntryType, a_FilePath);

        //  パスが存在しない
        if (a_Result == result_PathNotFound) { return; }
        a_Result.abortUnlessSuccess();

        //  ファイルを開き、指定したファイルのハンドルを取得
        a_Result = nn.fs.File.Open(ref m_FileHandle, a_FilePath, OpenFileMode.Read);
        //  ファイルオープンに失敗
        if(a_Result == result_PathNotFound)
        {
            return;
        }
        else
        if(a_Result.IsSuccess())
        {
            a_Result.abortUnlessSuccess();
        }

        //  ファイルサイズ取得
        long    a_FileSize = 0;
        a_Result = nn.fs.File.GetSize(ref a_FileSize, m_FileHandle);
        a_Result.abortUnlessSuccess();

        //  データの読み出し
        byte[]  a_Data = ReadData(m_FileHandle, 0, a_FileSize);

        //  ファイルを閉じる
        FileClose(m_FileHandle);

        //  データの取得
        using (MemoryStream a_Stream = new MemoryStream(a_Data))
        {
            if (!a_Stream.CanRead)  return;

            BinaryReader    a_Reader    = new BinaryReader(a_Stream);

            //  デシリアライズ
            string  a_ReadData          = a_Reader.ReadString();
            bool    a_IsClass           = GameUtils.GetClassType(p_LoadData);
            T       a_DesirializeData   = default(T);
            bool    a_bJsonData         = false;

            if (a_IsClass)
            {
                try
                {
                    a_DesirializeData = JsonUtility.FromJson<T>(a_ReadData);
                    p_LoadData = a_DesirializeData;

                    a_bJsonData = true;
                }
                catch
                {
                    a_bJsonData = false;
                }
            }

            //  保存されているデータがJson形式ではない
            if(!a_bJsonData)
            {
                Type        a_Type      = p_LoadData.GetType();
                Type[]      a_Types     = new Type[] { typeof(string), typeof(T).MakeByRefType() };
                object[]    a_Param     = new object[] { a_ReadData, a_DesirializeData };
                MethodInfo  a_Method    = a_Type.GetMethod("TryParse", a_Types);

                if(a_Method != null)
                {
                    if (!(bool)a_Method.Invoke(null, a_Param))
                    {
                        return;
                    }
                }
                else
                {
                    a_Param[1] = a_ReadData;
                }

                p_LoadData = (T)a_Param[1];
            }
        }
    }

    /// <summary>
    /// 指定したパスのディレクトリエントリの種類を取得する
    /// </summary>
    /// <param name="p_EntryType"></param>
    /// <param name="p_FilePath"></param>
    /// <returns></returns>
    public EntryType GetEntryType(string p_FilePath)
    {
        EntryType a_EntryType = 0;
        nn.Result a_Result = FileSystem.GetEntryType(ref a_EntryType, p_FilePath);

        //  パスが存在しない
        if (a_Result == result_PathNotFound) { return a_EntryType; }
        a_Result.abortUnlessSuccess();

        return a_EntryType;
    }

    /// <summary>
    /// 　指定したパスのファイルのサイズを取得する
    /// </summary>
    public long GetFileSize(FileHandle p_FileHandle)
    {
        long a_FileSize = 0;
        nn.Result a_Result = nn.fs.File.GetSize(ref a_FileSize, p_FileHandle);
        a_Result.abortUnlessSuccess();

        return a_FileSize;
    }

    /// <summary>
    /// 指定したファイルのデータを読み出す
    /// </summary>
    /// <returns></returns>
    public byte[] ReadData(FileHandle p_FileHandle, long p_Offset, long p_Size)
    {
        byte[] a_Data = new byte[p_Size];
        nn.Result a_Result = nn.fs.File.Read(p_FileHandle, p_Offset, a_Data, p_Size);
        a_Result.abortUnlessSuccess();

        return a_Data;
    }

    /// <summary>
    /// 指定したファイルハンドルを閉じる
    /// </summary>
    /// <param name="p_FileHandle"></param>
    public void FileClose(FileHandle p_FileHandle)
    {
        nn.fs.File.Close(p_FileHandle);
    }

    /// <summary>
    /// Unity上で再生を停止した時でも呼ばれる
    /// </summary>
    void OnDestroy()
    {
        //  マウント済みのファイルシステムをアンマウントし、リソースを解放する
        //  実機上でもUnity上でもセーブかロードを行った際は必ず呼ばれないといけない（アンマウントを行わずにもう一度実行しようとすると停止する）
        FileSystem.Unmount(c_MountName);
    }

    public void ResetSaveData()
    {
    }
}
