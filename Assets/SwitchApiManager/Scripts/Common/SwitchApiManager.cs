﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using nn.hid;

public class SwitchApiManager : MonoBehaviour
{
    public const string eTAG = "SwitchApiManager";

    //  デフォルトのパッドID
    private readonly List<NpadId> m_DefaultNpadIdList = new List<NpadId>
    {
        NpadId.Handheld,    //  Joy-Conが２つともパッドに挿さっている状態
        NpadId.No1          //  一番最初に認識したパッド
    };

    /// <summary>
    /// デフォルトのパッドのスタイル
    /// </summary>
    private readonly NpadStyle  m_DefaultNpadStyle = NpadStyle.Handheld | NpadStyle.JoyDual;

    static SwitchApiManager s_Instance;
    /// <summary>
    /// シングルトンインスタンス
    /// </summary>
    public static SwitchApiManager instance
    {
        get
        {
            if (s_Instance == null) Create();
            return s_Instance;
        }
        set
        {
            if (s_Instance == null) Create();
            s_Instance = value;
        }
    }

    AccountManager m_Account;
    /// <summary>
    /// アカウントマネージャ
    /// </summary>
    public AccountManager accountMan
    {
        get
        {
            if (m_Account == null)
            {
                AccountManager a_Instance = new AccountManager();
                a_Instance = AccountManager.Create();

                m_Account = a_Instance;
            }
            return m_Account;
        }

        set
        {
            if (m_Account == null)
            {
                AccountManager a_Instance = new AccountManager();
                a_Instance = AccountManager.Create();

                m_Account = a_Instance;
            }
            m_Account = value;
        }
    }

    NpadManager m_Npad;
    /// <summary>
    /// NPadマネージャ
    /// </summary>
    public NpadManager NpadMan
    {
        get
        {
            if (m_Npad == null)
            {
                NpadManager a_Instance = new NpadManager();
                a_Instance = NpadManager.Create(transform, m_DefaultNpadIdList, m_DefaultNpadStyle);

                m_Npad = a_Instance;
            }
            return m_Npad;
        }

        set
        {
            if (m_Npad == null)
            {
                NpadManager a_Instance = new NpadManager();
                a_Instance = NpadManager.Create(transform, m_DefaultNpadIdList, m_DefaultNpadStyle);

                m_Npad = a_Instance;
            }
            m_Npad = value;
        }
    }

    SaveManager m_Save;
    /// <summary>
    /// セーブデータマネージャ
    /// </summary>
    public SaveManager saveMan
    {
        get
        {
            if (m_Save == null)
            {
                SaveManager a_Instance = new SaveManager();
                a_Instance = SaveManager.Create(transform);

                m_Save = a_Instance;
            }
            return m_Save;
        }

        set
        {
            if (m_Save == null)
            {
                SaveManager a_Instance = new SaveManager();
                a_Instance = SaveManager.Create(transform);

                m_Save = a_Instance;
            }
            m_Save = value;
        }
    }

    VibrationManager m_Vibration;
    /// <summary>
    /// 振動マネージャー
    /// </summary>
    public VibrationManager vibrationMan
    {
        get
        {
            if(m_Vibration == null)
            {
                VibrationManager a_Instance = new VibrationManager();
                a_Instance = VibrationManager.Create(transform);

                m_Vibration = a_Instance;
            }
            return m_Vibration;
        }

        set
        {
            if (m_Vibration == null)
            {
                VibrationManager a_Instance = new VibrationManager();
                a_Instance = VibrationManager.Create(transform);

                m_Vibration = a_Instance;
            }
            m_Vibration = value;
        }
    }

    /// <summary>
    /// 生成
    /// </summary>
    public static void  Create()
    {
        if (s_Instance != null) return;

        GameObject  a_Obj = new GameObject("SwitchApiManager");
        s_Instance = a_Obj.AddComponent<SwitchApiManager>();

        s_Instance.Init();

        DontDestroyOnLoad(a_Obj);
    }

    public void Init()
    {
        //  クラス内で管理しているマネージャクラスは全てここで初期化を行う
        m_Account   = AccountManager.Create();
        m_Npad      = NpadManager.Create(transform, m_DefaultNpadIdList, m_DefaultNpadStyle);
        m_Save      = SaveManager.Create(transform);
        m_Vibration = VibrationManager.Create(transform);
    }
}
