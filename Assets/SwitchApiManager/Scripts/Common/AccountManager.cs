﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  Switch
using nn.account;
using System;

public class AccountManager : MonoBehaviour
{
    public const string eTAG = "AccountManager";

    UserHandle m_UserHandle;
    /// <summary>
    /// オープンしているハンドル
    /// </summary>
    public UserHandle userHandle
    {
        get { return m_UserHandle; }
    }

    Uid m_UserID;
    /// <summary>
    /// オープンしているユーザーID
    /// </summary>
    public Uid userID
    {
        get { return m_UserID; }
    }

    /// <summary>
    /// 初期化が完了している
    /// </summary>
    private static bool m_bInitialized;

    public static AccountManager   Create()
    {
        GameObject      a_Obj   = new GameObject();
        AccountManager me       = a_Obj.AddComponent<AccountManager>();

        Destroy(a_Obj);

        me.Init();

        return me;
    }

    public void Init()
    {
        if (m_bInitialized) return;
        m_bInitialized = true;

        //  アカウントシステムの初期化処理
        Account.Initialize();

        //  Open状態にあるユーザーを扱うためのハンドル
        m_UserHandle    = new UserHandle();
        m_UserID        = new Uid();

        //  アプリケーション起動時に選択されたユーザーをOpen状態に変更し、ハンドルを取得する
        Account.OpenPreselectedUser(ref m_UserHandle);
        //  ユーザーID取得
        Account.GetUserId(ref m_UserID, m_UserHandle);
    }
}
