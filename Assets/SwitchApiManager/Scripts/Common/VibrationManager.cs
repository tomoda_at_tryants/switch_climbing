﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  Switch
using nn.hid;

public class VibrationManager : MonoBehaviour
{
    private const int c_nVibrationDeviceCountMax = 2;

    private int m_nVibrationDeviceCount;

    private VibrationDeviceHandle[] m_VibrationDeviceHandles;
    private VibrationDeviceInfo[]   m_VibrationdeviceInfos;

    /// <summary>
    /// 振動値
    /// </summary>
    private VibrationValue m_VibrationValue;

    public static VibrationManager  Create(Transform p_Parent)
    {
        GameObject          a_Obj   = new GameObject("VibrationManager");
        a_Obj.transform.SetParent(p_Parent);

        VibrationManager    me      = a_Obj.AddComponent<VibrationManager>();
        me.Init();

//        Destroy(a_Obj);

        return me;
    }

    public void Init()
    {
        m_nVibrationDeviceCount = 0;

        m_VibrationDeviceHandles    = new VibrationDeviceHandle[c_nVibrationDeviceCountMax];
        m_VibrationdeviceInfos      = new VibrationDeviceInfo[c_nVibrationDeviceCountMax];
        //  デフォルトの振動値を生成
        m_VibrationValue            = VibrationValue.Make();
    }
    
    /// <summary>
    /// 振動処理の実行（1フレームのみ）
    /// </summary>
    /// <param name="p_fAmplitudeLow"></param>
    /// <param name="p_fAmplitudeHigh"></param>
    public void Execute(float p_fAmplitudeLow, float p_fAmplitudeHigh, Switch_def.eHandle p_TargetDevice)
    {
        m_VibrationValue.Clear();

        m_VibrationValue.amplitudeLow   = p_fAmplitudeLow;
        m_VibrationValue.amplitudeHigh  = p_fAmplitudeHigh;

        //  振動機器の確認
        if(CheckVibrationDevice())
        {
            //  左右どっちも振動させる
            switch (p_TargetDevice)
            {
                case Switch_def.eHandle.Both:
                    {
                        for (int i = 0; i < m_nVibrationDeviceCount; i++)
                        {
                            Vibration.SendValue(m_VibrationDeviceHandles[i], m_VibrationValue);
                        }
                    }
                    break;

                case Switch_def.eHandle.Left:
                case Switch_def.eHandle.Right:
                    {
                        for(int i = 0; i < m_nVibrationDeviceCount; i++)
                        {
                            if((p_TargetDevice == Switch_def.eHandle.Left) && (m_VibrationdeviceInfos[i].position == VibrationDevicePosition.Left) ||
                              ((p_TargetDevice == Switch_def.eHandle.Right) && (m_VibrationdeviceInfos[i].position == VibrationDevicePosition.Right)))
                            {
                                Vibration.SendValue(m_VibrationDeviceHandles[i], m_VibrationValue);
                            }
                        }
                    }
                    break;
            }
        }
    }

    public void Execute(float p_fAmplitudeLow, float p_fAmplitudeHigh, float p_fExeTime, Switch_def.eHandle p_TargetDevice)
    {
        StartCoroutine(Execute_Coroutine(p_fAmplitudeLow, p_fAmplitudeHigh, p_fExeTime, p_TargetDevice));
    }

    /// <summary>
    /// 指定した時間振動処理を行う（振動対象になっている機器全てに対して実行）
    /// </summary>
    /// <param name="p_fAmplitudeLow"></param>
    /// <param name="p_fAmplitudeHigh"></param>
    /// <param name="p_fExeTime"></param>
    /// <returns></returns>
    public IEnumerator  Execute_Coroutine(float p_fAmplitudeLow, float p_fAmplitudeHigh, float p_fExeTime, Switch_def.eHandle p_TargetDevice)
    {
        float a_fTime = 0;
        while (a_fTime < p_fExeTime)
        {
            a_fTime += Time.deltaTime;
            Execute(p_fAmplitudeLow, p_fAmplitudeHigh, p_TargetDevice);

            yield return 0;
        }

        //  終了したら振動値を初期化する
        Stop(p_TargetDevice);

        yield return 0;
    }

    public void Stop(Switch_def.eHandle p_TargetDevice)
    {
        m_VibrationValue.Clear();

        //  左右どっちも振動させる
        switch (p_TargetDevice)
        {
            case Switch_def.eHandle.Both:
                {
                    for (int i = 0; i < m_nVibrationDeviceCount; i++)
                    {
                        Vibration.SendValue(m_VibrationDeviceHandles[i], m_VibrationValue);
                    }
                }
                break;

            case Switch_def.eHandle.Left:
            case Switch_def.eHandle.Right:
                {
                    for (int i = 0; i < m_nVibrationDeviceCount; i++)
                    {
                        if ((p_TargetDevice == Switch_def.eHandle.Left) && (m_VibrationdeviceInfos[i].position == VibrationDevicePosition.Left) ||
                          ((p_TargetDevice == Switch_def.eHandle.Right) && (m_VibrationdeviceInfos[i].position == VibrationDevicePosition.Right)))
                        {
                            Vibration.SendValue(m_VibrationDeviceHandles[i], m_VibrationValue);
                        }
                    }
                }
                break;
        }
    }

    /// <summary>
    /// 振動対象のデバイスを取得する
    /// </summary>
    /// <returns></returns>
    public bool CheckVibrationDevice()
    {
        if (SwitchApiManager.instance.NpadMan.style == NpadStyle.None) return false;

        m_nVibrationDeviceCount = Vibration.GetDeviceHandles(m_VibrationDeviceHandles, c_nVibrationDeviceCountMax, SwitchApiManager.instance.NpadMan.id, SwitchApiManager.instance.NpadMan.style);

        for(int i = 0 ; i < m_nVibrationDeviceCount ; i++)
        {
            Vibration.InitializeDevice(m_VibrationDeviceHandles[i]);
            Vibration.GetDeviceInfo(ref m_VibrationdeviceInfos[i], m_VibrationDeviceHandles[i]);
        }

        return true;
    }
}
