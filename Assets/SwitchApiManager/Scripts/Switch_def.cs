﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch_def
{
    /// <summary>
    /// プレイモード
    /// </summary>
    public enum ePlayMode
    {
        None = -1,

        Single,
        Multi
    }
    /// <summary>
    /// プレイモード
    /// </summary>
    public static readonly ePlayMode s_PlayMode = ePlayMode.Single;

    /// <summary>
    /// 左右を表す固定値
    /// </summary>
    public enum eHandle
    {
        None = -1,

        Left,   //  左手
        Right,  //  右手
        Both,   //  両方

        Max
    }

    /// <summary>
    /// 使用するセーブデータサイズ（バイト単位） 
    /// （実際のセーブデータサイズとは異なる（詳細は「Memo」の「セーブデータの作成について」を参照））
    /// </summary>
    public static int s_nSaveDataSize
    {
        get
        {
            //  単位はKBで統一
            int a_nSize = 10;   //  10KB
            int a_nUnit = 16;   //  16KB単位で指定する（１バイトしか使用しない場合でも最低１６KBを指定する）
            int a_nRemainder = a_nSize % a_nUnit;

            //  Switchが確保するセーブデータサイズは必ず16KB単位で指定するため以下の計算を行う
            //  指定したデータサイズが16（KB）でで割り切れない時は繰り上げる
            a_nSize += (a_nRemainder == 0) ? 0 : (16 - (a_nSize % 16));
            a_nSize *= 1024;    //  バイトに変換

            return a_nSize;
        }
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public static void  Initialize()
    {
        //  「QualitySettings」のデフォルトの設定（基本的には変更しない）
        QualitySettings.maxQueuedFrames = 2;
        QualitySettings.vSyncCount = 1;     //  列挙型が取得できないのでハードコーディング（「Every V Blank」）

        //  SwitchのAPIマネージャー
        SwitchApiManager.Create();
    }

    /// <summary>
    /// ステージ名を取得する
    /// </summary>
    /// <returns></returns>
    public static string    GetStageName(int p_nStageNum)
    {
        string  a_Format    = "Stage_";
        string  a_Str       = p_nStageNum.ToString();

        return  a_Format + a_Str.ToString().PadLeft(3, '0');
    }
}
