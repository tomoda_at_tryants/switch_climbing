﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using nn.hid;

public class scn_switchtest : MonoBehaviour
{
    public static string eTAG
    {
        get { return typeof(scn_switchtest).ToString(); }
    }

    public GameObject   sixAxisPanel;
    public GameObject   quatGraphPanel;
    public Sprite        graphImage;

    public Graph[]      graphs;
    public Text         commandText;

    bool    m_bGraphStop;

    int m_nPushCount;

    public NpadStyle    preNpadStyle    = NpadStyle.None;
    public int          handleCount     = 0;
    public SixAxisSensorHandle[]    handle  = new SixAxisSensorHandle[2];
    public SixAxisSensorState[] sensorStates = new SixAxisSensorState[2];

    public GameObject   cube;

    void Start()
    {
        def.Initialize();

        m_bGraphStop    = false;
        m_nPushCount    = 0;
    }

    void Update()
    {
        NpadId npadId = NpadId.Handheld;
        NpadStyle npadStyle = NpadStyle.None;

        if (npadStyle != NpadStyle.Handheld)
        {
            npadId = NpadId.No1;
            npadStyle = Npad.GetStyleSet(npadId);
        }

        if (npadStyle != NpadStyle.None)
        {
            if (npadStyle != preNpadStyle)
            {
                for (int i = 0; i < handleCount; i++)
                {
                    SixAxisSensor.Stop(handle[i]);
                }

                handleCount = SixAxisSensor.GetHandles(handle, 2, npadId, npadStyle);

                for (int i = 0; i < handleCount; i++)
                {
                    SixAxisSensor.Start(handle[i]);
                }

                preNpadStyle = npadStyle;
            }

            for (int i = 0; i < handleCount; i++)
            {
                SixAxisSensor.GetState(ref sensorStates[i], handle[i]);
            }
        }

        //  ZRボタンが押された
        if (SwitchApiManager.instance.NpadMan.PushButtonCheck(NpadButton.ZL) && m_nPushCount == 0)
        {
            m_nPushCount++;
            m_bGraphStop ^= true;

            DetectionReset();
        }
        else
        if (SwitchApiManager.instance.NpadMan.ReleaseButtonCheck(NpadButton.ZL))
        {
            m_nPushCount        = 0;
        }

        if (!m_bGraphStop)
        {
            Vector3         a_Acceleration      = new Vector3(sensorStates[0].acceleration.x, sensorStates[0].acceleration.y, sensorStates[0].acceleration.z);
            Vector3         a_Velocity          = new Vector3(sensorStates[0].angularVelocity.x, sensorStates[0].angularVelocity.y, sensorStates[0].angularVelocity.z);

            QuaternionExec();

#if true
            nn.util.Float4 a_NpadQuaternion = new nn.util.Float4();
            //  コントローラーのクォータニオン取得
            sensorStates[0].GetQuaternion(ref a_NpadQuaternion);
            Quaternion a_Quaternion = new Quaternion(a_NpadQuaternion.x, a_NpadQuaternion.y, a_NpadQuaternion.z, a_NpadQuaternion.w);

            Vector3 a_Euler = a_Quaternion.eulerAngles;
            graphs[0].SetGraph(0, a_Acceleration.x, 7);
            graphs[1].SetGraph(0, a_Acceleration.y, 7);
            graphs[2].SetGraph(0, a_Acceleration.z, 7);

            graphs[0].SetGraph(1, a_Velocity.x, 5f);
            graphs[1].SetGraph(1, a_Velocity.y, 5f);
            graphs[2].SetGraph(1, a_Velocity.z, 5f);

            graphs[0].SetGraph(2, a_Quaternion.x, 360f);
            graphs[1].SetGraph(2, a_Quaternion.y, 360f);
            graphs[2].SetGraph(2, a_Quaternion.z, 360f);
#endif
        }
    }

    void    QuaternionExec()
    {
        nn.util.Float4 a_NpadQuaternion = new nn.util.Float4();

        //  コントローラーのクォータニオン取得
        sensorStates[0].GetQuaternion(ref a_NpadQuaternion);

        //  コントローラーのクォータニオン
        Quaternion  a_BufQuat       = new Quaternion(a_NpadQuaternion.x, a_NpadQuaternion.y, a_NpadQuaternion.z, a_NpadQuaternion.w);
        //  キャリブレーション値
        Quaternion  a_CalibQuat     = CalibrationExec(a_BufQuat);
        //  最終的なクォータニオン
        Quaternion  a_ResultQuat    = a_CalibQuat * a_BufQuat;

        //  ２D座標用にクォータニオンを変換
        a_ResultQuat.Set(a_ResultQuat.w, a_ResultQuat.y, a_ResultQuat.z, -a_ResultQuat.x);

        //  オブジェクトに反映
        cube.transform.rotation = a_ResultQuat;

        //  クォータニオンを２D座標に変換
        Vector3 a_QuatPos = GetQuaternionPos(cube.transform.rotation);
        QuaternionGraph(a_QuatPos);
    }

    /// <summary>
    /// クォータニオンの値から２次元上に変換した座標を返す
    /// </summary>
    /// <returns></returns>
    Vector3 GetQuaternionPos(Quaternion p_Quaternion)
    {
        //  オブジェクトのベクトル取得
        Vector3 a_Vec = p_Quaternion * Vector3.forward;
        float a_fAtan = Mathf.Atan2(a_Vec.x, a_Vec.z) * Mathf.Rad2Deg;
        float a_fZX = Mathf.Lerp(-1, 1, (a_fAtan / 180f + 1) / 2);
        Vector2 a_QuatPos = new Vector2(a_fZX, a_Vec.y);

        return a_QuatPos;
    }

    Quaternion  m_CalibQuat = Quaternion.identity;
    /// <summary>
    /// キャリブレーション値を設定する
    /// </summary>
    /// <param name="p_Quaternion"></param>
    /// <returns></returns>
    Quaternion  CalibrationExec(Quaternion p_Quaternion)
    {
        if (SwitchApiManager.instance.NpadMan.PushButtonCheck(NpadButton.Minus))
        {
            m_CalibQuat = Quaternion.Inverse(p_Quaternion * new Quaternion(0, 0, 1, 0));
        }

        return m_CalibQuat;
    }

    GameObject m_ImagePanel;
    /// <summary>
    /// グラフ作成
    /// </summary>
    /// <param name="p_GraphPos"></param>
    void QuaternionGraph(Vector2 p_GraphPos)
    {
        if (SwitchApiManager.instance.NpadMan.PushButtonCheck(NpadButton.Minus))
        {
            DestroyImmediate(m_ImagePanel);
            m_ImagePanel = new GameObject();

            m_ImagePanel.transform.SetParent(quatGraphPanel.transform);
            m_ImagePanel.transform.localPosition = Vector2.zero;
        }

        if (m_ImagePanel == null)
        {
            DestroyImmediate(m_ImagePanel);
            m_ImagePanel = new GameObject();

            m_ImagePanel.transform.SetParent(quatGraphPanel.transform);
            m_ImagePanel.transform.localPosition = Vector2.zero;
        }

        //        if (sixAxisPanel.activeSelf)   sixAxisPanel.SetActive(false);

        GameObject  a_Obj = new GameObject();
        a_Obj.transform.SetParent(m_ImagePanel.transform);

        Image a_Image = a_Obj.AddComponent<Image>();
        a_Image.sprite = graphImage;
        a_Image.color = Color.red;
        a_Image.rectTransform.sizeDelta = new Vector2(10, 10);

        p_GraphPos.x = Mathf.Lerp(-300, 300, (p_GraphPos.x + 1) / 2);
        p_GraphPos.y = Mathf.Lerp(-300, 300, (p_GraphPos.y + 1) / 2);
        a_Image.rectTransform.anchoredPosition = p_GraphPos;
    }

    /// <summary>
    /// 検出データのリセット
    /// </summary>
    public void DetectionReset()
    {
        commandText.text = "";
    }
}