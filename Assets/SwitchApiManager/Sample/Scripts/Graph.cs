﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class Graph : MonoBehaviour
{
    public Image[]  plusGraphImages;
    public Image[]  minusGraphImages;

    public Text[]   valueTexts;

    public void SetGraph(int p_nIndex, float p_fValue, float p_fMax)
    {
        Vector2 a_CurrentSize = Vector2.zero;
        if(p_fValue > 0)
        {
            a_CurrentSize = plusGraphImages[p_nIndex].rectTransform.sizeDelta;
            plusGraphImages[p_nIndex].rectTransform.sizeDelta = new Vector2(a_CurrentSize.x, p_fValue * ((def.s_DefaultSize.y / 2) / p_fMax));

            a_CurrentSize = minusGraphImages[p_nIndex].rectTransform.sizeDelta;
            minusGraphImages[p_nIndex].rectTransform.sizeDelta = new Vector2(a_CurrentSize.x, 0);
        }
        else
        if (p_fValue < 0)
        {
            a_CurrentSize = minusGraphImages[p_nIndex].rectTransform.sizeDelta;
            minusGraphImages[p_nIndex].rectTransform.sizeDelta = new Vector2(a_CurrentSize.x, (p_fValue * -1f) * ((def.s_DefaultSize.y / 2) / p_fMax));

            a_CurrentSize = plusGraphImages[p_nIndex].rectTransform.sizeDelta;
            plusGraphImages[p_nIndex].rectTransform.sizeDelta = new Vector2(a_CurrentSize.x, 0);
        }

        valueTexts[p_nIndex].text = ((float)Mathf.FloorToInt(p_fValue * 1000f) * 0.001f).ToString();
    }
}
