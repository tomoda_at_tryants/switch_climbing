﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugWindow : MonoBehaviour
{
    static DebugWindow  s_Instance;

    Vector2 m_ScrollPos;

    public static void  Create()
    {
        GameObject  a_Obj = new GameObject("DebugWindow");
        s_Instance = a_Obj.AddComponent<DebugWindow>();

        s_Instance.Init();
    }

    public void Init()
    {
        m_ScrollPos = Vector2.zero;
    }
}
