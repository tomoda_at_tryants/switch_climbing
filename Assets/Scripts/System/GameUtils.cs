﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameUtils
{

    public static string    eTAG
    {
        get
        {
            return typeof(GameUtils).ToString();
        }
    }

    /// <summary>
    /// Finds the game object and sort with tag.
    /// </summary>
    /// <returns>
    /// The game object and sort with tag.
    /// </returns>
    /// <param name='tag'>
    /// Tag.
    /// </param>
    public static GameObject[] FindGameObjectAndSortWithTag(string tag)
    {
        GameObject[] foundObs = GameObject.FindGameObjectsWithTag(tag);
        Array.Sort(foundObs, CompareOfNames);
        return foundObs;
    }

    private static int CompareOfNames(GameObject x, GameObject y)
    {
        return x.name.CompareTo(y.name);
    }

    /// <summary>
    /// Creates the bone hashtable.
    /// </summary>
    /// <returns>
    /// The bone hashtable.
    /// </returns>
    public static Hashtable CreateBoneHashtable(GameObject obj)
    {
        Hashtable table = new Hashtable();

        GameUtils.AppendBoneHashtable(ref table, obj.transform);

        return table;
    }

    public static void AppendBoneHashtable(ref Hashtable table, Transform tr)
    {
        table.Add(tr.name, tr);
        for (int i = 0; i < tr.childCount; i++)
        {
            GameUtils.AppendBoneHashtable(ref table, tr.GetChild(i));
        }
    }

    /// <summary>
    /// 任意の名前の子GameObjectを取得する
    /// </summary>
    /// <param name="p_Parent"></param>
    /// <param name="p_GameObjectHeadName"></param>
    /// <param name="p_nStartNo"></param>
    /// <param name="p_nNum"></param>
    /// <param name="p_nNoColumn"></param>
    /// <returns></returns>
    public static GameObject FindGameObjectByName(GameObject p_Parent, string p_GameObjectName)
    {
        Transform a_Tr = p_Parent.transform.Find(p_GameObjectName);
        if (a_Tr == null) return null;

        return a_Tr.gameObject;

    }

    /// <summary>
    /// シリアル番号付きのGameObjectの配列を取得する
    /// </summary>
    /// <param name="p_Parent"></param>
    /// <param name="p_GameObjectHeadName"></param>
    /// <param name="p_nStartNo"></param>
    /// <param name="p_nNum"></param>
    /// <param name="p_nNoColumn"></param>
    /// <returns></returns>
    public static GameObject[] FindGameObjectsByName(GameObject p_Parent, string p_GameObjectHeadName, int p_nStartNo, int p_nNum, int p_nNoColumn)
    {
        List<GameObject> a_List = new List<GameObject>();

        for (int i = 0; i < p_nNum; i++)
        {
            string a_IconName = string.Format("{0}{1:00}", p_GameObjectHeadName, (p_nStartNo + i));
            Transform a_Tr = p_Parent.transform.Find(a_IconName);
            if (a_Tr == null) break;

            a_List.Add(a_Tr.gameObject);
        }

        return a_List.ToArray();

    }

    /// <summary>
    /// 任意の名前の子ゲームオブジェクトからGameObjectの中の任意コンポーネントのリストを取得する
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    /// <param name="p_Parent"></param>
    /// <param name="p_GameObjectName"></param>
    /// <returns></returns>
    public static Type FindComponentByObjectName<Type>(GameObject p_Parent, string p_GameObjectName) where Type : MonoBehaviour
    {
        Transform a_Tr = FindChildRecursive(p_Parent.transform, p_GameObjectName);

        if (a_Tr != null)
        {
            MonoBehaviour[] a_MBList = a_Tr.gameObject.GetComponents<MonoBehaviour>();

            foreach (MonoBehaviour a_MB in a_MBList)
            {
                if (a_MB is Type)
                {
                    return a_MB as Type;
                }
            }
        }
        return null;
    }

    /// <summary>
    /// 再帰的に任意の名前の子オブジェクトを検索する
    /// </summary>
    /// <param name="p_Parent"></param>
    /// <param name="p_GameObjectName"></param>
    /// <returns></returns>
    public static Transform FindChildRecursive(Transform p_Parent, string p_GameObjectName)
    {
        if (p_Parent.name == p_GameObjectName)
        {
            return p_Parent;
        }

        for (int i = 0; i < p_Parent.childCount; i++)
        {
            Transform a_Tr = FindChildRecursive(p_Parent.GetChild(i), p_GameObjectName);
            if (a_Tr != null)
            {
                return a_Tr;
            }
        }

        return null;
    }

    /// <summary>
    /// アルファ値のアニメーション
    /// </summary>
    /// <returns></returns>
    public static IEnumerator AlphaAnim(Color p_Color, float p_fAniTime, float p_fDelay, float p_fFrom, float p_fTo, System.Action<Color> p_CallBack)
    {
        if (p_fDelay > 0) yield return new WaitForSeconds(p_fDelay);

        //	アルファ値のアニメーション
        float a_fTime = 0f;
        while (a_fTime < p_fAniTime)
        {
            a_fTime += Time.deltaTime;
            float a_fAlpha = Mathf.Lerp(p_fFrom, p_fTo, a_fTime * (1f / p_fAniTime));
            p_Color = new Color(p_Color.r, p_Color.g, p_Color.b, a_fAlpha);

            p_CallBack(p_Color);

            yield return 0;
        }
    }

    /// <summary>
    /// シリアル番号付きの子GameObjectの中の任意コンポーネントのリストを取得する
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    /// <param name="p_Parent"></param>
    /// <param name="p_GameObjectHeadName"></param>
    /// <param name="p_nStartNo"></param>
    /// <param name="p_nNum"></param>
    /// <param name="p_nNoColumn"></param>
    /// <returns></returns>
    public static List<Type> FindComponentsByObjectName<Type>(GameObject p_Parent, string p_GameObjectHeadName, int p_nStartNo, int p_nNum, int p_nNoColumn) where Type : MonoBehaviour
    {
        List<Type> a_List = new List<Type>();

        for (int i = 0; i < p_nNum; i++)
        {
            string a_IconName = string.Format("{0}{1:00}", p_GameObjectHeadName, (p_nStartNo + i));
            Transform a_Tr = p_Parent.transform.Find(a_IconName);
            if (a_Tr == null) break;

            MonoBehaviour[] a_MBList = a_Tr.GetComponents<MonoBehaviour>();

            foreach (MonoBehaviour a_MB in a_MBList)
            {
                if (a_MB is Type)
                {
                    a_List.Add(a_MB as Type);
                }
            }

        }

        return a_List;
    }




    /// <summary>
    /// Finds the transform.
    /// </summary>
    /// <returns>
    /// The transform.
    /// </returns>
    /// <param name='targetName'>
    /// Target name.
    /// </param>
    /// <param name='obj'>
    /// Object.
    /// </param>
    /// <param name='path'>
    /// Path.
    /// </param>
    public static Transform FindTransform(string targetName, Transform obj, string path)
    {
        if (path == null)
        {
            path = "";
        }
        //		Debug.Log(path + "/" + obj.name);
        path += "/";
        path += obj.name;

        if (obj.name.Equals(targetName) || path.Equals(targetName))
        {
            return obj;
        }

        for (int i = 0; i < obj.transform.childCount; i++)
        {
            Transform tg = FindTransform(targetName, obj.transform.GetChild(i), path);
            if (tg != null)
            {
                return tg;
            }
        }

        return null;
    }

    public static void SetTransformX(ref Transform tr, float x)
    {
        Vector3 pos = tr.localPosition;
        pos.x = x;
        tr.localPosition = pos;
    }
    public static void SetTransformY(ref Transform tr, float y)
    {
        Vector3 pos = tr.localPosition;
        pos.y = y;
        tr.localPosition = pos;
    }
    public static void SetTransformZ(ref Transform tr, float z)
    {
        Vector3 pos = tr.localPosition;
        pos.z = z;
        tr.localPosition = pos;
    }

    /// <summary>
    /// ローカル座標をワールド座表示変換する
    /// </summary>
    /// <param name="p_Tr"></param>
    /// <param name="p_LocalPos"></param>
    /// <returns></returns>
    public static Vector3 Local2World(Transform p_Tr, Vector3 p_LocalPos)
    {
        Vector3 a_WorldPos = p_Tr.localToWorldMatrix.MultiplyPoint(p_LocalPos);
        return a_WorldPos;
    }

    /// <summary>
    /// ワードル座標をローカル座標に変換する
    /// </summary>
    /// <param name="p_Tr"></param>
    /// <param name="p_WorldPos"></param>
    /// <returns></returns>
    public static Vector3 World2Local(Transform p_Tr, Vector3 p_WorldPos)
    {
        Vector3 a_LocalPos = p_Tr.worldToLocalMatrix.MultiplyPoint(p_WorldPos);
        return a_LocalPos;
    }

    /// <summary>
    /// ローカル座標を他のTransformのローカル座標に変換する
    /// </summary>
    /// <param name="p_Tr0"></param>
    /// <param name="p_Tr1"></param>
    /// <param name="p_LocalPos0">p_Tr0に対するローカル座標</param>
    /// <returns></returns>
    public static Vector3 Local2Local(Transform p_Tr0, Transform p_Tr1, Vector3 p_LocalPos0)
    {
        Vector3 a_WorldPos = p_Tr0.localToWorldMatrix.MultiplyPoint(p_LocalPos0);
        Vector3 a_LocalPos = p_Tr1.worldToLocalMatrix.MultiplyPoint(a_WorldPos);
        return a_LocalPos;
    }

    /*
        /// <summary>
        /// 任意タグのカメラを探す
        /// </summary>
        /// <param name="p_Tag"></param>
        /// <returns></returns>
        public static UICamera FindUICameraWithTag(string p_Tag)
        {
            UICamera a_Cam = null;
            GameObject[] a_CameraObj = GameObject.FindGameObjectsWithTag(p_Tag);
            foreach (GameObject a_Obj in a_CameraObj)
            {
                a_Cam = a_Obj.GetComponent<UICamera>();
                if (a_Cam != null) break;
            }

            return a_Cam;
        }


        /// <summary>
        /// 任意タグのカメラを探す
        /// </summary>
        /// <param name="p_Tag"></param>
        /// <returns></returns>
        public static Camera FindCameraWithTag(string p_Tag)
        {
            Camera a_Cam = null;
            GameObject[] a_CameraObj = GameObject.FindGameObjectsWithTag(p_Tag);
            foreach (GameObject a_Obj in a_CameraObj)
            {
                a_Cam = a_Obj.GetComponent<Camera>();
                if (a_Cam != null) break;
            }

            return a_Cam;
        }
    */

    /// <summary>
    /// Animations the state load.
    /// 
    /// 	string[][] c_AnimTable = 
    /// 		{
    /// 			new string[] { "Base Layer.Idle", "Base Layer.Idle" },
    /// 			new string[] { "Base Layer.Dead" },
    /// 			new string[] { "Base Layer.Damage" },
    /// 			new string[] { "Base Layer.Attack" },
    /// 		};
    ///
    ///                  |
    ///                  V
    ///
    ///		int[][] a_AnimState = 
    ///			{
    ///				new int[] { Animator.StringToHash("Base Layer.Idle"), Animator.StringToHash("Base Layer.Idle") },
    ///				new int[] { Animator.StringToHash("Base Layer.Dead") },
    ///				new int[] { Animator.StringToHash("Base Layer.Damage") },
    ///				new int[] { Animator.StringToHash("Base Layer.Attack") },
    ///			};
    /// 
    /// 
    /// </summary>
    /// <returns>
    /// The state load.
    /// </returns>
    /// <param name='c_AnimState'>
    /// C_ animation state.
    /// </param>
    public static int[][] AnimStateLoad(string[][] c_AnimTable)
    {
        int[][] a_AnimState = new int[c_AnimTable.Length][];
        for (int i = 0; i < c_AnimTable.Length; i++)
        {
            a_AnimState[i] = new int[c_AnimTable[i].Length];
            for (int j = 0; j < c_AnimTable[i].Length; j++)
            {
                a_AnimState[i][j] = Animator.StringToHash(c_AnimTable[i][j]);
            }
        }

        return a_AnimState;
    }

    /// <summary>
    /// アニメーションの速度を変更する
    /// </summary>
    /// <param name="p_TimeScale"></param>
    public static void AnimTimeScaleSet(GameObject p_Object, float p_TimeScale)
    {

        Animator[] a_Animators = p_Object.GetComponentsInChildren<Animator>();
        if (a_Animators != null)
        {
            foreach (Animator a_Animator in a_Animators)
            {
                a_Animator.speed = p_TimeScale;
            }
        }

    }
    /// <summary>
    /// アニメーションの再生タイムを変更する
    /// </summary>
    /// <param name="p_Time"></param>
    public static void AnimTimeSet(GameObject p_Object, float p_Time)
    {

        Animator[] a_Animators = p_Object.GetComponentsInChildren<Animator>();
        if (a_Animators != null)
        {
            foreach (Animator a_Animator in a_Animators)
            {
                a_Animator.playbackTime = p_Time;
            }
        }

    }


    /// <summary>
    /// アニメーションレイヤーのWeightを変更する
    /// </summary>
    /// <param name="p_TimeScale"></param>
    public static void AnimWeightSet(GameObject p_Object, string p_LayerName, float p_Weight)
    {

        Animator[] a_Animators = p_Object.GetComponentsInChildren<Animator>();
        if (a_Animators != null)
        {
            foreach (Animator a_Animator in a_Animators)
            {
                for (int i = 0; i < a_Animator.layerCount; i++)
                {
                    if (a_Animator.GetLayerName(i) == p_LayerName)
                    {
                        a_Animator.SetLayerWeight(i, p_Weight);
                        break;
                    }
                }
            }
        }

    }


    /// <summary>
    /// アニメーションレイヤーのWeightを変更する
    /// </summary>
    /// <param name="p_TimeScale"></param>
    public static float AnimWeightGet(GameObject p_Object, string p_LayerName)
    {

        Animator[] a_Animators = p_Object.GetComponentsInChildren<Animator>();
        if (a_Animators != null)
        {
            foreach (Animator a_Animator in a_Animators)
            {
                for (int i = 0; i < a_Animator.layerCount; i++)
                {
                    if (a_Animator.GetLayerName(i) == p_LayerName)
                    {
                        float p_Weight = a_Animator.GetLayerWeight(i);
                        return p_Weight;
                    }
                }
            }
        }

        return 0;

    }

    /// <summary>
    /// パーティクルの再生時間を取得する
    /// </summary>
    /// <returns></returns>
    public static float ParticleResumeTime_Get(GameObject p_Object)
    {
        float a_fResumeTime = 0f;
        ParticleSystem[] a_Particles = p_Object.GetComponentsInChildren<ParticleSystem>();
        if (a_Particles.Length > 0)
        {
            foreach (ParticleSystem p in a_Particles)
            {
                float f = p.main.duration + p.main.startLifetimeMultiplier; //	参照しているデータの再生時間取得
                                                        //	一番長い時間を保存
                if (f > a_fResumeTime) a_fResumeTime = f;
            }
        }

        return a_fResumeTime;
    }

    /// <summary>
    /// アニメーション野再生時間を取得する
    /// </summary>
    /// <param name="p_Obj"></param>
    /// <returns></returns>
    public static float AnimationResumeTime_Get(GameObject p_Obj)
    {
        float a_fResumeTime = 0f;
        Animator[] a_Animators = p_Obj.GetComponentsInChildren<Animator>();

        if (a_Animators.Length > 0)
        {
            foreach (Animator anim in a_Animators)
            {
                float a_fTime = anim.GetCurrentAnimatorStateInfo(0).length;
                if (a_fTime > a_fResumeTime) a_fResumeTime = a_fTime;
            }
        }

        return a_fResumeTime;
    }

    /*
        /// <summary>
        /// 「UIWidget」のサイズを設定する
        /// </summary>
        public static void WidgetSize_Set(GameObject p_Target, Vector2 p_Size)
        {
            if (p_Target == null)
            {
                Debug.LogError("WidgetSize_Set is null");
                return;
            }

            UIWidget a_Widget = p_Target.GetComponent<UIWidget>();
            a_Widget.width = Mathf.FloorToInt(p_Size.x);
            a_Widget.height = Mathf.FloorToInt(p_Size.y);
        }

        /// <summary>
        /// 「UIWidget」のサイズを取得する
        /// </summary>
        public static Vector2 WidgetSize_Get(GameObject p_Target)
        {
            if (p_Target == null)
            {
                Debug.LogError("WidgetSize_Set is null");
                return Vector2.zero;
            }

            UIWidget a_Widget = p_Target.GetComponent<UIWidget>();
            if (a_Widget == null) return Vector2.zero;
            Vector2 a_Size = new Vector2(a_Widget.width, a_Widget.height);

            return a_Size;
        }

        /// <summary>
        /// 「UIWidget」の「Depth」を設定する
        /// </summary>
        /// <param name="p_Target"></param>
        /// <param name="p_nDepth"></param>
        public static void WidgetDepth_Set(GameObject p_Target, int p_nDepth)
        {
            if (p_Target == null)
            {
                Debug.LogError("WidgetSize_Set is null");
                return;
            }

            UIWidget a_Widget = p_Target.GetComponent<UIWidget>();
            a_Widget.depth = p_nDepth;
        }
    */


    /// <summary>
    /// Enums the get.
    /// </summary>
    /// <returns>
    /// The get.
    /// </returns>
    /// <param name='p_MemberStr'>
    /// P_ member string.
    /// </param>
    /// <typeparam name='T'>
    /// The 1st type parameter.
    /// </typeparam>
    public static T EnumGet<T>(string p_MemberStr)
    {
        // typeof(MonsterExported.eAttr);
        // System.Enum.Parse(typeof(MonsterExported.eAttr), "Wood");

        T a_Res;
        try
        {
            a_Res = (T)System.Enum.Parse(typeof(T), p_MemberStr);
        }
        catch
        {
            //            TADebug.LogError(eTAG, "EnumGet Error " + p_MemberStr);
            p_MemberStr = Enum.GetNames(typeof(T))[0];
            a_Res = (T)System.Enum.Parse(typeof(T), p_MemberStr);
        }

        return a_Res;
    }

    public static void AttachChild(GameObject parent, GameObject child)
    {
        AttachChild(parent, child, Vector3.zero);
    }
    public static void AttachChild(GameObject parent, GameObject child, Vector3 p_LocalPos)
    {
        if (parent == null)
        {
            //			TADebug.LogError(eTAG, "AttachChild parent is null");
            child.transform.parent = null;
            return;
        }

        child.transform.SetParent(parent.transform);
        //		child.transform.parent = parent.transform;
        child.transform.localPosition = p_LocalPos;
        child.transform.localRotation = Quaternion.identity;
        child.transform.localScale = Vector3.one;

    }


    /// <summary>
    /// クラスTを持つGameObjectを生成する
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="p_Name"></param>
    /// <returns></returns>
    public static T CreateGameObject<T>(string p_Name = null)
        where T : Component
    {
        GameObject a_Obj = new GameObject();
        System.Type a_Type = typeof(T);
        T a_This = (T)a_Obj.AddComponent(a_Type);

        if (string.IsNullOrEmpty(p_Name))
        {
            a_Obj.name = a_Type.Name;
        }
        else
        {
            a_Obj.name = p_Name;
        }

        return a_This;
    }

    public static T CreateGameObject<T>(GameObject parent, Vector3 pos, string p_Name = null)
        where T : Component
    {
        GameObject a_Obj = new GameObject();
        GameUtils.AttachChild(parent, a_Obj, pos);

        System.Type a_Type = typeof(T);
        T a_This = (T)a_Obj.AddComponent(a_Type);

        if (string.IsNullOrEmpty(p_Name))
        {
            a_Obj.name = a_Type.Name;
        }
        else
        {
            a_Obj.name = p_Name;
        }

        return a_This;
    }

    public static GameObject InstantiateAndAttach(GameObject pref, GameObject parent, Vector3 pos)
    {
        GameObject a_Obj = (GameObject)GameObject.Instantiate(pref);
        a_Obj.transform.parent = parent.transform;
        a_Obj.transform.localPosition = pos;
        a_Obj.transform.localRotation = Quaternion.identity;
        a_Obj.transform.localScale = Vector3.one;

        return a_Obj;
    }

    public static T InstantiateAndAttach<T>(GameObject pref, GameObject parent, Vector3 pos)
    {
        GameObject a_Obj = (GameObject)GameObject.Instantiate(pref);
        a_Obj.transform.parent = parent.transform;
        a_Obj.transform.localPosition = pos;
        a_Obj.transform.localRotation = Quaternion.identity;
        a_Obj.transform.localScale = Vector3.one;

        T a_Res = a_Obj.GetComponent<T>();

        return a_Res;
    }

    /*
        public static void RemoveChildAll(GameObject parent, bool forceDestroy = false)
        {
            int a_nCount = parent.transform.childCount;
            for (int i = a_nCount - 1; i >= 0; i--)
            {
                GameObject child = parent.transform.GetChild(i).gameObject;
                DontDestroy a_DD = child.GetComponent<DontDestroy>();
                if (forceDestroy || (a_DD == null))
                {
                    GameObject.DestroyImmediate(child.gameObject);
                }
                child = null;
            }
        }

        /// <summary>
        /// 子ノードとの親子関係を切る（Destroyはしない）
        /// </summary>
        /// <param name="parent"></param>
        public static void UnlinkChildAll(GameObject parent)
        {
            int a_nCount = parent.transform.childCount;
            for (int i = a_nCount - 1; i >= 0; i--)
            {
                GameObject child = parent.transform.GetChild(i).gameObject;
                //			child.transform.parent = null;
                child.transform.parent = PanelManager.instance.alignmanLow.transform;   // 2016/2/29 選挙投票後、親無しのGameobject(SBoxCatListDrawのItem)が発生してしまう対策
                child = null;
            }
        }


        public static void LayerSet(GameObject p_Obj, int p_nLayerNo)
        {
            p_Obj.layer = p_nLayerNo;
            for (int i = 0; i < p_Obj.transform.childCount; i++)
            {
                LayerSet(p_Obj.transform.GetChild(i).gameObject, p_nLayerNo);
            }
        }

        public static void LayerSet(GameObject p_Obj, string p_LayerName)
        {
            int a_nLayerNo = LayerMask.NameToLayer(p_LayerName);
            LayerSet(p_Obj, a_nLayerNo);
        }



        /// <summary>
        /// Gets the size of the main game view.
        /// </summary>
        /// <returns>
        /// The main game view size.
        /// </returns>
        public static Vector2 ScreenSize()
        {
            if (PanelManager.instance != null)
            {
                Vector2 a_Res = new Vector2();
                a_Res.x = PanelManager.instance.width;
                a_Res.y = PanelManager.instance.height;
                return a_Res;
            }
            else
            {
    #if UNITY_EDITOR
                System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
                System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
                System.Object Res = GetSizeOfMainGameView.Invoke(null, null);
                return (Vector2)Res;
    #else
                Vector2 a_Res;
                a_Res.x = Screen.currentResolution.width;
                a_Res.y = Screen.currentResolution.height;

                Debug.Log("ScreenSize = " + a_Res.x + ", " + a_Res.y);
                return a_Res;
    #endif
            }
        }
    */

    /************** EasySerializer?ɑ???
        //
        // ?C?ӃI?u?W?F?N?g???V???A???C?Y???遁??Base64???????ɕϊ?
        //
        public static string SerializeObject<T>(T serializeObject)
        {
            Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");

            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, serializeObject);

            string a_Res = System.Convert.ToBase64String(ms.ToArray());

            ms.Close();

            ms = null;
            bf = null;

            return a_Res;
        }

        //
        // Base64?ɃV???A???C?Y???ꂽ?I?u?W?F?N?g?𕜌?????
        //
        public static T DeserializeObject<T>(string serializedString)
        {
            Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");

            MemoryStream ms = new MemoryStream(System.Convert.FromBase64String(serializedString));
            BinaryFormatter bf = new BinaryFormatter();

            T a_Res = (T)bf.Deserialize(ms);

            return a_Res;
        }

     ****/

    /// <summary>
    /// 現在日時を返す
    /// </summary>
    /// <returns></returns>
    public static DateTime Now()
    {
        return DateTime.Now;
    }


    //
    // UnixTime(1970/1/1?????̌o?ߕb??)????DateTime?ɕϊ?????
    //
    public static DateTime UnixTimeToDateTime(long p_UnixTime)
    {
        if (p_UnixTime == 0)
        {
            return DateTime.MinValue;
        }

        DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        int a_nDays = (int)(p_UnixTime / 60 / 60 / 24);
        int a_nHours = (int)((p_UnixTime / 60 / 60) % 24);
        int a_nMinites = (int)((p_UnixTime / 60) % 60);
        int a_nSeconds = (int)(p_UnixTime % 60);
        TimeSpan a_Span = new TimeSpan(a_nDays, a_nHours, a_nMinites, a_nSeconds);

        DateTime a_DateTime = (UNIX_EPOCH + a_Span);

        /*        if (def.c_bServerUnixTimeIsUTC)
                {
                    a_DateTime = a_DateTime.ToLocalTime();
                }
        */
        //		TADebug.Log("GameUtil", "UnixTimeToDateTime " + a_nDays + "," + a_nHours + ", " + a_nMinites + ", " + a_nSeconds);
        /*

                DateTime a_DateTime = UNIX_EPOCH.AddDays(a_nDays);
                a_DateTime = a_DateTime.AddHours(a_nHours);
                a_DateTime = a_DateTime.AddMinutes(a_nMinites);
                a_DateTime = a_DateTime.AddSeconds(a_nSeconds);
        */
        return a_DateTime;
    }

    /// <summary>
    /// 現在のUnixTime(1970/1/1からの経過秒数)を取得する
    /// </summary>
    /// <returns></returns>
    public static long UnixTime()
    {
        DateTime p_DateTime = DateTime.Now;
        return UnixTime(p_DateTime);
    }
    public static long UnixTime(bool p_bUtc)
    {
        DateTime p_DateTime = DateTime.Now;
        DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        if (p_DateTime.CompareTo(DateTime.MinValue) == 0)
            return 0;
        DateTime a_DateTime;
        if (p_bUtc)
            a_DateTime = p_DateTime.ToUniversalTime();
        else
            a_DateTime = p_DateTime;
        TimeSpan elapsedTime = a_DateTime - UNIX_EPOCH;
        long a_Span = (elapsedTime.Days * (24 * 60 * 60)) +
                        (elapsedTime.Hours * (60 * 60)) +
                        (elapsedTime.Minutes * 60) +
                        elapsedTime.Seconds;
        return a_Span;
    }

    /// <summary>
    /// 今日の午前０時(1日のはじめ)のUnixTime(1970/1/1からの経過秒数)を取得する
    /// </summary>
    /// <returns></returns>
    public static long UnixTimeDayStart(long p_UnixTime)
    {
        p_UnixTime /= (60 * 60 * 24);
        p_UnixTime *= (60 * 60 * 24);
        return p_UnixTime;
    }

    /// <summary>
    /// UnixTime(1970/1/1からの経過秒数)を取得する
    /// </summary>
    /// <param name="p_DateTime"></param>
    /// <returns></returns>
    public static long UnixTime(DateTime p_DateTime)
    {
        DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        if (p_DateTime.CompareTo(DateTime.MinValue) == 0)
        {
            return 0;
        }

        // UTC?
        DateTime a_DateTime;
        /*        if (def.c_bServerUnixTimeIsUTC)
                {
                    a_DateTime = p_DateTime.ToUniversalTime();
                }
                else
        */
        {
            a_DateTime = p_DateTime;
        }

        // UNIX?G?|?b?N?????̌o?ߎ??Ԃ??擾
        TimeSpan elapsedTime = a_DateTime - UNIX_EPOCH;
        long a_Span = (elapsedTime.Days * (24 * 60 * 60)) +
                        (elapsedTime.Hours * (60 * 60)) +
                        (elapsedTime.Minutes * 60) +
                        elapsedTime.Seconds;

        // ?o?ߕb???ɕϊ?
        return a_Span;
    }

    //
    // DateTime??UnixTime(1970/1/1?????̌o?ߕb??)?ɕϊ?????
    //
    public static double UnixMilliTime(DateTime p_DateTime)
    {
        DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        // UTC???Ԃɕϊ?
        DateTime a_DateTime;
        /*
                if (def.c_bServerUnixTimeIsUTC)
                {
                    a_DateTime = p_DateTime.ToUniversalTime();
                }
                else
        */
        {
            a_DateTime = p_DateTime;
        }

        // UNIX?G?|?b?N?????̌o?ߎ??Ԃ??擾
        TimeSpan elapsedTime = a_DateTime - UNIX_EPOCH;
        double a_Span = (elapsedTime.Days * (24 * 60 * 60)) +
                        (elapsedTime.Hours * (60 * 60)) +
                        (elapsedTime.Minutes * 60) +
                        (elapsedTime.Seconds) +
                        (elapsedTime.Milliseconds / 1000.0);

        // ?o?ߕb???ɕϊ?
        return a_Span;
    }

    /// <summary>
    /// エポック時間からの経過日数を取得する
    /// </summary>
    /// <param name="p_DateTime"></param>
    /// <returns></returns>
    public static int DayCountFromEpocTime(DateTime p_DateTime)
    {
        DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        TimeSpan a_Span = p_DateTime - UNIX_EPOCH;

        int a_nDays = a_Span.Days;

        return a_nDays;

    }

    /// <summary>
    /// エポック時間からの経過日数を取得する
    /// </summary>
    /// <param name="p_DateUnixTime"></param>
    /// <returns></returns>
    public static int DayCountFromEpocTime(long p_DateUnixTime)
    {
        DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        DateTime a_Date = UNIX_EPOCH.AddSeconds(p_DateUnixTime);

        TimeSpan a_Span = a_Date - UNIX_EPOCH;

        int a_nDays = a_Span.Days;

        return a_nDays;

    }

    /// <summary>
    /// UNIXタイムから年月を取得する（YYYYMM）
    /// </summary>
    /// <param name="p_UnixTime"></param>
    /// <returns></returns>
    public static int YearMonthFromEpocTime(long p_DateUnixTime)
    {
        DateTime a_Date = UnixTimeToDateTime(p_DateUnixTime);

        int a_nRes = ((a_Date.Year * 100) + a_Date.Month);

        return a_nRes;

    }


    //
    // ???݂????C?Ӄ~???b????DateTime???Ԃ?
    //
    public static DateTime AfterTime(DateTime p_BaseDateTime, double p_TimeMilliSecond)
    {
        p_TimeMilliSecond = ((int)p_TimeMilliSecond / 1000);

        int a_nDays = (int)(p_TimeMilliSecond / 60 / 60 / 24);
        int a_nHours = (int)((p_TimeMilliSecond / 60 / 60) % 24);
        int a_nMinites = (int)((p_TimeMilliSecond / 60) % 60);
        int a_nSeconds = (int)(p_TimeMilliSecond % 60);

        TimeSpan a_Span = new TimeSpan(a_nDays, a_nHours, a_nMinites, a_nSeconds);

        DateTime a_DateTime = p_BaseDateTime + a_Span;

        return a_DateTime;
    }


    /// <summary>
    /// ?c?莞?ԁi?~???b?j???擾????
    /// </summary>
    /// <param name="p_DateTime"></param>
    /// <returns></returns>
    public static double RemainTimeCalc(DateTime p_DateTime)
    {
        TimeSpan timeSpan = p_DateTime - DateTime.Now;

        double a_Span = (timeSpan.Days * (24 * 60 * 60 * 1000)) +
                        (timeSpan.Hours * (60 * 60 * 1000)) +
                        (timeSpan.Minutes * 60 * 1000) +
                        (timeSpan.Seconds * 1000) +
                        timeSpan.Milliseconds;


        return a_Span;
    }

    /// <summary>
    /// ２つの時刻の差（秒）を求める
    /// </summary>
    /// <param name="p_DateTime"></param>
    /// <returns></returns>
    public static double DiffMilliTimeCalc(DateTime p_DateTime0, DateTime p_DateTime1)
    {
        TimeSpan timeSpan = p_DateTime1 - p_DateTime0;

        double a_Span = (timeSpan.Days * (24 * 60 * 60 * 1000)) +
                        (timeSpan.Hours * (60 * 60 * 1000)) +
                        (timeSpan.Minutes * 60 * 1000) +
                        (timeSpan.Seconds * 1000) +
                        timeSpan.Milliseconds;


        return a_Span;
    }



    /// <summary>
    /// UUIDを取得する
    /// </summary>
    /// <returns></returns>
    public static string UuidGet()
    {
        string a_UUID = SystemInfo.deviceUniqueIdentifier;
        //		string a_UUID = System.Guid.NewGuid().ToString();
        return a_UUID;
    }

    /// <summary>
    /// エスケープされたUnicode文字列をデコードする
    /// </summary>
    /// <param name="p_Str"></param>
    /// <returns></returns>
    public static string String_DecodeFromEscapedUnicode(string p_Str)
    {
        string a_Name = "";
        int a_nIndex = 0;
        int a_nNextIndex = 0;
        while (a_nIndex < p_Str.Length)
        {
            if ((a_nNextIndex = p_Str.IndexOf("\\u", a_nIndex)) == -1)
            {
                a_Name += p_Str.Substring(a_nIndex);
                break;
            }

            a_Name += p_Str.Substring(a_nIndex, a_nNextIndex - a_nIndex);

            char c = (char)int.Parse(p_Str.Substring(a_nNextIndex + 2, 4), System.Globalization.NumberStyles.HexNumber);
            a_Name += c;

            a_nIndex = (a_nNextIndex + 6);

        }

        return a_Name;
    }

    /// <summary>
    /// 文字列中の
    /// 　\nを改行コードに
    /// 変換する
    /// </summary>
    /// <param name="p_Str"></param>
    /// <returns></returns>
    public static string String_UnEscape(string p_Str)
    {
        p_Str = p_Str.Replace("\\n", "\n");
        return p_Str;
    }


    /// <summary>
    /// 1970/1/1からの経過ミリ秒を返す
    /// </summary>
    /// <returns></returns>
    public static long currentTimeMillis()
    {
        long a_Time = 0;

        System.TimeSpan a_Span = System.DateTime.Now - new System.DateTime(1970, 1, 1);
        a_Time = (long)a_Span.TotalMilliseconds;

        return a_Time;
    }

    /// <summary>
    /// WebViewを開く
    /// </summary>
    /// <param name="p_URL"></param>
    public static void OpenURL(string p_URL, bool p_bEnableCloseButton, bool p_bEnableBackButton, System.Action p_Callback = null)
    {
#if false
        // WebViewダイアログを表示する
        DialogWebView a_Dialog = DialogWebView.Create(p_URL, p_bEnableCloseButton, p_bEnableBackButton, null, (dialog, buttonIndex) =>
        {
            dialog.Close(true, (dialog_) =>
            {
                if (p_Callback != null)
                {
                    p_Callback();
                }
            }
            );
        }
        ).Open();
#else
        // 外部ブラウザを起動する
        Application.OpenURL(p_URL);
#endif
    }


    /// <summary>
    /// 振動させる
    /// </summary>
    public static void Vibrate()
    {
#if ((UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR)
		Handheld.Vibrate();
#else
        //        TADebug.Log(eTAG, "Vibrate");
#endif
    }

    /// <summary>
    /// List<int>をランダムに並べ替える
    /// </summary>
    /// <param name="p_List"></param>
    public static void RandomSort(List<int> p_List)
    {
        for (int i = 0; i < p_List.Count; i++)
        {
            int a_nI = UnityEngine.Random.Range(0, p_List.Count);
            int a = p_List[i];
            int b = p_List[a_nI];
            p_List[i] = b;
            p_List[a_nI] = a;
        }
    }
    /// <summary>
    /// List<int>で定義されたリストの中からランダムで１つの値を選出する
    /// </summary>
    /// <param name="p_List"></param>
    public static int RandomChoose(List<int> p_RareList)
    {
        if ((p_RareList == null) || (p_RareList.Count == 0))
        {
            //            TADebug.LogError(eTAG, "RandomChoose null List");
            return 0;
        }

        int a_nIndex = UnityEngine.Random.Range(0, p_RareList.Count);
        return p_RareList[a_nIndex];
    }

    /// <summary>
    /// List<int>で定義された確率テーブルのインデックスを選出する
    /// 例）p_List[] = {50, 25, 15, 10}　=> 50%の確率でインデックス0が選出される
    /// </summary>
    /// <param name="p_List"></param>
    public static int RandomChooseIndex(List<int> p_RateList)
    {
        int a_nTotal = 0;
        foreach (int a_nN in p_RateList)
        {
            a_nTotal += a_nN;
        }

        int a_nRandom = UnityEngine.Random.Range(0, a_nTotal);

        int a_nIndex = 0;

        a_nTotal = 0;
        foreach (int a_nN in p_RateList)
        {
            a_nTotal += a_nN;
            if (a_nRandom < a_nTotal)
            {
                break;
            }
            a_nIndex++;
        }

        return a_nIndex;
    }

    /// <summary>
    /// List<int>で定義された確率テーブルのインデックスを選出する
    /// 例）p_List[] = {50, 25, 15, 10}　=> 50%の確率でインデックス0が選出される
    /// </summary>
    /// <param name="p_List"></param>
    public static int RandomChooseIndex(List<float> p_RateList)
    {
        float a_nTotal = 0;
        foreach (float a_nN in p_RateList)
        {
            a_nTotal += a_nN;
        }

        float a_nRandom = UnityEngine.Random.Range(0f, a_nTotal);

        int a_nIndex = 0;

        a_nTotal = 0;
        foreach (float a_nN in p_RateList)
        {
            a_nTotal += a_nN;
            if (a_nRandom <= a_nTotal)
            {
                break;
            }
            a_nIndex++;
        }

        return a_nIndex;
    }

    /// <summary>
    /// 成功したかを返す
    /// </summary>
    /// <param name="p_nSuccessRate">成功率</param>
    /// <returns></returns>
    public static bool Success_Check(int p_nSuccessRate)
    {
        //	０％以下は無条件で失敗
        if (p_nSuccessRate <= 0) return false;
        //	１００％以上は無条件で成功
        if (p_nSuccessRate >= 100) return true;

        //	０～１００
        int a_nRand = UnityEngine.Random.Range(1, 100 + 1);

        //	成功
        if (p_nSuccessRate >= a_nRand) return true;

        //	失敗
        return false;
    }

    /*
        /// <summary>
        /// 数値が上昇するアニメーション
        /// </summary>
        /// <param name="p_nTo"></param>
        /// <param name="p_CallBack"></param>
        /// <returns></returns>
        public static IEnumerator LabelUpdate_Lerp(UILabel p_Label, int p_nFrom, int p_nTo, System.Action<UILabel> p_CallBack)
        {
            //	数値の桁数
            int a_nDamageRank = p_nTo.ToString().Length;
            if (p_nTo < 0) a_nDamageRank -= 1;  //	「－」分

            //	１桁の時はアニメーションはしないで終了
            if (a_nDamageRank <= 1)
            {
                p_Label.text = p_nTo.ToString();
                //	ラベルを更新する
                if (p_CallBack != null) p_CallBack(p_Label);
                yield break;
            }

            float a_fTimeToRank = 0.08f;
            float a_fAniTime = a_nDamageRank * a_fTimeToRank;
            if (a_fAniTime > 0.4f) a_fAniTime = 0.4f;
            float a_fTime = 0f;

            while (a_fTime < a_fAniTime)
            {
                a_fTime += Time.deltaTime;
                int a_nNum = Mathf.FloorToInt(Mathf.Lerp(p_nFrom, p_nTo, a_fTime * (1f / a_fAniTime)));

                //	ラベル更新
                p_Label.text = a_nNum.ToString();

                //	ラベルを更新する
                if (p_CallBack != null) p_CallBack(p_Label);

                yield return 0;
            }
            //	ラベル更新
            p_Label.text = p_nTo.ToString();
        }
    */

    /// <summary>
    /// 指定したアルファ値にセットする
    /// </summary>
    /// <param name="p_Color"></param>
    /// <param name="p_nAlpha"></param>
    public static Color Alpha_Set(Color p_Color, int p_nAlpha)
    {
        if (p_nAlpha < 0) p_nAlpha = 0;
        if (p_nAlpha > 255) p_nAlpha = 255;

        return new Color(p_Color.r, p_Color.g, p_Color.b, (float)p_nAlpha / 255f);
    }

    /// <summary>
	/// 指定したアルファ値にセットする
	/// </summary>
	/// <param name="p_Color"></param>
	/// <param name="p_nAlpha"></param>
	public static Color Alpha_Set(Color p_Color, float p_fAlpha)
    {
        if (p_fAlpha < 0) p_fAlpha = 0;
        if (p_fAlpha > 1) p_fAlpha = 1;

        return new Color(p_Color.r, p_Color.g, p_Color.b, p_fAlpha);
    }

    /// <summary>
    ///	現在再生されているアニメーションの再生時間を取得する
    /// </summary>
    /// <returns></returns>
    public static float CurrentAnimatorTimeGet(Animator p_Animator, int p_nLayer = 0)
    {
        float a_fTime = 0f;
        if (p_Animator == null) return a_fTime;

        AnimatorStateInfo a_State = p_Animator.GetCurrentAnimatorStateInfo(p_nLayer);
        a_fTime = a_State.length;

        return a_fTime;
    }



    /// <summary>
    /// カンマ区切りの範囲指定を、数値の配列にする
    /// 例）"1,2,5-8,15-" -> List<int> { 1, 2, 5, 6, 7, 8, 15, ... p_nMaxValue};
    /// </summary>
    /// <param name="p_CommaSeparatedRange"></param>
    /// <param name="p_nMaxValue"></param>
    /// <returns></returns>
    public static List<int> ParseRange(string p_CommaSeparatedRange, int p_nMaxValue)
    {
        List<int> a_Range = new List<int>();

        string[] a_Token = p_CommaSeparatedRange.Split(',');
        foreach (string a_T in a_Token)
        {
            int a_Ch = a_T.IndexOf('-');
            // 範囲指定
            if (a_Ch != -1)
            {
                string[] a_SE = a_T.Split('-');
                int a_PS;
                int a_PE = p_nMaxValue;
                int.TryParse(a_SE[0], out a_PS);
                if (a_SE.Length >= 2)
                {
                    int.TryParse(a_SE[1], out a_PE);
                }
                if ((a_PS != 0) && (a_PE != 0))
                {
                    for (int i = a_PS; i <= a_PE; i++)
                    {
                        a_Range.Add(i);
                    }
                }
            }
            else
            {
                // 単一ページ指定
                int a_nPage;
                int.TryParse(a_T, out a_nPage);
                if (a_nPage != 0)
                {
                    a_Range.Add(a_nPage);
                }
            }
        }

        return a_Range;
    }

    /// <summary>
    /// 指定したデータからキーコードを取得する
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="p_Data"></param>
    /// <returns></returns>
    public static KeyCode GetKeyCode<T>(T p_Data)
    {
        KeyCode a_KeyCode = KeyCode.None;
        try
        {
            a_KeyCode = (KeyCode)Enum.Parse(typeof(KeyCode), p_Data.ToString());
        }
        catch
        {
        }

        return  a_KeyCode;
    }

    /// <summary>
    /// 指定したobject型の引数がクラスかどうか調べる
    /// </summary>
    public static bool GetClassType(object p_Object)
    {
        if (p_Object == null) return false;
        return p_Object.GetType().IsClass;
    }
}
