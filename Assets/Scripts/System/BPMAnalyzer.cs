﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using System;
using System.Text;

public class BPMAnalyzer
{
    private const int   c_nBpmMin = 60;
    private const int   c_nBpmMax = 400;

    /// <summary>
    /// 基本的なフリクエンシー値（44.1KB）
    /// </summary>
    private const int   c_nBaseFrequency = 44100;

    /// <summary>
    /// チャンネル数
    /// </summary>
    private const int c_nBaseChannels = 2;

    private const int c_nBaseSplitSampleSize = 3000;

    public struct stBpmMatchData
    {
        public int      bpm;
        public float    match;
    }

    private static stBpmMatchData[] m_BpmMatchDatas = new stBpmMatchData[c_nBpmMax - c_nBpmMin + 1];

    public static int   AnalyzeBpm(AudioClip p_Clip)
    {
        for (int i = 0; i < m_BpmMatchDatas.Length; i++)
        {
            m_BpmMatchDatas[i].match = 0f;
        }
        if (p_Clip == null) return -1;

        int frequency = p_Clip.frequency;
        int channels = p_Clip.channels;
        int splitFrameSize = Mathf.FloorToInt(((float)frequency / (float)c_nBaseFrequency) * ((float)channels / (float)c_nBaseChannels) * (float)c_nBaseSplitSampleSize);

        // Get all sample data from audioclip
        var allSamples = new float[p_Clip.samples * channels];
        p_Clip.GetData(allSamples, 0);

        // Create volume array from all sample data
        var volumeArr = CreateVolumeArray(allSamples, frequency, channels, splitFrameSize);

        // Search bpm from volume array
        int bpm = SearchBpm(volumeArr, frequency, splitFrameSize);
        
/*
        var strBuilder = new StringBuilder("BPM Match Data List\n");
        for (int i = 0; i < m_BpmMatchDatas.Length; i++)
        {
            strBuilder.Append("bpm : " + m_BpmMatchDatas[i].bpm + ", match : " + Mathf.FloorToInt(m_BpmMatchDatas[i].match * 10000f) + "\n");
        }
*/
        
        return bpm;
    }

    /// <summary>
    /// Create volume array from all sample data
    /// </summary>
    private static float[]  CreateVolumeArray(float[] allSamples, int frequency, int channels, int splitFrameSize)
    {
        // Initialize volume array
        var volumeArr = new float[Mathf.CeilToInt((float)allSamples.Length / (float)splitFrameSize)];
        int powerIndex = 0;

        // Sample data analysis start
        for (int sampleIndex = 0; sampleIndex < allSamples.Length; sampleIndex += splitFrameSize)
        {
            float sum = 0f;
            for (int frameIndex = sampleIndex; frameIndex < sampleIndex + splitFrameSize; frameIndex++)
            {
                if (allSamples.Length <= frameIndex)
                {
                    break;
                }
                // Use the absolute value, because left and right value is -1 to 1
                float absValue = Mathf.Abs(allSamples[frameIndex]);
                if (absValue > 1f)
                {
                    continue;
                }

                // Calculate the amplitude square sum
                sum += (absValue * absValue);
            }

            // Set volume value
            volumeArr[powerIndex] = Mathf.Sqrt(sum / splitFrameSize);
            powerIndex++;
        }

        // Representing a volume value from 0 to 1
        float maxVolume = volumeArr.Max();
        for (int i = 0; i < volumeArr.Length; i++)
        {
            volumeArr[i] = volumeArr[i] / maxVolume;
        }

        return volumeArr;
    }

    /// <summary>
    /// Search bpm from volume array
    /// </summary>
    private static int SearchBpm(float[] volumeArr, int frequency, int splitFrameSize)
    {
        // Create volume diff list
        var diffList = new List<float>();
        for (int i = 1; i < volumeArr.Length; i++)
        {
            diffList.Add(Mathf.Max(volumeArr[i] - volumeArr[i - 1], 0f));
        }

        // Calculate the degree of coincidence in each BPM
        int index = 0;
        float splitFrequency = (float)frequency / (float)splitFrameSize;
        for (int bpm = c_nBpmMin; bpm <= c_nBpmMax; bpm++)
        {
            float sinMatch = 0f;
            float cosMatch = 0f;
            float bps = (float)bpm / 60f;

            if (diffList.Count > 0)
            {
                for (int i = 0; i < diffList.Count; i++)
                {
                    sinMatch += (diffList[i] * Mathf.Cos(i * 2f * Mathf.PI * bps / splitFrequency));
                    cosMatch += (diffList[i] * Mathf.Sin(i * 2f * Mathf.PI * bps / splitFrequency));
                }

                sinMatch *= (1f / (float)diffList.Count);
                cosMatch *= (1f / (float)diffList.Count);
            }

            float match = Mathf.Sqrt((sinMatch * sinMatch) + (cosMatch * cosMatch));

            m_BpmMatchDatas[index].bpm = bpm;
            m_BpmMatchDatas[index].match = match;
            index++;
        }

        // Returns a high degree of coincidence BPM
        int matchIndex = Array.FindIndex(m_BpmMatchDatas, x => x.match == m_BpmMatchDatas.Max(y => y.match));

        return m_BpmMatchDatas[matchIndex].bpm;
    }
}
