﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private const string c_eTAG = "SoundManager";

    private const int c_nBgmMax = 2;
    private const int c_nSeMax = 5;

    private AudioSource currentBgmSource
    {
        get { return m_BgmSourceList[0]; }
        set { m_BgmSourceList[0] = value; }
    }

    /// <summary>
    /// 現在再生しているBGMの長さ長さ
    /// </summary>
    public AudioClip    currentBgmClip
    {
        get{ return m_BgmSourceList[0].clip; }
    }

    /// <summary>
    /// 現在再生しているBGMの長さ
    /// </summary>
    public float currentBgmLength
    {
        get
        {
            if (currentBgmSource.clip == null) return -1;
            return currentBgmSource.clip.length;
        }
    }

    /// <summary>
    /// 現在再生しているBGMの再生時間
    /// </summary>
    public float currentBgmTime
    {
        get
        {
            if (currentBgmSource.clip == null) return -1;
            return currentBgmSource.time;
        }
    }

    /// <summary>
    /// 再生中か
    /// </summary>
    public bool isPlaying
    {
        get
        {
            return currentBgmSource.isPlaying;
        }
    }

    private AudioSource addBgmSource
    {
        get { return m_BgmSourceList[1]; }
        set { m_BgmSourceList[1] = value; }
    }

    static SoundManager s_Instance;
    public static SoundManager instance
    {
        get
        {
            if (s_Instance == null) Create();
            return s_Instance;
        }

        set
        {
            if (s_Instance == null) Create();
            s_Instance = value;
        }
    }

    /// <summary>
    /// クロスフェード中か
    /// </summary>
    public bool isBgmCrossFading
    {
        get
        {
            foreach(AudioSource a_Source in m_BgmSourceList)
            {
                //  再生中でないならクロスフェードは行っていない
                if(!a_Source.isPlaying)
                {
                    return false;
                }
            }

            return true;
        }
    }

    /// <summary>
    /// BGM用
    /// </summary>
    private List<AudioSource>   m_BgmSourceList;

    /// <summary>
    /// SE用
    /// </summary>
    private List<AudioSource>   m_SeSourceList;

    /// <summary>
    /// AudioClipリスト
    /// </summary>
    private Dictionary<AudioEnum.eSE, AudioClip>    m_SeDic;
    private Dictionary<AudioEnum.eBGM, AudioClip>   m_BgmDic;

    public static void  Create()
    {
        if (s_Instance != null) return;

        GameObject  a_Obj = new GameObject("SoundManager");
        s_Instance = a_Obj.AddComponent<SoundManager>();

        DontDestroyOnLoad(a_Obj);

        s_Instance.Init();
    }

    public void Init()
    {
        gameObject.AddComponent<AudioListener>();

        m_SeSourceList = new List<AudioSource>();
        for(int i = 0; i < c_nSeMax; i++)
        {
            AudioSource a_SeSource = gameObject.AddComponent<AudioSource>();
            m_SeSourceList.Add(a_SeSource);
            a_SeSource.playOnAwake = false;
        }

        m_BgmSourceList = new List<AudioSource>();
        for(int i = 0; i < c_nBgmMax; i++)
        {
            AudioSource a_BgmSource = gameObject.AddComponent<AudioSource>();
            m_BgmSourceList.Add(a_BgmSource);
            a_BgmSource.loop = true;
        }

        m_BgmDic    = new Dictionary<AudioEnum.eBGM, AudioClip>();
        m_SeDic     = new Dictionary<AudioEnum.eSE, AudioClip>();

        object[] a_BgmList  = Resources.LoadAll(def.c_SoundAssetPath + "BGM");
        object[] a_SeList   = Resources.LoadAll(def.c_SoundAssetPath + "SE");

        foreach(AudioClip a_Bgm in a_BgmList)
        {
            AudioEnum.eBGM  a_Type = (AudioEnum.eBGM)Enum.Parse(typeof(AudioEnum.eBGM), a_Bgm.name);
            m_BgmDic.Add(a_Type, a_Bgm);
        }

        foreach (AudioClip a_Se in a_SeList)
        {
            AudioEnum.eSE   a_Type = (AudioEnum.eSE)Enum.Parse(typeof(AudioEnum.eSE), a_Se.name);
            m_SeDic.Add(a_Type, a_Se);
        }
    }

    public void PlaySE(AudioEnum.eSE p_Se, float p_fVolume = 1f, float p_fDelay = 0f)
    {
        if(!m_SeDic.ContainsKey(p_Se))
        {
            return;
        }

        AudioClip a_Clip = m_SeDic[p_Se];
        foreach(AudioSource a_Source in m_SeSourceList)
        {
            if(!a_Source.isPlaying)
            {
                a_Source.PlayOneShot(a_Clip, p_fVolume);
                return;
            }
        }
    }

    /// <summary>
    /// BGMを再生する（再生中のBGMは上書きされる）
    /// </summary>
    /// <param name="p_Bgm"></param>
    /// <param name="p_fVolume"></param>
    /// <param name="p_fFadeTime"></param>
    public void PlayBGM(AudioEnum.eBGM p_Bgm, bool p_bLoop, float p_fVolume = 1f, float p_fFadeTime = 0f, bool p_bCrossFade = false)
    {
        //  指定されたBGMが登録されていないか、現在クロスフェード中なら終了
        if (!m_BgmDic.ContainsKey(p_Bgm) || isBgmCrossFading)
        {
            return;
        }

        currentBgmSource.loop = p_bLoop;
        PlayBGM(p_Bgm, p_fVolume, p_fFadeTime, p_bCrossFade);
    }

    /// <summary>
    /// BGMを再生する（再生中のBGMは上書きされる）
    /// </summary>
    /// <param name="p_Bgm"></param>
    /// <param name="p_fVolume"></param>
    /// <param name="p_fFadeTime"></param>
    public void PlayBGM(AudioEnum.eBGM p_Bgm, float p_fVolume = 1f, float p_fFadeTime = 0f, bool p_bCrossFade = false)
    {
        //  指定されたBGMが登録されていないか、現在クロスフェード中なら終了
        if(!m_BgmDic.ContainsKey(p_Bgm) || isBgmCrossFading)
        {
            return;
        }

        if(p_bCrossFade)
        {
            StartCoroutine(PlayBGM_CrossFade(p_Bgm, p_fVolume, p_fFadeTime));
        }
        else
        {
            StartCoroutine(PlayBGM_Fade(p_Bgm, p_fVolume, p_fFadeTime));
        }
    }

    /// <summary>
    /// BGMをフェード形式で再生する
    /// </summary>
    /// <param name="p_Bgm"></param>
    /// <param name="p_fVolume"></param>
    /// <param name="p_fFadeTime"></param>
    /// <returns></returns>
    private IEnumerator PlayBGM_Fade(AudioEnum.eBGM p_Bgm, float p_fVolume = 1f, float p_fFadeTime = 0f)
    {
        //  フェード時間が指定されていない
        if (p_fFadeTime == 0)
        {
            currentBgmSource.clip = m_BgmDic[p_Bgm];
            currentBgmSource.volume = p_fVolume;

            currentBgmSource.Play();

            yield break;
        }
        else
        if(p_fFadeTime > 0)
        {
            currentBgmSource.clip     = m_BgmDic[p_Bgm];
            currentBgmSource.volume   = 0;

            currentBgmSource.Play();

            float   a_fTime = 0;
            while(a_fTime < p_fFadeTime)
            {
                a_fTime += Time.deltaTime;

                float   a_fVol = Mathf.Lerp(0f, p_fVolume, (1f / p_fFadeTime) * a_fTime);
                currentBgmSource.volume = a_fVol;

                yield return 0;
            }
            currentBgmSource.volume = p_fVolume;
        }
    }

    /// <summary>
    /// 現在再生されているBGMと指定されたBGMをクロスフェード形式で再生する
    /// </summary>
    /// <returns></returns>
    private IEnumerator PlayBGM_CrossFade(AudioEnum.eBGM p_Bgm, float p_fVolume = 1f, float p_fFadeTime = 0f)
    {
        if (p_fFadeTime == 0 || !currentBgmSource.isPlaying)
        {
            currentBgmSource.clip = m_BgmDic[p_Bgm];
            currentBgmSource.Play();

            yield break;
        }
        else
        if (p_fFadeTime > 0)
        {
            addBgmSource.volume      = 0;
            addBgmSource.clip        = m_BgmDic[p_Bgm];

            addBgmSource.Play();

            float a_fTime = 0;
            while(a_fTime < p_fFadeTime)
            {
                a_fTime += Time.deltaTime;

                float   a_fVol = Mathf.Lerp(0, p_fVolume, (1f / p_fFadeTime * a_fTime));
                addBgmSource.volume = a_fVol;

                a_fVol = Mathf.Lerp(p_fVolume, 0, (1f / p_fFadeTime * a_fTime));
                currentBgmSource.volume = a_fVol;

                yield return 0;
            }

            //  追加したBGMの
            currentBgmSource.clip     = null;
            currentBgmSource.clip     = addBgmSource.clip;
            currentBgmSource.volume   = addBgmSource.volume;
            currentBgmSource.time     = addBgmSource.time;

            currentBgmSource.Play();

            addBgmSource.clip = null;
        }

        yield return 0;
    }

    /// <summary>
    /// 再生されているSEを全て停止する
    /// </summary>
    public void PauseAllSE()
    {
        foreach(AudioSource a_Source in m_SeSourceList)
        {
            if(a_Source.isPlaying)  a_Source.Pause();
        }
    }

    /// <summary>
    /// BGMを停止する
    /// </summary>
    public void PauseBGM()
    {
        foreach(AudioSource a_Source in m_BgmSourceList)
        {
            if(a_Source.isPlaying) a_Source.Pause();
        }
    }

    public void StopAllSE()
    {
        foreach (AudioSource a_Source in m_SeSourceList)
        {
            if (a_Source.isPlaying) a_Source.Stop();
        }
    }

    public void StopAllBGM()
    {
        foreach (AudioSource a_Source in m_BgmSourceList)
        {
            if (a_Source.isPlaying) a_Source.Stop();
        }
    }

    /// <summary>
    /// 停止しているSEを全て再開する
    /// </summary>
    public void ResumeAllSE()
    {
        foreach (AudioSource a_Source in m_SeSourceList)
        {
            if (!a_Source.isPlaying)    a_Source.Play();
        }
    }

    /// <summary>
    /// 停止しているBGMを再生する
    /// </summary>
    public void ResumeBGM()
    {
        foreach (AudioSource a_Source in m_BgmSourceList)
        {
            if (!a_Source.isPlaying)    a_Source.Play();
        }
    }
}
