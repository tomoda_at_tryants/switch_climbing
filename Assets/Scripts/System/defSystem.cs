﻿using UnityEngine;
using System.Collections;

public class defSystem : MonoBehaviour {

	// Define Scene
	public enum Scene : int
	{
		// Main Scene
		TITLE,
	    MAIN,
		TEMPORARY,
	};

	public static string[]  c_SceneFileName =
	{
		// Main Scene
		"scn_title",
		"scn_main",
		"scn_temporary"
	};

    public static string    c_ProjectName       = "MyData";      //  プロジェクト名

	public static int c_FPS = 60;

	public static string	c_PrefabPath		= "Prefabs/";		// = Assets/Resources/Prefabs/
	public static string	c_AnimPath			= "Animations/";	// = Assets/Resources/Animations/
	public static string	c_BytesAssetPath	= "Bytes/";			// = Assets/Resources/Bytes/
	public static string	c_TextAssetPath		= "Texts/";			// = Assets/Resources/Texts/
	public static string	c_SoundAssetPath	= "Sounds/";		// = Assets/Resources/Sounds/
	public static string	c_TextureAssetPath	= "Textures/";		// = Assets/Resources/Texture/
	public static string	c_XmlPath			= "Xml/";			// = Assets/Resources/Xml/
    public static string    c_CSVPath           = "CSV/";

	public static string	c_StringTablePath_Jp	= c_TextAssetPath + "Strings_jp";
	public static string	c_StringTablePath_En	= c_TextAssetPath + "Strings_en";
}
