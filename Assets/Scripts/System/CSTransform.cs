﻿using UnityEngine;

public static class CSTransform
{
	/// <summary>
	/// X座標をセットする
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_X"></param>
	/// <param name="p_bLocal"></param>
	public static void SetPos_X(Transform p_Transform, float p_fX, bool p_bLocal = true)
	{
		if (p_Transform == null) return;
		Vector3 a_NewPos = Vector3.zero;
		if (p_bLocal)
		{
			a_NewPos = new Vector3(p_fX, p_Transform.localPosition.y, p_Transform.localPosition.z);
			p_Transform.localPosition = a_NewPos;
		}
		else
		{
			a_NewPos = new Vector3(p_fX, p_Transform.position.y, p_Transform.position.z);
			p_Transform.position = a_NewPos;
		}
	}

	/// <summary>
	/// X座標に加算する
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_X"></param>
	/// <param name="p_bLocal"></param>
	public static void AddPos_X(Transform p_Transform, float p_fAdd, bool p_bLocal = true)
	{
		if (p_Transform == null) return;
		Vector3 a_NewPos = Vector3.zero;
		if (p_bLocal)
		{
			a_NewPos = new Vector3(p_Transform.localPosition.x + p_fAdd, p_Transform.localPosition.y, p_Transform.localPosition.z);
			p_Transform.localPosition = a_NewPos;
		}
		else
		{
			a_NewPos = new Vector3(p_Transform.position.x + p_fAdd, p_Transform.position.y, p_Transform.position.z);
			p_Transform.position = a_NewPos;
		}
	}

	/// <summary>
	/// Y座標をセットする
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_Y"></param>
	/// <param name="p_bLocal"></param>
	public static void SetPos_Y(Transform p_Transform, float p_fY, bool p_bLocal = true)
	{
		if (p_Transform == null) return;
		Vector3 a_NewPos = Vector3.zero;
		if (p_bLocal)
		{
			a_NewPos = new Vector3(p_Transform.localPosition.x, p_fY, p_Transform.localPosition.z);
			p_Transform.localPosition = a_NewPos;
		}
		else
		{
			a_NewPos = new Vector3(p_Transform.position.x, p_fY, p_Transform.position.z);
			p_Transform.position = a_NewPos;
		}

	}

	/// <summary>
	/// Y座標に加算する
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_X"></param>
	/// <param name="p_bLocal"></param>
	public static void AddPos_Y(Transform p_Transform, float p_fAdd, bool p_bLocal = true)
	{
		if (p_Transform == null) return;
		Vector3 a_NewPos = Vector3.zero;
		if (p_bLocal)
		{
			a_NewPos = new Vector3(p_Transform.localPosition.x, p_Transform.localPosition.y + p_fAdd, p_Transform.localPosition.z);
			p_Transform.localPosition = a_NewPos;
		}
		else
		{
			a_NewPos = new Vector3(p_Transform.position.x, p_Transform.position.y + p_fAdd, p_Transform.position.z);
			p_Transform.position = a_NewPos;
		}
	}

	/// <summary>
	/// Z座標をセットする
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_Z"></param>
	/// <param name="p_bLocal"></param>
	public static void SetPos_Z(Transform p_Transform, float p_fZ, bool p_bLocal = true)
	{
		if (p_Transform == null) return;
		Vector3 a_NewPos = Vector3.zero;
		if (p_bLocal)
		{
			a_NewPos = new Vector3(p_Transform.localPosition.x, p_Transform.localPosition.y, p_fZ);
			p_Transform.localPosition = a_NewPos;
		}
		else
		{
			a_NewPos = new Vector3(p_Transform.position.x, p_Transform.position.y, p_fZ);
			p_Transform.position = a_NewPos;
		}

	}

	/// <summary>
	/// Z座標に加算する
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_Z"></param>
	/// <param name="p_bLocal"></param>
	public static void AddPos_Z(Transform p_Transform, float p_fAdd, bool p_bLocal = true)
	{
		if (p_Transform == null) return;
		Vector3 a_NewPos = Vector3.zero;
		if (p_bLocal)
		{
			a_NewPos = new Vector3(p_Transform.localPosition.x, p_Transform.localPosition.y, p_Transform.localPosition.z + p_fAdd);
			p_Transform.localPosition = a_NewPos;
		}
		else
		{
			a_NewPos = new Vector3(p_Transform.position.x, p_Transform.position.y, p_Transform.position.z + p_fAdd);
			p_Transform.position = a_NewPos;
		}

	}

	/// <summary>
	/// Xスケールをセットする
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_X"></param>
	public static void SetScale_X(Transform p_Transform, float p_fX)
	{
		if (p_Transform == null) return;
		Vector3 a_NewScale = new Vector3(p_fX, p_Transform.localScale.y, p_Transform.localScale.z);
		p_Transform.localScale = a_NewScale;
	}

	/// <summary>
	/// Yスケールをセットする
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_Y"></param>
	public static void SetScale_Y(Transform p_Transform, float p_fY)
	{
		if (p_Transform == null) return;
		Vector3 a_NewScale = new Vector3(p_Transform.localScale.x, p_fY, p_Transform.localScale.z);
		p_Transform.localScale = a_NewScale;
	}

	/// <summary>
	/// Zスケールをセットする
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_Z"></param>
	public static void SetScale_Z(Transform p_Transform, float p_fZ)
	{
		if (p_Transform == null) return;
		Vector3 a_NewScale = new Vector3(p_Transform.localScale.x, p_Transform.localScale.y, p_fZ);
		p_Transform.localScale = a_NewScale;
	}

	/// <summary>
	/// Xスケールに加算する
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_X"></param>
	public static void AddScale_X(Transform p_Transform, float p_fX)
	{
		if (p_Transform == null) return;
		Vector3 a_NewScale = new Vector3(p_Transform.localScale.x + p_fX, p_Transform.localScale.y, p_Transform.localScale.z);
		p_Transform.localScale = a_NewScale;
	}

	/// <summary>
	/// Yスケールに加算する
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_Y"></param>
	public static void AddScale_Y(Transform p_Transform, float p_fY)
	{
		if (p_Transform == null) return;
		Vector3 a_NewScale = new Vector3(p_Transform.localScale.x, p_Transform.localScale.y + p_fY, p_Transform.localScale.z);
		p_Transform.localScale = a_NewScale;
	}

	/// <summary>
	/// Zスケールに加算する
	/// </summary>
	/// <param name="p_Transform"></param>
	/// <param name="p_Z"></param>
	public static void AddScale_Z(Transform p_Transform, float p_fZ)
	{
		if (p_Transform == null) return;
		Vector3 a_NewScale = new Vector3(p_Transform.localScale.x, p_Transform.localScale.y, p_Transform.localScale.z + p_fZ);
		p_Transform.localScale = a_NewScale;
	}

	/// <summary>
	///	X方向のオイラー角をセットする
	/// </summary>
	public static void SetEuler_X(Transform p_Transform, float p_fX)
	{
		if (p_Transform == null) return;
		Vector3 a_NewAngle = new Vector3(p_fX, p_Transform.eulerAngles.y, p_Transform.eulerAngles.z);
		p_Transform.eulerAngles = a_NewAngle;
	}

	/// <summary>
	///	Y方向のオイラー角をセットする
	/// </summary>
	public static void SetEuler_Y(Transform p_Transform, float p_fY)
	{
		if (p_Transform == null) return;
		Vector3 a_NewAngle = new Vector3(p_Transform.eulerAngles.x, p_fY, p_Transform.eulerAngles.z);
		p_Transform.eulerAngles = a_NewAngle;
	}

	/// <summary>
	///	Z方向のオイラー角をセットする
	/// </summary>
	public static void SetEuler_Z(Transform p_Transform, float p_fZ)
	{
		if (p_Transform == null) return;
		Vector3 a_NewAngle = new Vector3(p_Transform.eulerAngles.x, p_Transform.eulerAngles.y, p_fZ);
		p_Transform.eulerAngles = a_NewAngle;
	}

	/// <summary>
	///	X方向のオイラー角に加算する
	/// </summary>
	public static void AddEuler_X(Transform p_Transform, float p_fX)
	{
		if (p_Transform == null) return;
		Vector3 a_NewAngle = new Vector3(p_Transform.eulerAngles.x + p_fX, p_Transform.eulerAngles.y, p_Transform.eulerAngles.z);
		p_Transform.eulerAngles = a_NewAngle;
	}

	/// <summary>
	///	Y方向のオイラー角に加算する
	/// </summary>
	public static void AddEuler_Y(Transform p_Transform, float p_fY)
	{
		if (p_Transform == null) return;
		Vector3 a_NewAngle = new Vector3(p_Transform.eulerAngles.x, p_Transform.eulerAngles.y + p_fY, p_Transform.eulerAngles.z);
		p_Transform.eulerAngles = a_NewAngle;
	}

	/// <summary>
	///	Z方向のオイラー角に加算する
	/// </summary>
	public static void AddEuler_Z(Transform p_Transform, float p_fZ)
	{
		if (p_Transform == null) return;
		Vector3 a_NewAngle = new Vector3(p_Transform.eulerAngles.x, p_Transform.eulerAngles.y, p_Transform.eulerAngles.z + p_fZ);
		p_Transform.eulerAngles = a_NewAngle;
	}

	/// <summary>
	///	２点間の角度をオイラーで取得する
	/// </summary>
	/// <param name="p_T1"></param>
	/// <param name="p_T2"></param>
	/// <returns></returns>
	public static float Atan2_Get_Euler(Transform p_T1, Transform p_T2)
	{
		if (p_T1 == null || p_T2 == null) return 0f;

		float a_fAngle = Mathf.Atan2(p_T1.localPosition.y - p_T2.localPosition.y,
									 p_T1.localPosition.x - p_T2.localPosition.x);

		return a_fAngle * Mathf.Rad2Deg;
	}
}
