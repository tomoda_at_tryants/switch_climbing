﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemplateCanvas : CanvasBehaviour
{
    public const string eTAG = "TemplateCanvas";
    public const string c_PrefabName = "UICanvas";

    public RectTransform    panelLow;
    public RectTransform    panelMid;
    public RectTransform    panelHigh;

    static TemplateCanvas s_Instance;
    /// <summary>
    /// インスタンス
    /// </summary>
    public static TemplateCanvas    instance
    {
        get
        {
            if (s_Instance == null) return Create();
            return  s_Instance;
        }
    }

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static TemplateCanvas    Create()
    {
        if (s_Instance != null) return s_Instance;

        try
        {
            GameObject  a_Obj = GameObject.Find("Common/" + c_PrefabName);
            if (a_Obj != null)
            {
                s_Instance = a_Obj.GetComponent<TemplateCanvas>();
                DontDestroyOnLoad(s_Instance);

                return s_Instance;
            }
        }
        catch
        {
            return null;
        }

        s_Instance = ResourceManager.PrefabLoadAndInstantiate<TemplateCanvas>("Common/" + c_PrefabName);
        s_Instance.Init();

        DontDestroyOnLoad(s_Instance);

        return s_Instance;
    }

    public override void    Init()
    {
        Clean();

        s_Instance.name = c_PrefabName;
    }

    void Clean()
    {
        if(panelLow.childCount > 0)
        {
            for(int i = panelLow.childCount - 1; i > 0; i--)
            {
                Destroy(panelLow.GetChild(i));
            }
        }

        if (panelMid.childCount > 0)
        {
            for (int i = panelMid.childCount - 1; i > 0; i--)
            {
                Destroy(panelMid.GetChild(i));
            }
        }

        if (panelHigh.childCount > 0)
        {
            for (int i = panelHigh.childCount - 1; i > 0; i--)
            {
                Destroy(panelHigh.GetChild(i));
            }
        }
    }
}
