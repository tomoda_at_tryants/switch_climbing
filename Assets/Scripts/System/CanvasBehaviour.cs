﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基底のキャンバスクラス
/// </summary>
public abstract class CanvasBehaviour : MonoBehaviour
{
    public Canvas   canvas;

    public abstract void Init();
}
