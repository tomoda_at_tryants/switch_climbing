﻿using System.Collections.Generic;
	
/// <summary>
/// オーディオ名を定数で管理するクラス
/// <summary>
public class AudioEnum
{
	
    public enum eBGM
    {
        Dummy = -1,
	
        Max,
    }
	
    public enum eSE
    {
        Dummy = -1,
	
        se_catch,
        Max,
    }
	
}
