﻿using UnityEngine;
	
/// <summary>
/// レイヤーを定数で管理するクラス
/// <summary>
public class LayerEnum
{
	
    public enum eLayer
    {
        Dummy = -1,
	
        Default,
        TransparentFX,
        IgnoreRaycast,
        Water,
        UI,
        TemplateCanvas,
        ScrollObj,
        Map,
        StageObject,
        Max,
    }
	
    public static int  GetLayerNum(eLayer p_Layer, bool p_bForCullingMask)
    {
        if (p_Layer == eLayer.Dummy || p_Layer == eLayer.Max)     return -1;
        if(p_bForCullingMask)
        {
            return 1 << LayerMask.NameToLayer(p_Layer.ToString());
        }
        else
        {
            return  LayerMask.NameToLayer(p_Layer.ToString());
        }
    }
}
