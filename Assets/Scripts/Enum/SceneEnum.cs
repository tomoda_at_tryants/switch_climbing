﻿using UnityEngine;
	
/// <summary>
/// シーン名を定数で管理するクラス
/// <summary>
public class SceneEnum
{
	
    public enum eScene
    {
        Dummy = -1,
	
        scn_boot,
        scn_title,
        scn_main,
        scn_switchtest,
        scn_temporary,
        scn_MapEditor,
        stage_001,
        stage_002,
        Max,
    }
}
