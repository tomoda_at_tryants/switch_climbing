﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using nn.hid;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scn_main : SceneBehaviour<scn_main.eState>
{
    //  ステート処理用
    public enum eState : int
    {
        None,

        Init,
        Ready,
        Play,
        GameOver,
        GameClear,

        Max
    }

    /// <summary>
    /// ゲームマネージャー
    /// </summary>
    GameManager m_GameManager;

    /// <summary>
    /// 「void Start()」で呼ばれる
    /// </summary>
    protected override void Start_Exec(System.Action<eState> p_BeginFirstState)
    {
        p_BeginFirstState(eState.Init);
    }

    void State_Init()
    {
        StartCoroutine(State_Init_Coroutine());
    }
    IEnumerator State_Init_Coroutine()
    {
        //  基本的なデータの初期化
        def.Initialize();   //  SwitchApiManager等もここで初期化しているので必ず呼び出す

        bool    a_bWait = true;
        m_GameManager = GameManager.Create(1,
            () =>
            {
                a_bWait = false;
            }
        );
        //  ゲームデータの初期化が完了するまで待機
        while (a_bWait) yield return 0;

        Button a_ReloadButton = ResourceManager.PrefabLoadAndInstantiate<Button>("ReloadButton");
        a_ReloadButton.transform.SetParent(TemplateCanvas.instance.panelHigh.transform);
        a_ReloadButton.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;

        a_ReloadButton.onClick.AddListener(ButtonEvent_Reload);

        StateChange(eState.Ready);
    }

    void State_Ready()
    {
        StartCoroutine(State_Ready_Coroutine());
    }
    IEnumerator State_Ready_Coroutine()
    {
        //  プレイヤーが両腕とも掴んでいる状態になるまで待機
        while(!m_GameManager.player.isEnableGameStart)
        {
            //  カメラは追跡する
            m_GameManager.mapManager.TrackingObject_Exec();
            GameCanvas.instance.TrackingObject();

            //  腕のサイズと角度の更新
            foreach(PlayerArm a_Arm in m_GameManager.player.armList)
            {
                a_Arm.SizeUpdate();
                a_Arm.SetAngleToHand();
            }

            yield return 0;
        }

        //  入力の準備が完了した段階で掴む
        foreach(PlayerArm a_Arm in m_GameManager.player.armList)
        {
            a_Arm.InputCatch_Exec();
        }

        StateChange(eState.Play);
    }

    void State_Play()
    {
        StartCoroutine(State_Play_Coroutine());
    }
    IEnumerator State_Play_Coroutine()
    {
        while(true)
        {
            m_GameManager.Update_Exec();

            if(m_GameManager.isGameClear)
            {
                StateChange(eState.GameClear);
            }

            yield return 0;
        }
    }

    void State_GameClear()
    {
        StartCoroutine(State_GameClear_Coroutine());
    }
    IEnumerator State_GameClear_Coroutine()
    {
        while(true)
        {
            foreach (PlayerArm a_Arm in m_GameManager.player.armList)
            {
                a_Arm.SetAngleToHand();
                a_Arm.SizeUpdate();
            }

            yield return 0;
        }
    }

    void Update()
    {
        if(SwitchApiManager.instance.NpadMan.PushButtonCheck(NpadButton.Plus))
        {
            ButtonEvent_Reload();
        }
    }

    public void ButtonEvent_Reload()
    {
        Destroy(TemplateCanvas.instance.gameObject);
        //  シーンのロード
        SceneManager.LoadSceneAsync(SceneEnum.eScene.scn_main.ToString(), LoadSceneMode.Single);
    }
}
