﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEndPanel : MonoBehaviour
{
    public enum eType
    {
        None = -1,

        GameClear,
        GameOver
    }

    [SerializeField]
    eType m_Type;

    public List<stTextType> textTypeList;

    public Button m_RetryButton;
    public Button m_EndButton;

    Dictionary<eType, stTextType>   GetDictionary_TextType()
    {
        Dictionary<eType, stTextType> a_Dic = new Dictionary<eType, stTextType>();
        foreach(stTextType a_TextType in textTypeList)
        {
            a_Dic.Add(a_TextType.type, a_TextType);
        }

        return a_Dic;
    }
    

    [System.Serializable]
    public struct stTextType
    {
        public Text text;
        public eType type;

        public stTextType(Text p_Text, eType p_Mode)
        {
            text = p_Text;
            type = p_Mode;
        }

        public void SetEnable(bool p_bEnable)
        {
            text.enabled = p_bEnable;
        }
    }

    public static GameEndPanel  Create(eType p_Mode, Transform p_Parent)
    {
        GameEndPanel    me = ResourceManager.PrefabLoadAndInstantiate<GameEndPanel>("GameEndPanel");
        me.Init(p_Mode, p_Parent);

        return me;
    }

    public void Init(eType p_Mode, Transform p_Parent)
    {
        m_Type = p_Mode;
        GetDictionary_TextType()[m_Type].SetEnable(true);

        transform.SetParent(p_Parent);
        transform.localScale = Vector3.one;
        transform.localPosition = Vector3.zero;
    }

    public void Event_Retry()
    {
        Destroy(TemplateCanvas.instance.gameObject);
        //  シーンのロード
        SceneManager.LoadSceneAsync(SceneEnum.eScene.scn_main.ToString(), LoadSceneMode.Single);
    }

    public void Event_End()
    {
        Debug.Log("Event_End()");
    }
}
