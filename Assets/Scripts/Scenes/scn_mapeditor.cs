﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class scn_mapeditor : MonoBehaviour
{
    public static string    eTAG
    {
        get { return typeof(scn_mapeditor).ToString(); }
    }

    [SerializeField]
    RectTransform m_TitlePanel;

    [SerializeField]
    Button m_StartButton;

    [SerializeField]
    InputField m_StageNumField;

    [SerializeField]
    GameObject m_WaitDialog;

    MapManager m_MapManager;

    bool m_bEditStop;

    /// <summary>
    /// クリックのチェックと座標を取得する
    /// </summary>
    /// <returns></returns>
    public bool Check_ClickPosAndGetPosition(ref Vector2 p_Pos)
    {
        bool a_bClick = Input.GetMouseButtonDown(0);
        if (a_bClick)
        {
            p_Pos = Input.mousePosition;
        }

        return a_bClick;
    }

    // Use this for initialization
    void Start()
    {
        m_bEditStop = true;

        m_TitlePanel.gameObject.SetActive(true);

        //  イベント登録
        m_StageNumField.onValueChanged.AddListener(Event_InputStageNum);
    }

    void Init()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void LateUpdate()
    {
        if (m_bEditStop) return;

        float a_fMoveValue = 20;

        Transform a_MapCamera = m_MapManager.canvasCamera.transform;
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (a_MapCamera.localPosition.x <= (Screen.width * (m_MapManager.mapSize.width - 1)))
            {
                CSTransform.AddPos_X(a_MapCamera, a_fMoveValue);
            }
        }
        else
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (a_MapCamera.localPosition.x >= 0 + a_fMoveValue)
            {
                CSTransform.AddPos_X(a_MapCamera, -a_fMoveValue);
            }
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (a_MapCamera.localPosition.y <= (Screen.height * (m_MapManager.mapSize.height - 1)))
            {
                CSTransform.AddPos_Y(a_MapCamera, a_fMoveValue);
            }
        }
        else
        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (a_MapCamera.localPosition.y >= 0 + a_fMoveValue)
            {
                CSTransform.AddPos_Y(a_MapCamera, -a_fMoveValue);
            }
        }

        //  カメラの移動範囲の制御
        Vector3 a_LimitPos = a_MapCamera.localPosition;
        a_LimitPos.x = Mathf.Clamp(a_LimitPos.x, 0, Screen.width * (m_MapManager.mapSize.width - 1));
        a_LimitPos.y = Mathf.Clamp(a_LimitPos.y, 0, Screen.height * (m_MapManager.mapSize.height - 1));

        a_MapCamera.localPosition = a_LimitPos;
        GameCanvas.instance.canvasCamera.transform.localPosition = a_LimitPos;

#if true
        //  クリック座標をマップのインデックスに置き換える
        Vector2 a_ClickPos = Vector2.zero;
        if (Check_ClickPosAndGetPosition(ref a_ClickPos))
        {
            //  カメラからRayを飛ばす
            Ray             a_Ray           = GameCanvas.instance.canvasCamera.ScreenPointToRay(a_ClickPos);
            RaycastHit2D[]  a_Hits          = Physics2D.RaycastAll(a_Ray.origin, a_Ray.direction, Mathf.Infinity);
            CatchObject     a_CatchObject   = null;

            foreach(RaycastHit2D a_Hit in a_Hits)
            {
                try
                {
                    a_CatchObject = a_Hit.collider.GetComponent<CatchObject>();
                    a_CatchObject.TouchEvent();

                    break;  //  最初に取得したオブジェクトで決定する
                }
                catch
                {
                    continue;
                }
            }

            //  タッチした座標にオブジェクトが存在しない時は新規に生成する
            if(a_CatchObject == null)
            {
                m_MapManager.CreateObjectToTouch(a_ClickPos);
            }
        }
#endif
    }

    int m_nStageNum;
    string m_StageName
    {
        get
        {
            string a_Str = m_nStageNum.ToString();
            return "stage_" + a_Str.ToString().PadLeft(3, '0');
        }
    }

    /// <summary>
    /// ステージ番号が入力されている時のイベント
    /// </summary>
    public void Event_InputStageNum(string p_InputText)
    {
        if (p_InputText.Length > 3)
        {
            p_InputText = p_InputText.Remove(p_InputText.Length - 1);
        }

        for (int i = p_InputText.Length - 1; i >= 0; i--)
        {
            if (!int.TryParse(p_InputText[i].ToString(), out m_nStageNum))
            {
                p_InputText.Remove(i);
            }
        }

        if (string.IsNullOrEmpty(p_InputText)) return;

        m_StageNumField.text = p_InputText;
        m_nStageNum = int.Parse(p_InputText);

//        bool a_bEnableFile = CSVManager.isEnableFile(m_StageName);
        m_StartButton.interactable = !string.IsNullOrEmpty(m_StageName);
    }
    
    public void Event_StartButton()
    {
        m_TitlePanel.gameObject.SetActive(false);

        m_MapManager = MapManager.Create(m_nStageNum, true,
            //  マップのロード完了時に呼ばれるコールバック
            () =>
            {
                Init();
                m_bEditStop = false;
            }
        );
    }

    public void Event_SaveButton()
    {
        //  マップサイズを保存
        m_MapManager.mapSize.SaveData(m_nStageNum);
        //  ステージデータを保存
        m_MapManager.SaveStageData();
    }
}
