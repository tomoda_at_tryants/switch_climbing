﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scn_title : SceneBehaviour<scn_title.eState>
{
    public enum eState
    {
        None,

        Init,
        Main,
        End,

        Max
    }

    protected override void Start_Exec(Action<eState> p_BeginFirstState)
    {
        p_BeginFirstState(eState.Init);
    }

    void State_Init()
    {
        StartCoroutine(State_Init_Coroutine());
    }
    IEnumerator State_Init_Coroutine()
    {
        //  基本的なデータの初期化
        def.Initialize();   //  SwitchApiManager等もここで初期化しているので必ず呼び出す

        StateChange(eState.Main);

        yield return 0;
    }

    void State_Main()
    {
        StartCoroutine(State_Main_Coroutine());
    }
    IEnumerator State_Main_Coroutine()
    {
        StateChange(eState.End);

        yield return 0;
    }

    void State_End()
    {
        StartCoroutine(State_End_Coroutine());
    }
    IEnumerator State_End_Coroutine()
    {
        SceneChange(SceneEnum.eScene.scn_main);

        yield return 0;
    }
}
