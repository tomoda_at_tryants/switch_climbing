﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scn_boot : SceneBehaviour<scn_boot.eState>
{
    public enum eState
    {
        None,

        Init,
        Main,
        End,

        Max
    }


    /// <summary>
    /// 「void Start()」で呼ばれる
    /// </summary>
    protected override void Start_Exec(System.Action<eState> p_BeginFirstState)
    {
        p_BeginFirstState(eState.Init);
    }

    void State_Init()
    {
        StartCoroutine(State_Init_Coroutine());
    }
    IEnumerator State_Init_Coroutine()
    {
        //  基本的なデータの初期化
        def.Initialize();   //  SwitchApiManager等もここで初期化しているので必ず呼び出す

        bool    a_bWait = true;
        //  スプラッシュの表示完了を検知
        WaitShowCompletedSplashScreen(
            () =>
            {
                a_bWait = false;
            }
        );

        //  スプラッシュ画面の表示が完了するまで待機
        while (a_bWait)
        {
            yield return 0;
        }

        StateChange(eState.Main);
    }

    void State_Main()
    {
        StartCoroutine(State_Main_Coroutine());
    }
    IEnumerator State_Main_Coroutine()
    {
        StateChange(eState.End);

        yield return 0;
    }

    void State_End()
    {
        StartCoroutine(State_End_Coroutine());
    }
    IEnumerator State_End_Coroutine()
    {
        SceneChange(SceneEnum.eScene.scn_title);

        yield return 0;
    }

    void WaitShowCompletedSplashScreen(System.Action p_CompletedCallback)
    {
        StartCoroutine(WaitShowCompletedSplashScreen_Coroutine(p_CompletedCallback));
    }
    IEnumerator WaitShowCompletedSplashScreen_Coroutine(System.Action p_ComplatedCallback)
    {
        while (Application.isShowingSplashScreen)
        {
            yield return 0;
        }

        p_ComplatedCallback();
    }
}
