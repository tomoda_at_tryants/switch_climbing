﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerPosture : MonoBehaviour
{
    [SerializeField]
    CtrlData m_LeftCon;

    [SerializeField]
    CtrlData m_RightCon;

    private Dictionary<Switch_def.eHandle, CtrlData> m_ObjectDic;

    static ControllerPosture s_Instance;
    public static ControllerPosture instance
    {
        get
        {
            if(s_Instance == null)
            {
                s_Instance = Create();
            }
            return s_Instance;
        }
    }

    public static ControllerPosture Create()
    {
        s_Instance = ResourceManager.PrefabLoadAndInstantiate<ControllerPosture>("ControllerPosture");
        s_Instance.transform.SetParent(GameCanvas.instance.transform);

        s_Instance.transform.localPosition = Vector3.zero;

        s_Instance.Init();

        return  s_Instance;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        m_ObjectDic = new Dictionary<Switch_def.eHandle, CtrlData>();
        m_ObjectDic.Add(Switch_def.eHandle.Left, m_LeftCon);
        m_ObjectDic.Add(Switch_def.eHandle.Right, m_RightCon);

        m_LeftCon.Init();
        m_RightCon.Init();

        int a_nLayer = LayerEnum.GetLayerNum(LayerEnum.eLayer.Default, false);
        gameObject.layer        = a_nLayer;
        m_LeftCon.obj.layer     = a_nLayer;
        m_RightCon.obj.layer    = a_nLayer;
    }

    [System.Serializable]
    public class CtrlData
    {
        [SerializeField]
        Switch_def.eHandle m_Handle;

        [SerializeField]
        GameObject m_Object;
        public GameObject obj
        {
            get { return m_Object; }
            set { m_Object = value; }
        }

        public void Init()
        {
            PositionDefaultSet();
            ActiveSet(true);
        }

        /// <summary>
        /// デフォルトのポジションにセットする
        /// </summary>
        public void PositionDefaultSet()
        {
            float a_fZ = GameCanvas.instance.canvas.transform.localPosition.z;

            Vector3 a_Viewport = Vector3.zero;
            if(m_Handle == Switch_def.eHandle.Left)
            {
                a_Viewport.Set(0.07f, 0.1f, a_fZ);
            }
            else
            if (m_Handle == Switch_def.eHandle.Right)
            {
                a_Viewport.Set(0.93f, 0.1f, a_fZ);
            }

            //  ビューポート座標をワールド座標に変換
            Vector3 a_World = Camera.main.ViewportToWorldPoint(a_Viewport);
            m_Object.transform.position = a_World;
        }

        public void ActiveSet(bool p_bActive)
        {
            m_Object.SetActive(p_bActive);
        }

        public void QuaternionSet(Quaternion p_Quaternion)
        {
            m_Object.transform.rotation = p_Quaternion;
        }
    }

    public void ActiveSet(Switch_def.eHandle p_Handle, bool p_bActive)
    {
        if (p_Handle == Switch_def.eHandle.Max || p_Handle == Switch_def.eHandle.None) return;
        m_ObjectDic[p_Handle].ActiveSet(p_bActive);
    }

    /// <summary>
    /// クォータニオンをセットする
    /// </summary>
    public void QuaternionSet(Switch_def.eHandle p_Handle, Quaternion p_Quaternion)
    {
        if (p_Handle == Switch_def.eHandle.Max || p_Handle == Switch_def.eHandle.None) return;
        m_ObjectDic[p_Handle].QuaternionSet(p_Quaternion);
    }
}
