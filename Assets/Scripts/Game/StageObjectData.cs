﻿using UnityEngine;
using UnityEngine.UI;

public class StageObjectData : BaseObjectData
{
    public enum eType
    {
        None = -1,

        Rock001,    //  岩
        Bird001,     //  鳥

        Max
    }

    [SerializeField]
    eType   m_Type;
    public eType type
    {
        get { return m_Type; }
        set { m_Type = value; }
    }

public Image    image
    {
        get
        {
            Image a_Image = transform.GetComponentInChildren<Image>();
            return a_Image;
        }
    }

    [SerializeField]
    float m_fColliderPowerToRev;
    /// <summary>
    /// 画像に対するコライダーの倍率
    /// </summary>
    public float fColliderPowerToRev
    {
        get { return m_fColliderPowerToRev; }
        set { m_fColliderPowerToRev = value; }
    }

    [SerializeField]
    int m_fCreditDamage;
    /// <summary>
    /// 与ダメージ
    /// </summary>
    public int  creditDamage
    {
        get { return m_fCreditDamage; }
        set { m_fCreditDamage = value; }
    }
}
