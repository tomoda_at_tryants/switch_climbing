﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    static string eTAG
    {
        get { return typeof(GameManager).ToString(); }
    }

    Player m_Player;
    public Player player
    {
        get { return m_Player; }
    }

    MapManager m_MapManager;
    public MapManager mapManager
    {
        get { return m_MapManager; }
    }

    //  ゲーム用キャンバス生成
    GameCanvas m_GameCanvas;

    int m_nStage;
    public int stage
    {
        get { return m_nStage; }
    }

    bool m_bGameClear;
    /// <summary>
    /// ゲームクリアかゲームオーバーになった
    /// </summary>
    public bool isGameClear
    {
        get { return m_bGameClear; }
    }

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static GameManager   Create(int p_nStage, System.Action p_InitComplateCallback)
    {
        GameObject  a_Obj   = new GameObject("GameManager");
        GameManager me      = a_Obj.AddComponent<GameManager>();

        me.Init(p_nStage, p_InitComplateCallback);

        return me;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="p_InitComplateCallback"></param>
    public void Init(int p_nStage, System.Action p_InitComplateCallback)
    {
        StartCoroutine(Init_Coroutine(p_nStage, p_InitComplateCallback));
    }

    IEnumerator Init_Coroutine(int p_nStage, System.Action p_InitCompleteCallback)
    {
        m_nStage = p_nStage;
        m_bGameClear = false;

        if (m_GameCanvas == null)
        {
            m_GameCanvas = GameCanvas.Create();
        }

        bool    a_bWait = true;
        //  マップマネージャーの初期化
        m_MapManager = MapManager.Create(1, false, 
            //  初期化の完了時に呼ばれる
            () =>
            {
                a_bWait = false;
            }
        );

        while (a_bWait) yield return 0;
        a_bWait = true;

        //  プレイヤーの初期化
        m_Player = Player.Create(
            () =>
            //  初期化の了時に呼ばれる
            {
                a_bWait = false;
            }
        );

        while (a_bWait) yield return 0;
        a_bWait = true;

        m_MapManager.catchObjMan.SetPlayerStartToCatch(ref m_Player);
        //  カメラの追跡対象オブジェクトを登録
        SetTrackingCamera_On(player.cameraTrackingTarget);

        //  初期化完了時に呼ばれるコールバック
        if (p_InitCompleteCallback != null)  p_InitCompleteCallback();
    }

    public void Update_Exec()
    {
        m_MapManager.Update_Exec();
        m_Player.Update_Exec();

        if (m_Player.isGoal && !m_bGameClear)
        {
            StageEnd(true);
        }
    }

    /// <summary>
    /// ステージ終了時に呼ばれる
    /// </summary>
    /// <returns></returns>
    public void StageEnd(bool p_bGameClear)
    {
        GameEndPanel.eType a_Type = GameEndPanel.eType.None;
        if (p_bGameClear)
        {
            m_bGameClear = true;
            a_Type = GameEndPanel.eType.GameClear;

            GameClear();
        }
        else
        {
            a_Type = GameEndPanel.eType.GameOver;

            GameOver();
        }

        m_Player.SetControllDisable();  //  プレイヤーの操作を無効にする
        //  ゲーム終了ダイアログを表示
        GameEndPanel.Create(a_Type, TemplateCanvas.instance.panelHigh);
    }

    void GameClear()
    {
    }

    void GameOver()
    {
    }

    /// <summary>
    /// カメラで追跡を行うオブジェクトを登録する
    /// </summary>
    /// <param name="p_TrackingTarget"></param>
    void    SetTrackingCamera_On(GameObject p_TrackingTarget)
    {
        //  カメラの追跡対象オブジェクトを登録
        GameCanvas.instance.SetTrackingTarget(p_TrackingTarget);
        mapManager.SetTrackingTarget(p_TrackingTarget);
    }

    /// <summary>
    /// カメラで追跡を行うオブジェクトを解除する
    /// </summary>
    void    SetTrackingCamera_Off()
    {
        //  カメラの追跡対象オブジェクトを登録
        GameCanvas.instance.SetTrackingTarget(null);
        mapManager.SetTrackingTarget(null);
    }
}
