﻿using nn.hid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public delegate void CatchSuccessDelegate();
public delegate bool ReleaseHandDelegate(Switch_def.eHandle p_Handle);
public delegate int GetCatchObjIDToReverseHandDelegate(Switch_def.eHandle p_Handle);

public class PlayerArm : MonoBehaviour
{
    static string   eTAG
    {
        get { return typeof(PlayerArm).ToString(); }
    }

    public event CatchSuccessDelegate               catchSuccessCallback;
    public event ReleaseHandDelegate                releaseHandCallback;
    public event GetCatchObjIDToReverseHandDelegate getCatchObjIDToReverseHandCallback;

    /// <summary>
    /// キャッチ可能なオブジェクトを認識可能な範囲
    /// </summary>
    public const float c_fEnableSearchObjectDistance = 600;

    /// <summary>
    /// アニメーションによって伸ばせる腕の長さ
    /// </summary>
    public const float c_fStretchArmDistanceMax = 400;

    public Image image;
    public GameObject tip;
    public Image hand;
    public Rigidbody2D handRigidBody;
    public SpringJoint2D joint;
    public Switch_def.eHandle handle;

    [SerializeField]
    Rigidbody2D m_ParentRigidbody;

    /// <summary>
    /// 実際につかまっているオブジェクト
    /// </summary>
    [SerializeField]
    CatchObject m_CatchObj;
    public CatchObject catchObj
    {
        get { return m_CatchObj; }
        set { m_CatchObj = value; }
    }

    /// <summary>
    /// ベクトルの長さ
    /// </summary>
    public float handMagnitude
    {
        get { return handRigidBody.velocity.magnitude; }
    }

    /// <summary>
    /// 手のワールド座標
    /// </summary>
    public Vector3 handWorldPos
    {
        get { return hand.rectTransform.position; }
        set { hand.rectTransform.position = value; }
    }

    /// <summary>
    /// キャリブレーション用
    /// </summary>
    Quaternion m_CalibQuaternion;

    Player.stSwingParam m_SwingParam;
    /// <summary>
    /// スイング用パラメータ
    /// </summary>
    public Player.stSwingParam  swingParam
    {
        get { return m_SwingParam; }
    }

    public void SwingParamReset()
    {
        m_SwingParam = new Player.stSwingParam(handle);
    }

    /// <summary>
    /// 入力可能か
    /// </summary>
    [SerializeField]
    bool m_bCanInput;

    [SerializeField]
    bool m_bAnimation;

    /// <summary>
    /// 落下している
    /// </summary>
    public bool falling;

    /// <summary>
    /// 腕のサイズ
    /// </summary>
    public Vector2 armSize
    {
        get
        {
            if (image == null) return Vector2.zero;

            return image.rectTransform.sizeDelta;
        }

        set
        {
            if (image == null) return;
            image.rectTransform.sizeDelta = value;
        }
    }

    /// <summary>
    /// 掴まってから経過した時間
    /// </summary>
    float m_fCatchElapcedTime;

    PlayerPowerGage m_PowerGage;

//  型判定
    public bool IsCatchObjectType(Type p_Type)
    {
        if (p_Type == null) return false;
        return (p_Type == typeof(CatchObject)) ? true : false;
    }
    public bool IsCatchObjectType<T>()
    {
        return (typeof(T) == typeof(CatchObject)) ? true : false;
    }
//

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init(PlayerPowerGage p_PowerGage)
    {
        falling         = false;
        m_bAnimation    = false;
        m_bCanInput     = false;

        m_PowerGage = p_PowerGage;

        m_CalibQuaternion = Quaternion.identity;

        m_SwingParam = new Player.stSwingParam(handle);
        m_SwingParam.Reset();
    }

    /// <summary>
    /// キャッチが許可されているオブジェクトか調べる
    /// </summary>
    /// <returns></returns>
    public bool CheckEnableCatchObject(CatchObject p_CatchObject)
    {
        if (p_CatchObject == null) return false;

        return true;
    }

    /// <summary>
    /// 腕のサイズを更新する
    /// </summary>
    public void SizeUpdate()
    {
        //  サイズの更新
        Vector2 a_Size          = armSize;
        Vector2 a_ParentScale   = m_ParentRigidbody.transform.localScale;
        float   a_fDistance     = Vector3.Distance(image.transform.position, handWorldPos) / a_ParentScale.x;

        armSize = new Vector2(a_Size.x, a_fDistance);
    }

    /// <summary>
    /// ターゲットのオブジェクトへの角度に更新する
    /// </summary>
    public void SetAngleToTargetObject()
    {
        int a_nTargetID = targetCatchObjID;
        CatchObject a_CatchObj = CatchObjectManager.instance.GetObjectToID(a_nTargetID);
        if (a_CatchObj == null) return;

        Vector3 a_ToPos = a_CatchObj.worldPos_Pivot_Catch;

        AngleUpdate(a_ToPos);
    }

    /// <summary>
    /// ターゲットの手への角度に更新する
    /// </summary>
    public void SetAngleToHand()
    {
        Vector3 a_ToPos = handWorldPos;
        AngleUpdate(a_ToPos);
    }

    /// <summary>
    /// 腕の角度を更新する
    /// </summary>
    void AngleUpdate(Vector3 p_ToPos)
    {
        Vector3 a_From = transform.position;
        //  角度の更新
        Vector3 a_SubPos    = a_From - p_ToPos;
        //  ラジアン角に変換
        float   a_fRad      = Mathf.Atan2(a_SubPos.y, a_SubPos.x) * Mathf.Rad2Deg;

        image.transform.localRotation = Quaternion.Euler(0, 0, a_fRad + 90);
    }
    
    /// <summary>
    /// キャリブレーションを行う
    /// </summary>
    /// <param name="p_Quaternion"></param>
    Quaternion Calibration_Exec(Quaternion p_Quaternion)
    {
        m_SwingParam.Reset();
        return Quaternion.Inverse(p_Quaternion * new Quaternion(0, 0, 1, 0));   //  逆クォータニオンを登録
    }

    /// <summary>
    /// コントローラーのクォータニオンを２D座標系用で取得する
    /// </summary>
    Quaternion GetJoyConQuaternionFor2D()
    {
        //  コントローラーのクォータニオン
        Quaternion  a_CtrlQuaternion = SwitchApiManager.instance.NpadMan.GetJoyconQuaternion(handle);
        //  最終的なクォータニオン
        Quaternion  a_ResultQuaternion = Quaternion.identity;

        //  フリーモードの時のみキャリブレーションを有効にする
#if MAP_TYPE_FREE
        //  ボタンが押された時点のコントローラーの姿勢からキャリブレーション値を設定する
        NpadButton a_Button = Player.r_CalibrationButtonDic[handle];
        if (SwitchApiManager.instance.NpadMan.PushButtonCheck(a_Button))
        {
            m_CalibQuaternion = Calibration_Exec(a_CtrlQuaternion);
        }
#endif

        //  最終的なクォータニオン
        a_ResultQuaternion = m_CalibQuaternion * a_CtrlQuaternion;

        //  ２D座標系用に変換
        a_ResultQuaternion.Set(a_ResultQuaternion.w, a_ResultQuaternion.y, a_ResultQuaternion.z, -a_ResultQuaternion.x);

        return a_ResultQuaternion;
    }

    /// <summary>
    /// 手を放している時の処理
    /// </summary>
    /// <param name="p_SensorState"></param>
    public void Ctrl_Exec(SixAxisSensorState p_SensorState)
    {
        //  スイングパラメータ更新
//        m_SwingParam.Update();

        //  スイング中
        if (m_SwingParam.isSwinging)
        {
            //  スイング開始時のコントローラー座標を登録する
            if (m_SwingParam.swingStartPos == Vector2.zero)
            {
                //  スイング検出時にパワーゲージを停止する
                if (Player.isGage && m_PowerGage.enableMove) m_PowerGage.enableMove = false;

                m_SwingParam.swingStartPos = ctrlVector;
            }
        }
        else
        //  スイング終了
        if (m_SwingParam.isSwingEnd)
        {
            m_SwingParam.swingEndAngleY = p_SensorState.angle.y;
        }

        //  新しいターゲットに向かって手を伸ばすアニメーション
        if (m_SwingParam.isSwingEnd)
        {
            UnityEngine.Object a_Object = new UnityEngine.Object();

            //  腕の角度算出
#if MAP_TYPE_STAGEOBJ
            //  次に掴むオブジェクト
            CatchObject a_TargetObj = CatchObjectManager.instance.GetObjectToID(targetCatchObjID);
            a_Object = a_TargetObj;
#endif

            StretchAnimation_Exec(a_Object);
        }
    }

    /// <summary>
    /// コントローラーのベクトル値
    /// </summary>
    Vector2 ctrlVector
    {
        get
        {
            //  コントローラーのクォータニオン取得
            Quaternion a_CtrlQuaternion = GetJoyConQuaternionFor2D();
            //  コントローラーの向き
            Vector2 a_CtrlVector = a_CtrlQuaternion * Vector3.forward;

            //  コントローラー座標の制限
            float a_fMin = -0.8f;
            float a_fMax = 0.8f;

            //  値の制限
            a_CtrlVector.x = Mathf.Clamp(a_CtrlVector.x, a_fMin, a_fMax);
            a_CtrlVector.y = Mathf.Clamp(a_CtrlVector.y, a_fMin, a_fMax);

            //  コントローラーの姿勢から座標に変換する
            a_CtrlVector.x = Mathf.Lerp(-200, 200, (a_CtrlVector.x + 1) / 2);
            a_CtrlVector.y = Mathf.Lerp(-200, 200, (a_CtrlVector.y + 1) / 2) + 10;

            return a_CtrlVector;
        }
    }



    /// <summary>
    /// ターゲットになっているキャッチオブジェクトと手の角度
    /// </summary>
    float targetCatchObjToAngle
    {
        get
        {
            float a_fAngle = 0;
#if MAP_TYPE_FREE
            Vector2 a_CtrlVector = ctrlVector;
            //  コントローラーの振り始めと現在のフレームの座標を取得し、その２点の角度を取得する
            a_fAngle = Mathf.Atan2(a_CtrlVector.y - m_SwingParam.swingStartPos.y, a_CtrlVector.x - m_SwingParam.swingStartPos.x) * Mathf.Rad2Deg;
            a_fAngle -= 90f;
#elif MAP_TYPE_STAGEOBJ
            //  次に掴むオブジェクト
            CatchObject a_TargetObj = CatchObjectManager.instance.GetObjectToID(targetCatchObjID);
            if (a_TargetObj == null) return 0;

            //  手と次に掴むオブジェクトの角度を取得
            a_fAngle = Mathf.Atan2(handWorldPos.y - a_TargetObj.worldPos_Pivot_Catch.y, handWorldPos.x - a_TargetObj.worldPos_Pivot_Catch.x) * Mathf.Rad2Deg;
            a_fAngle += 90f;
#endif

            return a_fAngle;
        }
    }

    /// <summary>
    /// 反対の手がつかんでいるオブジェクトのIDを取得する
    /// </summary>
    int catchObjID_reverseHand
    {
        get
        {
#if MAP_TYPE_FREE
            return 0;
#endif
            return  getCatchObjIDToReverseHandCallback(handle);
        }
    }

    /// <summary>
    /// ターゲットになっているキャッチオブジェクトのID（そのまま振るとターゲットになるオブジェクト）
    /// </summary>
    int targetCatchObjID
    {
        get
        {
#if MAP_TYPE_FREE
            return 0;
#elif MAP_TYPE_STAGEOBJ
            int a_nID = catchObjID_reverseHand;
            CatchObject a_Target = null;
            //  何も掴んでいない時は反対側の手がつかんでいるオブジェクトが指定しているターゲットにする
            if (a_nID >= 0)
            {
                a_Target = CatchObjectManager.instance.GetObjectToID(a_nID);
                a_nID = (m_bPreviousCatch) ? a_Target.GetPreviousID(handle) : a_Target.GetNextID(handle);
            }

            return a_nID;
#endif
        }
    }

    int targetCatchObjID_Next
    {
        get
        {
            CatchObject a_Target = CatchObjectManager.instance.GetObjectToID(catchObjID_reverseHand);
            if (a_Target == null)   return -1;
            return  a_Target.GetNextID(handle);
        }
    }

    int targetCatchObjID_Previous
    {
        get
        {
            CatchObject a_Target = CatchObjectManager.instance.GetObjectToID(catchObjID_reverseHand);
            if (a_Target == null)   return -1;
            return a_Target.GetPreviousID(handle);
        }
    }

    /// <summary>
    /// 「CatchPivot」のconstraintsを設定する
    /// </summary>
    void SetEnableMoveHand(bool p_bEnable)
    {
        if (p_bEnable && handRigidBody.constraints != RigidbodyConstraints2D.FreezeRotation)
        {
            handRigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
        else
        if (!p_bEnable && handRigidBody.constraints != (RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation))
        {
            handRigidBody.constraints = (RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation);
        }
    }

    /// <summary>
    /// 腕を伸ばすアニメーションを実行する
    /// </summary>
    /// <param name="p_fAngle"></param>
    void StretchAnimation_Exec<T>(T p_Object) where T : UnityEngine.Object
    {
        StartCoroutine(StretchAnimation_Coroutine(p_Object));

        //  振った方向の矢印作成
#if false
        {
            Image a_Arrow = ResourceManager.PrefabLoadAndInstantiate<Image>("Arrow");

            a_Arrow.transform.SetParent(TemplateCanvas.instance.panelHigh);

            a_Arrow.transform.localPosition = Vector2.zero;
            a_Arrow.rectTransform.sizeDelta = new Vector2(100, 150);
            a_Arrow.rectTransform.localEulerAngles = new Vector3(0, 0, p_fAngle);

            Destroy(a_Arrow.gameObject, 2f);
        }
#endif
    }

    /// <summary>
    /// 腕を伸ばすアニメーションを実行する（直接は呼び出さない）
    /// </summary>
    /// <param name="p_fX"></param>
    /// <param name="p_fY"></param>
    /// <returns></returns>
    IEnumerator StretchAnimation_Coroutine<T>(T p_Object) where T : UnityEngine.Object
    {
        m_bAnimation    = true;
        //  入力禁止にする
        m_bCanInput     = false;
        joint.enabled   = false;  //  スプリングジョイントを一時的に無効にする

        //  プレイヤーの体を固定する
        m_ParentRigidbody.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;
        //  手の移動を有効にする
        SetEnableMoveHand(true);

        //  アニメーション
        float   a_fAngle    = targetCatchObjToAngle;
        float   a_fSin      = Mathf.Sin(a_fAngle * Mathf.Deg2Rad);
        float   a_fCos      = Mathf.Cos(a_fAngle * Mathf.Deg2Rad);

        iTween.Stop(hand.gameObject); //  既に追加されているitweenは停止する

        float   a_fDistance = 0;
        float   a_fTime     = 0;
        Type    a_ObjType   = TryGetType(p_Object);
        Vector2 a_ByPos     = Vector2.zero;

        bool    a_bAnimation = false;
        if (IsCatchObjectType(a_ObjType))
        {
            a_bAnimation = true;

            //  次に掴むオブジェクトを取得する
            CatchObject a_TargetObj = p_Object as CatchObject;

            //  手とオブジェクトの距離を取得する
            a_fDistance = Vector2.Distance(handWorldPos, a_TargetObj.worldPos_Pivot_Catch);
            //  距離の制限
            a_fDistance = Mathf.Clamp(a_fDistance, 0, c_fStretchArmDistanceMax);

            a_fTime     = (Player.isAcceleration) ? m_SwingParam.swingReachTime : (Player.isGage) ? (a_fDistance / 1100f) + 0.1f : 0;
            a_ByPos     = new Vector2(-a_fSin * a_fDistance, a_fCos * a_fDistance);
        }

        if(a_bAnimation)
        {
            handRigidBody.simulated = false;

            //  腕を伸ばすアニメーション
            iTween.MoveBy(hand.gameObject, iTween.Hash("x", a_ByPos.x, "y", a_ByPos.y, "time", a_fTime, "easetype", iTween.EaseType.easeOutSine,
                "oncomplete", "StretchAnimationEnd",
                "oncompletetarget", gameObject
            ));
        }

        yield return 0;
    }

    Type    TryGetType<T>(T p_Object) where T : UnityEngine.Object
    {
        Type    a_Type = null;
        try
        {
            a_Type = p_Object.GetType();
        }
        catch
        {
        }

        return a_Type;
    }

    /// <summary>
    /// 腕が伸びるアニメーションの完了時に呼ばれる（iTweenの「oncomplete」イベントで指定されている）
    /// </summary>
    void StretchAnimationEnd()
    {
        StartCoroutine(StretchAnimationEnd_Coroutine());
    }

    /// <summary>
    /// 腕を伸ばすアニメーションが終了した時に呼ばれる
    /// </summary>
    /// <returns></returns>
    IEnumerator StretchAnimationEnd_Coroutine()
    {
        yield return new WaitForSeconds(0.1f); //  0.1秒待機

        handRigidBody.simulated = true;
        joint.enabled           = true;

        //  プレイヤーの固定にしていた体を解除する
        m_ParentRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;

#if MAP_TYPE_STAGEOBJ
        //  ターゲットポジション非表示
        TargetPosition_Off(targetCatchObjID_Next);
        TargetPosition_Off(targetCatchObjID_Previous);
#endif

        //  アニメーションが完全に終了するまで待機
        while (m_bAnimation)
        {
            //  伸びるアニメーションが完了し、縮むアニメーションが停止と判断する値になったらループ終了
            if (handMagnitude <= 150f)  break;
            yield return 0;
        }

        //  アニメーション中なら終了処理
        if (m_bAnimation)
        {
            CommonAnimationEnd_Exec();
        }
    }
    
    /// <summary>
    /// ターゲットポジション用イメージをオンにする
    /// </summary>
    /// <param name="p_ByPos"></param>
    public void TargetPosition_On(int p_nID, bool p_bSelectTarget)
    {

#if MAP_TYPE_FREE
        
#elif MAP_TYPE_STAGEOBJ
        CatchObject a_Target = CatchObjectManager.instance.GetObjectToID(p_nID);
        if (a_Target == null) return;

        a_Target.TargetOn(p_bSelectTarget);
#endif
    }

    /// <summary>
    /// ターゲットポジション用イメージをオフにする
    /// </summary>
    /// <param name="p_ByPos"></param>
    public void TargetPosition_Off(int p_nID)
    {
        CatchObject a_Target = CatchObjectManager.instance.GetObjectToID(p_nID);
        if (a_Target == null) return;

        a_Target.TargetOff();
    }

    /// <summary>
    /// 捕まえたオブジェクトを取得する
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T    GetCatchObject<T>() where T : UnityEngine.Object
    {
        UnityEngine.Object  a_Obj = default(T);

        if (IsCatchObjectType<T>())
        {
            // 自分の位置とプレイヤーの位置から向きベクトルを作成しRayに渡す
            Ray2D           a_Ray2D = new Ray2D(handWorldPos, Vector3.forward);
            RaycastHit2D[]  a_Hits  = Physics2D.RaycastAll(a_Ray2D.origin, a_Ray2D.direction, c_fEnableSearchObjectDistance);

            CatchObject a_CatchObj = null;

            for (int i = 0; i < a_Hits.Length; i++)
            {
                RaycastHit2D    a_Hit = a_Hits[i];
                if(a_Hit.transform.GetComponent<CatchObject>())
                {
                    CatchObject a_CatchObject = a_Hit.collider.GetComponent<CatchObject>();
                    //  掴むのを許可されていなければcontinue
                    if (!CheckEnableCatchObject(a_CatchObject)) continue;

                    a_Obj = a_Hit.collider.GetComponent<CatchObject>();
                    a_CatchObj = a_Obj as CatchObject;
                    break;
                }
            }

            //  反対側の手がつかんでいるIDを取得する
            int a_nID_Reverse = getCatchObjIDToReverseHandCallback(handle);
            //  掴めていない
            //  反対の手は掴んでいる
            //  手が動いていない
            if (a_CatchObj == null && a_nID_Reverse >= 0 && !m_bAnimation && handMagnitude <= 150)
            {
                //  反対側の手がつかんでいるオブジェクトをターゲットにする
                a_Obj = CatchObjectManager.instance.GetObjectToID(a_nID_Reverse);
            }
        }

        return (T)a_Obj;
    }

    /// <summary>
    /// 掴む入力がされた
    /// </summary>
    public bool isInputCatch
    {
        get
        {
            NpadButton a_Button = Player.r_ReleaseObjectButtonDic[handle];
            if ((SwitchApiManager.instance.NpadMan.ReleaseButtonCheck(a_Button) && Player.isPushingToRelease) ||
                (SwitchApiManager.instance.NpadMan.PushButtonCheck(a_Button) && Player.isPushingToCatch))
            {
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// 掴む入力がされ続けている
    /// </summary>
    public bool isInputCatching
    {
        get
        {
            NpadButton a_Button = Player.r_ReleaseObjectButtonDic[handle];
            if ((SwitchApiManager.instance.NpadMan.ReleasingButtonCheck(a_Button) && Player.isPushingToRelease) ||
                (SwitchApiManager.instance.NpadMan.PushingButtonCheck(a_Button) && Player.isPushingToCatch))
            {
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// 手を離す入力がされた
    /// </summary>
    public bool isInputRelease
    {
        get
        {
            NpadButton  a_Button = Player.r_ReleaseObjectButtonDic[handle];

            //  手を離す
            if ((SwitchApiManager.instance.NpadMan.PushButtonCheck(a_Button) && m_bCanInput && Player.isPushingToRelease) ||
                (SwitchApiManager.instance.NpadMan.ReleaseButtonCheck(a_Button) && m_bCanInput && Player.isPushingToCatch))
            {
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// 手を離し続ける入力がされた
    /// </summary>
    public bool isInputReleasing
    {
        get
        {
            NpadButton  a_Button = Player.r_ReleaseObjectButtonDic[handle];
            if ((SwitchApiManager.instance.NpadMan.PushingButtonCheck(a_Button) && Player.isPushingToRelease) ||
                (SwitchApiManager.instance.NpadMan.ReleasingButtonCheck(a_Button) && Player.isPushingToCatch))
            {
                return true;
            }
            return false;
        }
    }

    public bool GetSensorState(ref SixAxisSensorState p_SensorState)
    {
        bool    a_bGetState = SwitchApiManager.instance.NpadMan.GetSensorState(handle, ref p_SensorState);
        return  a_bGetState;
    }

    public  void Update_Exec()
    {
#if MAP_TYPE_FREE
        //  コントローラーの姿勢をオブジェクトに設定する
        ControllerPosture.instance.QuaternionSet(handle, GetJoyConQuaternionFor2D());
#endif
        m_SwingParam.Update();

        //  センサーの状態を取得する
        SixAxisSensorState  a_SensorState = new SixAxisSensorState();
        if(!GetSensorState(ref a_SensorState))  
        {
            //  センサーの状態の取得に失敗した
            return;
        }

        bool    a_bFalling = (falling) ? true : false; //  入力可能な状態か

        bool    a_bInputCatch       = isInputCatch;
        if (a_bInputCatch)      InputCatch_Exec();

        bool    a_bInputRelease     = (!a_bFalling && isInputRelease) ? true : false;
        if (a_bInputRelease)    InputRelease_Exec();

        bool    a_bInputReleasing   = (!a_bFalling && isInputReleasing) ? true : false;
        if (a_bInputReleasing)  InputReleasing_Exec(a_SensorState);

        bool    a_bInputCatching    = (!a_bFalling && isInputCatching) ? true : false;
        if (a_bInputCatching)   InputCatching_Exec();

        //  サイズの更新
        if (!a_bFalling)
        {
            SizeUpdate();
        }
        //  落下中の処理
        else
        {
            Falling_Exec();
        }
    }

    bool    m_bAnimationToFalling = false;

    /// <summary>
    /// 落下中のアニメーション
    /// </summary>
    void AnimationToFalling()
    {
        StartCoroutine(AnimationToFalling_Coroutine());
    }

    IEnumerator AnimationToFalling_Coroutine()
    {
        m_bAnimationToFalling = true;

        handRigidBody.mass = 0;
        image.transform.localEulerAngles = new Vector3(0, 0, (handle == Switch_def.eHandle.Right) ? -60 : 60);
        armSize = new Vector2(armSize.x, 100f);

        float   a_fAddAngle     = (handle == Switch_def.eHandle.Right) ? -5f : (handle == Switch_def.eHandle.Left) ? 5f : 0;
        float   a_fAngle_Max    = image.transform.localEulerAngles.z + 45f;
        float   a_fAngle_Min    = image.transform.localEulerAngles.z - 45f;

        //  腕を上下に動かすアニメーションをループする
        while (true)
        {
            //  落下していない時はアニメーション終了
            if (!falling)   break;

            float   a_fArmAngle = image.transform.localEulerAngles.z;
            a_fArmAngle += a_fAddAngle;

            if(a_fArmAngle < a_fAngle_Min)
            {
                a_fArmAngle = a_fAngle_Min;
                a_fAddAngle *= -1f;
            }
            else
            if(a_fArmAngle > a_fAngle_Max)
            {
                a_fArmAngle = a_fAngle_Max;
                a_fAddAngle *= -1f;
            }

            image.transform.localEulerAngles = new Vector3(0, 0, a_fArmAngle);
            hand.transform.position = tip.transform.position;

            yield return 0;
        }

        handRigidBody.mass = 1;

        m_bAnimationToFalling = false;
    }

    /// <summary>
    /// 落下中の処理
    /// </summary>
    void Falling_Exec()
    {
        m_PowerGage.EnableView(false);

        if (!m_bAnimationToFalling)
        {
#if MAP_TYPE_STAGEOBJ
            //  全ターゲットポジションを非表示にする
            CatchObjectManager.instance.AllTargetPosition_Off();
#endif
            //  落下中のアニメーション
            AnimationToFalling();
        }
    }

    /// <summary>
    /// 手を離す操作が行われた時の処理
    /// </summary>
    void InputRelease_Exec()
    {
        //  何も掴んでいない状態にする
        m_CatchObj      = null;

        //  手が離れた時に呼ばれるコールバック
//      if(releaseHandCallback != null) releaseHandCallback(handle);

        //  プレイヤーの体の固定を解除する
        m_ParentRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        //  手の移動を許可する
        SetEnableMoveHand(true);

        //  スイングパラメータをリセットする
        m_SwingParam.Reset();

        image.color = Color.magenta;
    }

    bool    m_bPreviousCatch;

    /// <summary>
    /// 手が離れている時の処理
    /// </summary>
    void    InputReleasing_Exec(SixAxisSensorState p_SensorState)
    {
        //  入力が可能な時は操作処理を実行する
        if (m_bCanInput)
        {
            Ctrl_Exec(p_SensorState);


#if MAP_TYPE_STAGEOBJ
            //  ターゲットを切り替える
            NpadButton  a_Button = Player.r_TargetPreviousButtonDic[handle];
            //  デフォルトは掴んでいるオブジェクトの次のオブジェクト
            //  ボタンを押すと逆に前のオブジェクトに戻る
            if (SwitchApiManager.instance.NpadMan.PushButtonCheck(a_Button) && Player.isPushingToCatch)
            {
                //  ターゲットを切り替える
                TargetChange();
            }

            //  ターゲットポジション表示
            TargetPosition_On(targetCatchObjID_Next, (targetCatchObjID == targetCatchObjID_Next));
            TargetPosition_On(targetCatchObjID_Previous, (targetCatchObjID == targetCatchObjID_Previous));
#endif
        }

        //  ゲージモード
        //  スイングしていない
        //  アニメーションしていない
        if (Player.isGage && !m_SwingParam.isSwinging && !m_bAnimation)
        {
            if (!m_PowerGage.view)          m_PowerGage.EnableView(true);
            if (!m_PowerGage.enableMove)    m_PowerGage.enableMove = true;
        }

        //  次のターゲットの向きに手を伸ばす
        if(!m_bAnimation && Mathf.Abs(handRigidBody.velocity.x) <= 20)
        {
            //  腕の角度を更新する
            SetAngleToTargetObject();
            hand.transform.position = tip.transform.position;
        }
        else
        {
            //  腕の角度を更新する
            SetAngleToHand();
        }

#if false
        if (m_bAnimation)
        //  アニメーション中はYを軸に回転させることで左右に調整できる
        {
            //  角度の調整
            AdjustAngle(a_SensorState);
        }
#endif
    }

    /// <summary>
    /// ターゲットを切り替える
    /// </summary>
    void    TargetChange()
    {
        m_bPreviousCatch ^= true;

        int a_nCatchObjID = getCatchObjIDToReverseHandCallback(handle);
        CatchObject a_CatchObj = CatchObjectManager.instance.GetObjectToID(a_nCatchObjID);
        //  ターゲットが存在しない
        if (m_bPreviousCatch && a_CatchObj.GetPreviousID(handle) < 0)
        {
            m_bPreviousCatch = false;
            SwitchApiManager.instance.vibrationMan.Execute(0f, 0.5f, 0.03f, handle);
        }
    }

    /// <summary>
    /// 掴む操作を行った時の処理
    /// </summary>
    public void    InputCatch_Exec()
    {
        if (!m_bAnimation && !falling)
        {
            //  パワーゲージを非表示にする
            if (Player.isGage)
            {
                m_PowerGage.EnableView(false);
            }
        }

        UnityEngine.Object  a_Obj = null;
#if MAP_TYPE_STAGEOBJ
        //  掴む対象になっているオブジェクトを取得する
        a_Obj = GetCatchObject<CatchObject>();
#endif

        SwingEnd_Exec(a_Obj);
    }

    public void     InputCatching_Exec()
    {
        if (m_CatchObj != null)
        {
            //  手の座標を掴まっているオブジェクトの座標に更新する
            hand.transform.position = m_CatchObj.worldPos_Pivot_Catch;
        }

        SetAngleToHand();
    }

    /// <summary>
    /// 角度の調整を行う
    /// </summary>
    void    AdjustAngle(SixAxisSensorState p_SensorState)
    {
        //  振り終わり時のコントローラーのYを軸にした角度と現在のコントローラーの角度を比較し、
        //  腕の角度を調整する

        float a_fAngleY = p_SensorState.angle.y;
        float a_fSub    = m_SwingParam.swingEndAngleY - a_fAngleY;

        a_fSub *= 2;    //  補正

        if (a_fSub >= 0.05f || a_fSub <= -0.05f)
        {
            hand.transform.RotateAround(image.transform.position, Vector3.forward, a_fSub);
        }
    }

    public void CommonAnimationEnd_Exec()
    {
        handRigidBody.simulated = true;

        m_bCanInput     = true;
        joint.enabled   = true;  //  スプリングジョイントを有効にする
        m_bAnimation    = false;

#if MAP_TYPE_STAGEOBJ
        //  ターゲットポジション表示
        TargetPosition_Off(targetCatchObjID_Next);
        TargetPosition_Off(targetCatchObjID_Previous);
#endif
        //  アニメーションを停止する
        iTween.Stop(hand.gameObject);
        //  スイングパラメータのリセット
        m_SwingParam.Reset();
        //  プレイヤーの固定にしていた体を解除する
        m_ParentRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    /// <summary>
    /// スイング終了後の処理（主にステージオブジェクトを掴んで上る状態の時に使用される）
    /// </summary>
    /// <param name="p_StageObject"></param>
    public void SwingEnd_Exec<T>(T p_Object) where T : UnityEngine.Object
    {
        bool    a_bSuccess  = false;
        Type    a_ObjType   = null;

#if MAP_TYPE_STAGEOBJ
        if (p_Object != null)   a_ObjType = TryGetType(p_Object);
#endif

        //  引数に渡されたオブジェクトがキャッチオブジェクト
        if (IsCatchObjectType(a_ObjType))
        {
            CatchObject a_CatchObject = p_Object as CatchObject;

            //  掴む事を許可されているか
            a_bSuccess = CheckEnableCatchObject(a_CatchObject);
        }

        if (a_bSuccess)
        {
            CatchStageObject_Success(p_Object);
        }
        else
        {
            CatchStageObject_Failed();
        }

        //  成功時、腕の色を変える
        image.color = new Color(40f / 255f, 220f / 255f, 50f / 255f);

        CommonAnimationEnd_Exec();
    }
    
    /// <summary>
    /// キャッチに成功した時の処理
    /// </summary>
    public void CatchStageObject_Success<T>(T p_Object) where T : UnityEngine.Object
    {
        m_bPreviousCatch = false;

        catchSuccessCallback();
        Type    a_ObjType = TryGetType(p_Object);

        if (IsCatchObjectType(a_ObjType))
        {
            //  キャッチオブジェクトを登録
            CatchObject a_CatchObject = p_Object as CatchObject;
            SetCatchObj(a_CatchObject);
        }

        if(Player.isGage)   m_PowerGage.EnableView(false);

        SwitchApiManager.instance.vibrationMan.Execute(0.2f, 1, 0.03f, handle);

        SoundManager.instance.PlaySE(AudioEnum.eSE.se_catch);

        //  キャッチピボットを移動不可にする
        SetEnableMoveHand(false);
    }

    /// <summary>
    /// キャッチに失敗した時の処理
    /// </summary>
    public void CatchStageObject_Failed()
    {
        if (Player.isGage)
        {
            m_PowerGage.EnableView(true);
            if (!m_PowerGage.enableMove) m_PowerGage.enableMove = true;
        }
    }
    
    /// <summary>
    /// キャッチしたオブジェクトを登録する
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="p_Object"></param>
    public void SetCatchObj<T>(T p_Object) where T : UnityEngine.Object
    {
        Type    a_ObjType = TryGetType(p_Object);
        try
        {
            if (IsCatchObjectType(a_ObjType))
            {
                CatchObject a_CatchObject = p_Object as CatchObject;

                m_CatchObj      = a_CatchObject;
                handWorldPos    = m_CatchObj.worldPos_Pivot_Catch;
            }
        }
        catch
        {
            Debug.LogError("PlayerArm->SetCatchObj CatchError");
        }
    }
}
