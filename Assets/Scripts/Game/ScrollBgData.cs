﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollBgData : MonoBehaviour
{
    /// <summary>
    /// デフォルトの背景サイズ
    /// </summary>
    public static readonly Vector2 c_ImageSize = new Vector2(1920, 1080);

    /// <summary>
    /// 背景データ
    /// </summary>
    [SerializeField]
    RawImage    image;

    /// <summary>
    /// 表示の有効フラグ
    /// </summary>
    public bool enable
    {
        get { return image.enabled; }
        set { image.enabled = value; }
    }

    /// <summary>
    /// 端のデータ
    /// </summary>
    [System.Serializable]
    public class EdgeManager
    {
        public EdgeData     top;
        public EdgeData     bottom;
        public EdgeData     left;
        public EdgeData     right;

        public override string ToString()
        {
            return string.Format("top:{0} bottom:{1} left:{2} right:{3}", top.ToString(), bottom.ToString(), left.ToString(), right.ToString());
        }

        [System.Serializable]
        public class EdgeData
        {
            public RectTransform rectTransform;
            [SerializeField]
            Vector2 m_ViewPort;
            public Vector2  viewPort
            {
                get
                {
                    m_ViewPort = GameCanvas.instance.canvasCamera.WorldToViewportPoint(rectTransform.position);
                    return m_ViewPort;
                }
            }

            public override string ToString()
            {
                return (viewPort.x.ToString());
            }
        }
    }

    /// <summary>
    /// 描画範囲内か
    /// </summary>
    public bool isView
    {
        get
        {
            return (edgeManager.top.viewPort.y < 0 || edgeManager.bottom.viewPort.y > 1 || edgeManager.left.viewPort.x > 1 || edgeManager.right.viewPort.x < 0) ? false : true;
        }
    }

    public EdgeManager edgeManager;

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="p_ImagePath"></param>
    /// <param name="p_ScreenIndex"></param>
    public void Init(string p_ImagePath)
    {
        Texture2D   a_Tex = ResourceManager.TextureLoad(p_ImagePath, false);

        //  背景画像設定
        image.texture = a_Tex;
        image.rectTransform.sizeDelta = c_ImageSize;
    }
}
