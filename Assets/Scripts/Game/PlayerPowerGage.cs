﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPowerGage : MonoBehaviour
{
    static string   eTAG
    {
        get { return typeof(PlayerPowerGage).ToString(); }
    }

    [SerializeField]
    Slider slider;
    public RectTransform rectTransform;

    public float power
    {
        get { return slider.value; }
        set { slider.value = value; }
    }

    bool m_bEnableMove;
    public bool enableMove
    {
        get { return m_bEnableMove; }
        set { m_bEnableMove = value; }
    }

    /// <summary>
    /// 表示フラグ
    /// </summary>
    public bool view
    {
        get { return gameObject.activeSelf; }
    }

    public void EnableView(bool p_bView)
    {
#if MAP_TYPE_STAGEOBJ
        if (gameObject.activeSelf) gameObject.SetActive(false);
        return;
#endif
        if (view != p_bView)  gameObject.SetActive(p_bView);
        if (!view)              m_bEnableMove = false;
    }

    public static PlayerPowerGage Create()
    {
        PlayerPowerGage me = ResourceManager.PrefabLoadAndInstantiate<PlayerPowerGage>("PlayerPowerGage");
        me.Init();

        return me;
    }

    public void Init()
    {
        slider.value = 0;
        m_bEnableMove = false;

        EnableView(false);
    }

    void Update()
    {
        StartCoroutine(Update_Coroutine());
    }

    IEnumerator Update_Coroutine()
    {
#if MAP_TYPE_STAGEOBJ
        if(gameObject.activeSelf)   gameObject.SetActive(false);
        yield break;
#endif

        float a_fTime = 0;
        float a_fAniTime = 0.3f;

        while(true)
        {
            a_fTime += Time.deltaTime;

            if (m_bEnableMove)
            {
                if(a_fTime <= a_fAniTime)
                {
                    slider.value = Mathf.Lerp(0, 100, a_fTime * (1f / a_fAniTime));
                }
                else
                {
                    slider.value = Mathf.Lerp(100, 0, (a_fTime - a_fAniTime) * (1f / a_fAniTime));

                    if(a_fTime >= a_fAniTime * 2)
                    {
                        a_fTime = 0;
                    }
                }
            }

            yield return 0;
        }
    }
}
