﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCanvas : CanvasBehaviour
{
    static string eTAG
    {
        get { return typeof(GameCanvas).ToString(); }
    }

    public Camera canvasCamera;

    public RectTransform backgroundView;
    public RectTransform catchObjView;
    public RectTransform stageObjView;
    public RectTransform playerView;

    static GameCanvas s_Instance;
    /// <summary>
    /// インスタンス
    /// </summary>
    public static GameCanvas instance
    {
        get
        {
            if (s_Instance == null) return Create();
            return s_Instance;
        }
    }

    /// <summary>
    /// 追跡対象のオブジェクト
    /// </summary>
    GameObject m_TrackingTarget;

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static GameCanvas Create(MapManager.MapSize p_MapSize = null)
    {
        if (s_Instance != null) return s_Instance;

        try
        {
            GameObject a_Obj = GameObject.Find(eTAG);
            if (a_Obj != null)
            {
                s_Instance = a_Obj.GetComponent<GameCanvas>();

                s_Instance.SetCamera();

                return s_Instance;
            }
        }
        catch
        {
            return null;
        }

        s_Instance = ResourceManager.PrefabLoadAndInstantiate<GameCanvas>(eTAG);
        s_Instance.name = eTAG;
        s_Instance.Init(p_MapSize);

        return s_Instance;
    }

    /// <summary>
    /// カメラをセットする
    /// </summary>
    public void SetCamera()
    {
        if (canvasCamera != null) return;

        GameObject  a_Obj = GameObject.Find("GameCamera");
        if (a_Obj == null)
        {
            a_Obj = ResourceManager.PrefabLoadAndInstantiate("GameCamera", Vector3.zero);
        }

        canvasCamera = a_Obj.GetComponent<Camera>();
    }

    public void Init(MapManager.MapSize p_MapSize = null)
    {
        SetCamera();

        foreach (Transform a_Transform in catchObjView.transform)
        {
            Destroy(a_Transform.gameObject);
        }

        foreach (Transform a_Transform in playerView.transform)
        {
            Destroy(a_Transform.gameObject);
        }
    }

    public override void Init()
    {
    }

    /// <summary>
    /// 追跡対象オブジェクトを設定する
    /// </summary>
    /// <param name="p_TargetObj"></param>
    public void SetTrackingTarget(GameObject p_TargetObj)
    {
        if (m_TrackingTarget == p_TargetObj) return;
        m_TrackingTarget = p_TargetObj;
    }

    void Update()
    {
/*
        Vector3 a_MoveValue = Vector2.zero;
        //  カメラの移動処理
        {
            a_MoveValue.x -= SwitchApiManager.instance.NpadMan.PushStickCheck(nn.hid.NpadButton.StickRLeft);
            a_MoveValue.x += SwitchApiManager.instance.NpadMan.PushStickCheck(nn.hid.NpadButton.StickRRight);

            a_MoveValue.y -= SwitchApiManager.instance.NpadMan.PushStickCheck(nn.hid.NpadButton.StickRDown);
            a_MoveValue.y += SwitchApiManager.instance.NpadMan.PushStickCheck(nn.hid.NpadButton.StickRUp);

            bool a_bPushingStickR = SwitchApiManager.instance.NpadMan.PushingButtonCheck(nn.hid.NpadButton.StickR);
            float a_fRev = (a_bPushingStickR) ? 5 : 10;

            a_MoveValue.x /= a_fRev;
            a_MoveValue.y /= a_fRev;
        }

        //  カメラのズーム処理
        {
            if(viewCamera.transform.localPosition.z <= 0 && viewCamera.transform.localPosition.z >= -300)
            {
                if (SwitchApiManager.instance.NpadMan.PushingButtonCheck(nn.hid.NpadButton.Up))
                {
                    a_MoveValue.z += 4f;
                }

                if (SwitchApiManager.instance.NpadMan.PushingButtonCheck(nn.hid.NpadButton.Down))
                {
                    a_MoveValue.z -= 4f;
                }
            }

            if(viewCamera.transform.localPosition.z + a_MoveValue.z > 0)
            {
                a_MoveValue.z = 0 - viewCamera.transform.localPosition.z;
            }

            if (viewCamera.transform.localPosition.z + a_MoveValue.z < -300)
            {
                a_MoveValue.z = -(viewCamera.transform.localPosition.z + 300);
            }
        }

        //  カメラの座標更新
        viewCamera.transform.localPosition += a_MoveValue;
*/
        //  オブジェクトを追跡する
 //       if (a_MoveValue == Vector3.zero)
        {
            TrackingObject();
        }
    }

    public void    TrackingObject()
    {
        if (m_TrackingTarget != null)
        {
            Vector2 a_TargetPos = m_TrackingTarget.transform.localPosition;
            Vector3 a_Pos       = new Vector3(a_TargetPos.x, a_TargetPos.y + 100, canvas.planeDistance);

            if (a_Pos.x < 0) a_Pos.x = 0;
            if (a_Pos.y < 0) a_Pos.y = 0;

            canvasCamera.transform.localPosition = a_Pos;
        }
    }
}
