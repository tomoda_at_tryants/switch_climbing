﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MapManager : MonoBehaviour
{
    static string eTAG
    {
        get { return typeof(MapManager).ToString(); }
    }

    public delegate void MapEditor_GetSelectObjectDelegate(BaseObjectData p_Obj);
    public static MapEditor_GetSelectObjectDelegate getSelectObjectCallback;

    public delegate void MapEditor_DestroyObjectDelegate(BaseObjectData p_Obj);
    public static MapEditor_DestroyObjectDelegate destroyObjectCallback;

    public delegate void MapEditor_GetMapManager(MapManager p_MapManager);
    public static MapEditor_GetMapManager getMapManagerCallback;

    MapSize m_MapSize;
    /// <summary>
    /// マップサイズ
    /// </summary>
    public MapSize mapSize
    {
        get { return m_MapSize; }
        set { m_MapSize = value; }
    }

    CatchObjectManager m_CatchObjMan;
    public CatchObjectManager catchObjMan
    {
        get { return m_CatchObjMan; }
    }

    StageObjectManager m_StageObjMan;
    public StageObjectManager stageObjMan
    {
        get { return m_StageObjMan; }
    }

    Canvas m_MapCanvas;
    public Canvas mapCanvas
    {
        get { return m_MapCanvas; }
    }

    RectTransform m_RectTransform;
    public RectTransform rectTransform
    {
        get { return m_RectTransform; }
    }

    /// <summary>
    /// キャンバスのカメラ
    /// </summary>
    public Camera canvasCamera
    {
        get { return m_MapCanvas.worldCamera; }
    }

    bool m_bEditor;
    bool m_bInitialized;

    int m_nStageNum;
    public int stageNum
    {
        get { return m_nStageNum; }
    }

    public class MapSize
    {
        [SerializeField]
        int m_nWidth;
        /// <summary>
        /// 横のマップ数（１画面分を１とする）
        /// </summary>
        public int width
        {
            get { return m_nWidth; }
            set
            {
                if (value <= 0) value = 1;
                m_nWidth = value;
            }
        }

        [SerializeField]
        int m_nHeight;
        /// <summary>
        /// 縦のマップ数（1画面分を１とする）
        /// </summary>
        public int height
        {
            get { return m_nHeight; }
            set
            {
                if (value <= 0) value = 1;
                m_nHeight = value;
            }
        }

        /// <summary>
        /// 画面数
        /// </summary>
        public int count
        {
            get
            {
                return m_nWidth * m_nHeight;
            }
        }

        /// <summary>
        /// 生成する
        /// </summary>
        /// <returns></returns>
        public static MapSize Create(int p_nStageNum)
        {
            MapSize me = new MapSize(1, 1);
            me.LoadData(p_nStageNum);

            return me;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="p_nWidth"></param>
        /// <param name="p_nHeight"></param>
        public MapSize(int p_nWidth, int p_nHeight)
        {
            width = p_nWidth;
            height = p_nHeight;
        }

        /// <summary>
        /// マップサイズをロードする
        /// </summary>
        /// <param name="p_StageName"></param>
        public void LoadData(int p_nStageNum)
        {
            string a_StageName = Switch_def.GetStageName(p_nStageNum);
            string a_FileName = a_StageName + "_size";
            MapSize a_MapSize = new MapSize(1, 1);
            //  マップサイズのロードを行う
            SwitchApiManager.instance.saveMan.Load(a_FileName, ref a_MapSize);

            width = a_MapSize.width;
            height = a_MapSize.height;
        }

        /// <summary>
        /// マップサイズをセーブする
        /// </summary>
        /// <param name="p_StageName"></param>
        public void SaveData(int p_nStageNum)
        {
            string a_StageName = Switch_def.GetStageName(p_nStageNum);
            string a_FileName = a_StageName + "_size";
            MapSize a_MapSize = new MapSize(m_nWidth, m_nHeight);

            //  マップサイズのセーブを行う
            SwitchApiManager.instance.saveMan.Save(a_FileName, a_MapSize);
        }

        public override string ToString()
        {
            StringBuilder a_Str = new StringBuilder();
            a_Str.Append(string.Format("width:{0}　", m_nWidth));
            a_Str.Append(string.Format("height:{0}", m_nHeight));

            return a_Str.ToString();
        }
    }

    float a_fTime = 0;
    float a_fCreateInterval = 3f;
    public void CreateStageObject()
    {
        a_fTime += Time.deltaTime;
        if (a_fTime >= a_fCreateInterval)
        {
            a_fTime = 0;
            a_fCreateInterval = UnityEngine.Random.Range(2f, 5f);

            Vector2 a_Pos = new Vector3(UnityEngine.Random.Range(-500, 500) + GameCanvas.instance.canvasCamera.transform.localPosition.x, GameCanvas.instance.canvasCamera.transform.localPosition.y + (Screen.height / 2) + 400f);
            m_StageObjMan.CreateObject(StageObjectBase.eType.Rock001, a_Pos);
        }
    }

    float a_fTime_bird = 0;
    float a_fCreateInterval_bird = 3f;
    public void CreateStageObject_Bird()
    {
        a_fTime_bird += Time.deltaTime;
        if (a_fTime_bird >= a_fCreateInterval_bird)
        {
            a_fTime_bird = 0;
            a_fCreateInterval_bird = UnityEngine.Random.Range(2f, 5f);

            Vector2 a_Pos = new Vector3(GameCanvas.instance.canvasCamera.transform.localPosition.x - (Screen.width / 2) - 200, GameCanvas.instance.canvasCamera.transform.localPosition.y + UnityEngine.Random.Range(-300, 300));
            m_StageObjMan.CreateObject(StageObjectBase.eType.Bird001, a_Pos);
        }
    }

    public void SaveStageData()
    {
        m_CatchObjMan.SaveStageData(m_nStageNum);
    }

    public void Update_Exec()
    {
        if (!m_bInitialized) return;

        CreateStageObject();
        CreateStageObject_Bird();

        //  オブジェクトを追跡する
        TrackingObject_Exec();
    }

    /// <summary>
    /// 生成（オブジェクトモード時）
    /// </summary>
    /// <param name="p_nStageNum"></param>
    /// <returns></returns>
    public static MapManager Create(int p_nStageNum, bool p_bEditor, System.Action p_InitComplete)
    {
        GameObject a_Obj = new GameObject("MapManager");
        MapManager me = a_Obj.AddComponent<MapManager>();

        me.Init(p_nStageNum, p_bEditor, p_InitComplete);

        return me;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="p_nStageNum"></param>
    public void Init(int p_nStageNum, bool p_bEditor, System.Action p_InitComplete)
    {
        StartCoroutine(Init_Coroutine(p_nStageNum, p_bEditor, p_InitComplete));
    }

    public IEnumerator Init_Coroutine(int p_nStageNum, bool p_bEditor, System.Action p_bLoadCompletedCallback)
    {
        m_bInitialized = false;

        m_bEditor = p_bEditor;
        m_nStageNum = p_nStageNum;
        m_nCreateType = 0;

        if (m_MapCanvas == null)
        {
            string  a_PrefabName = "MapCanvas";
            m_MapCanvas = ResourceManager.PrefabLoadAndInstantiate(a_PrefabName, Vector3.zero).GetComponent<Canvas>();
            m_MapCanvas.name = a_PrefabName;

            m_RectTransform = m_MapCanvas.GetComponent<RectTransform>();

            m_MapCanvas.worldCamera.depth = -1;
            m_MapCanvas.worldCamera.cullingMask = LayerEnum.GetLayerNum(LayerEnum.eLayer.Map, true);
        }

        //  キャッチオブジェクトリストのマネージャー生成
        m_CatchObjMan = CatchObjectManager.Create(p_bEditor, m_MapCanvas);

        //  ステージオブジェクトリストのマネージャー生成
        m_StageObjMan = StageObjectManager.Create(p_bEditor, m_MapCanvas);

        //  マップサイズのローディング
        m_MapSize = MapSize.Create(m_nStageNum);

        //  ステージデータのロードを行う
        yield return StartCoroutine(LoadStageData_Coroutine());

        if (m_bEditor)
        {
            //  自身を渡すコールバック
            getMapManagerCallback(this);
        }

        //  コールバックの設定
        if (p_bLoadCompletedCallback != null)
        {
            p_bLoadCompletedCallback();
        }

        m_bInitialized = true;
    }

    public void CreateObjectToTouch(Vector2 p_TouchPos)
    {
        if (m_nCreateType  < 0 || m_nCreateObjType < 0) return;

        if(m_nCreateObjType == 0)  //  CatchObject
        {
            CatchObjectData.eType a_Type = (CatchObjectData.eType)m_nCreateType;
            m_CatchObjMan.CreateObjectToTouch(a_Type, p_TouchPos);
        }
        else
        if(m_nCreateObjType == 1)
        {
            StageObjectData.eType a_Type = (StageObjectData.eType)m_nCreateType;
            StageObjectBase a_StageObj = m_StageObjMan.CreateObjectToTouch(a_Type, p_TouchPos);

            a_StageObj.rigidbody.bodyType = RigidbodyType2D.Static;
            a_StageObj.functionEnd          = true;
        }
    }

    int m_nCreateObjType;
    public int createObjType
    {
        get { return m_nCreateObjType; }
        set { m_nCreateObjType = value; }
    }

    int m_nCreateType;
    /// <summary>
    /// 生成タイプ（エディター用）
    /// </summary>
    public int  createType
    {
        get { return m_nCreateType; }
        set { m_nCreateType = value; }
    }

    /// <summary>
    /// ステージデータをロードする
    /// </summary>
    /// <returns></returns>
    public IEnumerator LoadStageData_Coroutine()
    {
        //  ステージ用シーンのロード
        string          a_StageName = Switch_def.GetStageName(m_nStageNum);
        AsyncOperation  a_Async     = SceneManager.LoadSceneAsync(a_StageName, LoadSceneMode.Additive);
        //  ステージのロードが完了するまで待機
        while (!a_Async.isDone)
        {
            yield return 0;
        }

        //  キャッチオブジェクトのロード
        m_CatchObjMan.Load_CatchObj(m_nStageNum);
    }

    GameObject m_TrackingTarget;

    /// <summary>
    /// 追跡対象オブジェクトを設定する
    /// </summary>
    /// <param name="p_TargetObj"></param>
    public void SetTrackingTarget(GameObject p_TargetObj)
    {
        if (m_TrackingTarget == p_TargetObj) return;

        m_TrackingTarget = p_TargetObj;
    }

    /// <summary>
    /// カメラの追跡対象を登録する
    /// </summary>
    public void TrackingObject_Exec()
    {
        if (m_TrackingTarget == null)   return;

        Vector2     a_TargetPos         = m_TrackingTarget.transform.position;
        float       a_fCanvasScale      = 0.01388889f;     //  ※CanvasのScaleが取得できなかったのでとりあえずハードコーディング
        Vector3     a_Pos               = (new Vector3(a_TargetPos.x, a_TargetPos.y + 100, canvasCamera.transform.position.z)) / (1f / a_fCanvasScale);

        if (a_Pos.x < 0) a_Pos.x = 0;
        if (a_Pos.y < 0) a_Pos.y = 0;

        canvasCamera.transform.position = a_Pos;
    }
}
