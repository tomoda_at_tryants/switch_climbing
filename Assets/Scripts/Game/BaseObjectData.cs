﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BaseObjectData : MonoBehaviour
{
    bool m_bSelect;
    public bool select
    {
        get { return m_bSelect; }
        set { m_bSelect = value; }
    }

    [SerializeField]
    int m_nID;
    public int id
    {
        get { return m_nID; }
        set { m_nID = value; }
    }

    [SerializeField]
    Vector2 m_DefaultPos;
    public Vector2 defaultPos
    {
        get { return m_DefaultPos; }
        set { m_DefaultPos = value; }
    }

    [SerializeField]
    Vector2 m_DefaultScale;
    public Vector2 defaultScale
    {
        get { return m_DefaultScale; }
        set { m_DefaultScale = value; }
    }

    [SerializeField]
    Vector3 m_DefaultAngle;
    public Vector3 defaultAngle
    {
        get { return m_DefaultAngle; }
        set { m_DefaultAngle = value; }
    }

    [SerializeField]
    Image   m_TargetPosition;

    /// <summary>
    /// ターゲットがオンの状態か
    /// </summary>
    public bool isTargetOn
    {
        get
        {
            return m_TargetPosition.enabled;
        }
    }

    public void TargetPosition_On(float p_fOnTime, bool p_bSelect = false)
    {
        StartCoroutine(TargetPosition_On_Coroutine(p_bSelect, p_fOnTime));
    }

    IEnumerator TargetPosition_On_Coroutine(bool p_bSelect, float p_fOnTime)
    {
        //  既にターゲットがオンになっている時は終了
        if (isTargetOn) yield break;

        TargetOn(p_bSelect);
        yield return new WaitForSeconds(p_fOnTime);
        TargetOff();
    }

    /// <summary>
    /// ターゲットをオンにする
    /// </summary>
    public void TargetOn(bool p_bSelect = false)
    {
        m_TargetPosition.enabled = true;

        m_TargetPosition.color = (p_bSelect) ? new Color(Color.red.r, Color.red.g, Color.red.b, 150f / 255f) : new Color(Color.white.r, Color.white.g, Color.white.b, 160f / 255f);
        m_TargetPosition.transform.localScale = (p_bSelect) ? Vector2.one : new Vector2(0.7f, 0.7f);
    }

    /// <summary>
    /// ターゲットポジションをオフにする
    /// </summary>
    public void TargetOff()
    {
        m_TargetPosition.enabled = false;

        m_TargetPosition.color = Color.white;
        m_TargetPosition.transform.localScale = Vector2.one;
    }

    /// <summary>
    /// タッチイベント（主にエディター用）
    /// </summary>
    public void TouchEvent()
    {
        //  キャッチオブジェクトが選択された時によばれるコールバック
        MapManager.getSelectObjectCallback(this);
    }
}
