﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageObjectBase : StageObjectData
{
    /// <summary>
    /// 削除
    /// </summary>
    public void Destroy()
    {
        DestroyImmediate(gameObject);
    }

    public void OnDestroy()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CollisionExec(collision.transform, TriggerEnter2D_Exec);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        CollisionExec(collision.transform, TriggerExit2D_Exec);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        CollisionExec(collision.transform, CollisionEnter2D_Exec);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        CollisionExec(collision.transform, CollisionExit2D_Exec);
    }

    /// <summary>
    /// 共通の衝突処理
    /// </summary>
    /// <param name="p_HitComponents"></param>
    /// <param name="p_Callback"></param>
    void CollisionExec(Transform p_Object, Action<UnityEngine.Object> p_Callback)
    {
        Component[] a_Components = p_Object.GetComponentsInChildren(typeof(Component));
        foreach (UnityEngine.Object a_Object in a_Components)
        {
            p_Callback(a_Object);
        }
    }

    public virtual void Init(eType p_Type, Vector2 p_Pos)
    {
    }

    public virtual void Update_Exec()
    {
    }

    protected virtual void TriggerEnter2D_Exec(UnityEngine.Object p_Object)
    {
    }
    protected virtual void TriggerExit2D_Exec(UnityEngine.Object p_Object)
    {
    }
    protected virtual void     CollisionEnter2D_Exec(UnityEngine.Object p_Object)
    {
    }
    protected virtual void     CollisionExit2D_Exec(UnityEngine.Object p_Object)
    {
    }

    [SerializeField]
    Rigidbody2D m_Rigidbody;
    public new Rigidbody2D rigidbody    //  「new」はComponent.rigidbodyを非表示にするため
    {
        get { return m_Rigidbody; }
    }

    public float gravity
    {
        get { return m_Rigidbody.gravityScale; }
        set { m_Rigidbody.gravityScale = value; }
    }

    [SerializeField]
    Image   m_Target;
    public Image    target
    {
        get { return m_Target; }
        set { m_Target = value; }
    }

    [SerializeField]
    Collider2D          m_Collider;
    public new Collider2D collider
    {
        get { return m_Collider; }
        set { m_Collider = value; }
    }

    public bool functionEnd;

    /// <summary>
    /// 親を設定する
    /// </summary>
    /// <param name="p_Parent"></param>
    public void SetParent(Transform p_Parent)
    {
        transform.SetParent(p_Parent);
    }

    /// <summary>
    /// コリジョンのトリガーを設定する
    /// </summary>
    public void SetCollisionTrigger(bool p_bTrigger)
    {
        if (m_Collider == null) return;

        collider.isTrigger = p_bTrigger;
    }

    /// <summary>
    /// 画面に描画されているかチェックし、結果を返す
    /// </summary>
    /// <returns></returns>
    public bool RendererCheck()
    {
        Vector3 a_ViewportPos = GameCanvas.instance.canvasCamera.WorldToViewportPoint(transform.position);
        if (!functionEnd)
        {
            //  画面外の時はコライダーをオフにする
            if ((a_ViewportPos.x > 1.2f || a_ViewportPos.x < -1.2f) || (a_ViewportPos.y > 1.2f || a_ViewportPos.y < -1.2f))
            {
                if (m_Collider.enabled) m_Collider.enabled = false;
            }
            else
            {
                if (!m_Collider.enabled) m_Collider.enabled = true;
            }
        }

        bool    a_bRender = true;
        //  描画範囲のチェック 
        if ((a_ViewportPos.x > 2f || a_ViewportPos.x < -2f) || (a_ViewportPos.y > 2f || a_ViewportPos.y < -2f))
        {
            a_bRender = false;
        }
        return a_bRender;
    }
}
