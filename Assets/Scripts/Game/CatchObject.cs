﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatchObject : CatchObjectData
{
    static string   eTAG
    {
        get { return typeof(CatchObject).ToString(); }
    }

    [SerializeField]
    Image m_Image;
    public Image image
    {
        get { return m_Image; }
        set { m_Image = value; }
    }

    /// <summary>
    /// ピボット（掴む場所）
    /// </summary>
    [SerializeField]
    RectTransform   m_Pivot_Catch;

    /// <summary>
    /// イメージのサイズ
    /// </summary>
    public Vector2 imageSize
    {
        get
        {
            return image.rectTransform.sizeDelta;
        }
    }

    /// <summary>
    /// ピボットのワールド座標
    /// </summary>
    public Vector3 worldPos_Pivot_Catch
    {
        get
        {
            return m_Pivot_Catch.position;
        }
    }

    public Color imageColor
    {
        get { return image.color; }
        set { image.color = value; }
    }

    /// <summary>
    /// 表示されているか
    /// </summary>
    public bool isView
    {
        get { return image.enabled; }
    }

#if false
    /// <summary>
    /// 生成
    /// </summary>
    /// <param name="p_Type"></param>
    /// <param name="p_Pos"></param>
    /// <returns></returns>
    public static CatchObject Create(eType p_Type, Vector2 p_Pos, Transform p_Parent)
    {
        string a_PrefabName = "CatchObject_" + p_Type.ToString();
        string a_Path = "CatchObject/" + a_PrefabName;
        CatchObject me = ResourceManager.PrefabLoadAndInstantiate<CatchObject>(a_Path);

        me.Init(p_Type, p_Pos, p_Parent);

        return me;
    }
#endif

    /// <summary>
    /// 生成
    /// </summary>
    /// <param name="p_Data"></param>
    /// <param name="p_Pos"></param>
    /// <returns></returns>
    public static CatchObject Create(CatchObjectData p_Data, Transform p_Parent)
    {
        string a_PrefabName = "CatchObject_" + p_Data.type.ToString();
        string a_Path = "CatchObject/" + a_PrefabName;
        CatchObject me = ResourceManager.PrefabLoadAndInstantiate<CatchObject>(a_Path);

        me.Init(p_Data, p_Parent);

        return me;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="p_Type"></param>
    /// <param name="p_Pos"></param>
    public void Init(CatchObjectData p_Data, Transform p_Parent)
    {
        transform.SetParent(p_Parent);
        ParamSet(p_Data);

        transform.localPosition     = defaultPos;
        transform.localScale        = defaultScale;
        transform.localEulerAngles  = defaultAngle;
    }

#if false
    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="p_Type"></param>
    /// <param name="p_Pos"></param>
    public void Init(eType p_Type, Vector2 p_Pos, Transform p_Parent)
    {
        Init(p_Pos, p_Parent);
        type = p_Type;
    }
#endif

    /// <summary>
    /// 次に掴むオブジェクトのIDを取得する
    /// </summary>
    /// <param name="p_Handle"></param>
    /// <returns></returns>
    public int GetNextID(Switch_def.eHandle p_Handle)
    {
        return  GetID(p_Handle, true);
    }

    /// <summary>
    /// １つ前に掴んだオブジェクトのIDを取得する
    /// </summary>
    /// <param name="p_Handle"></param>
    /// <returns></returns>
    public int GetPreviousID(Switch_def.eHandle p_Handle)
    {
        return  GetID(p_Handle, false);
    }

    public void Update_Exec()
    {
        try
        {
            Vector3 a_ScreenPos = GameCanvas.instance.canvasCamera.WorldToScreenPoint(transform.position);
            Vector2 a_ImageSize = imageSize;

            bool a_bOutScreen = false;
            if (a_ScreenPos.x + a_ImageSize.x < 0)
            {
                a_bOutScreen = true;
            }
            else
            if (a_ScreenPos.x - a_ImageSize.x > Screen.width)
            {
                a_bOutScreen = true;
            }
            else
            if (a_ScreenPos.y - a_ImageSize.y > Screen.height)
            {
                a_bOutScreen = true;
            }
            else
            if (a_ScreenPos.y + a_ImageSize.y < 0)
            {
                a_bOutScreen = true;
            }

            //  表示設定
            if (image.enabled == a_bOutScreen)
            {
                image.enabled = !a_bOutScreen;
            }
        }
        catch
        {
        }
    }
}
