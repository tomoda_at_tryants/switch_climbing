﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CatchObjectManager : MonoBehaviour
{
    static string eTAG
    {
        get { return typeof(CatchObjectManager).ToString(); }
    }

    public delegate void MapEditor_GetSelectObjectDelegate(CatchObject p_CatchObj);
    public static MapEditor_GetSelectObjectDelegate getSelectObjectCallback;

    public delegate void MapEditor_DestroyObjectDelegate(CatchObject p_CatchObj);
    public static MapEditor_DestroyObjectDelegate destroyObjectCallback;

    List<CatchObject> m_ObjList;
    public List<CatchObject> objList
    {
        get { return m_ObjList; }
    }
    bool m_bEditor;

    public int  objCount
    {
        get { return objList.Count; }
    }

    static CatchObjectManager s_Instance;
    public static CatchObjectManager instance
    {
        get { return s_Instance; }
    }

    Canvas  m_CanvasRoot;

    public static CatchObjectManager    Create(bool p_bEditor, Canvas p_CanvasRoot)
    {
        GameObject  a_Object = new GameObject(eTAG);
        s_Instance = a_Object.AddComponent<CatchObjectManager>();

        s_Instance.Init(p_bEditor, p_CanvasRoot);

        return  s_Instance;
    }

    public void Init(bool p_bEditor, Canvas p_CanvasRoot)
    {
        m_bEditor       = p_bEditor;
        m_CanvasRoot    = p_CanvasRoot;
        m_ObjList       = new List<CatchObject>();

        //  コールバックの設定
        if (m_bEditor)
        {
            //  キャッチオブジェクトが削除された時のコールバック
            destroyObjectCallback += (p_CatchObj) =>
            {
                for (int i = m_ObjList.Count - 1; i >= 0; i--)
                {
                    //  削除対象のオブジェクトはリストから削除
                    if (m_ObjList[i] == p_CatchObj)
                    {
                        DestroyImmediate(m_ObjList[i].gameObject);

                        m_ObjList.Remove(p_CatchObj);
                        return;
                    }
                }
            };
        }
    }

    /// <summary>
    /// キャッチオブジェクトをロードする
    /// </summary>
    public void Load_CatchObj(int p_nStageNum)
    {
        //  データの読み込み
        string          a_StageName = Switch_def.GetStageName(p_nStageNum);
        string          a_ReadText  = CSVManager.Read(a_StageName);
        if (string.IsNullOrEmpty(a_ReadText)) return;
        //  CSVから読み込んだテキストを改行毎に分ける
        List<string>    a_TextList  = a_ReadText.Split('\n').ToList();

        //  テキストからデータを設定する
        foreach (string a_Text in a_TextList)
        {
            string[]    a_ParamStr = a_Text.Split(',');
            if (a_ParamStr.Length == 0) continue;

            try
            {
                CatchObjectData a_Data = new CatchObjectData();
                a_Data.ParamSet(a_ParamStr);

                Transform   a_Parent = GameCanvas.instance.catchObjView;

                CreateCatchObject(a_Data, a_Parent);
            }
            catch
            {
                continue;
            }
        }
    }

    /// <summary>
    /// IDからオブジェクトを取得する
    /// </summary>
    /// <returns></returns>
    public CatchObject  GetObjectToID(int p_nId)
    {
        if (p_nId < 0)  return null;

        foreach(CatchObject a_CatchObj in objList)
        {
            if(a_CatchObj.id == p_nId)  return a_CatchObj;
        }
        return null;
    }

    /// <summary>
    /// 全オブジェクトのターゲットを非表示にする
    /// </summary>
    public void AllTargetPosition_Off()
    {
        foreach(CatchObject a_CatchObj in objList)
        {
            if(a_CatchObj.isView)
            {
                a_CatchObj.TargetOff();
            }
        }
    }

    /// <summary>
    /// キャッチオブジェクトを生成する
    /// </summary>
    /// <param name="p_Data"></param>
    /// <param name="p_Parent"></param>
    /// <returns></returns>
    CatchObject CreateCatchObject(CatchObjectData p_Data, Transform p_Parent)
    {
        CatchObject a_CatchObject = CatchObject.Create(p_Data, p_Parent);
        m_ObjList.Add(a_CatchObject);

        return a_CatchObject;
    }

    /// <summary>
    /// プレイヤーを初期位置に設定する
    /// </summary>
    public void SetPlayerStartToCatch(ref Player p_Player)
    {
        CatchObject a_StartToObject = null;
        foreach (CatchObject a_Data in m_ObjList)
        {
            if (a_Data.startToCatch)
            {
                a_StartToObject = a_Data;
            }
        }

        p_Player.SetCatchObj(Switch_def.eHandle.Left, a_StartToObject);
        p_Player.SetCatchObj(Switch_def.eHandle.Right, a_StartToObject);
    }

    private void Update()
    {
        //  アップデート処理
        foreach(CatchObject a_CatchObj in objList)
        {
            a_CatchObj.Update_Exec();

            switch (a_CatchObj.type)
            {
                case CatchObjectData.eType.None:
                    {
                    }
                    break;

                case CatchObjectData.eType.Rock001:
                    {
                    }
                    break;

                case CatchObjectData.eType.Branch001:
                    {

                    }
                    break;

                case CatchObjectData.eType.Goal001:
                    {
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// タッチ位置からオブジェクトを生成する
    /// </summary>
    /// <param name="p_Type"></param>
    /// <param name="p_TouchPos"></param>
    public void CreateObjectToTouch(CatchObjectData.eType p_Type, Vector2 p_TouchPos)
    {
        if (!m_bEditor) return;

        CatchObject a_Object = GetCatchObjectToPosition(p_TouchPos);
        if (a_Object == null)
        {
            Vector3 a_CameraPos = m_CanvasRoot.worldCamera.transform.localPosition;
            Vector2 a_CreatePos = new Vector2(p_TouchPos.x + a_CameraPos.x - (Screen.width / 2), p_TouchPos.y + a_CameraPos.y - (Screen.height / 2));

            CatchObjectData         a_Data      = new CatchObjectData();
            a_Data.type         = p_Type;
            a_Data.defaultPos   = a_CreatePos;
            a_Data.defaultScale = Vector2.one;

            a_Object = CreateCatchObject(a_Data, GameCanvas.instance.catchObjView);
        }
    }

    /// <summary>
    /// タッチ（クリック）座標からキャッチオブジェクトを取得する
    /// </summary>
    /// <param name="p_TouchPosition"></param>
    /// <returns></returns>
    CatchObject GetCatchObjectToPosition(Vector2 p_TouchPosition)
    {
        // 自分の位置とプレイヤーの位置から向きベクトルを作成しRayに渡す
        Ray2D           a_Ray2D     = new Ray2D(p_TouchPosition, Vector3.forward);
        RaycastHit2D[]  a_Hits      = Physics2D.RaycastAll(a_Ray2D.origin, a_Ray2D.direction, Mathf.Infinity);
        CatchObject     a_CatchObj  = null;

        for (int i = 0; i < a_Hits.Length; i++)
        {
            RaycastHit2D a_Hit = a_Hits[i];
            if (a_Hit.transform.GetComponent<CatchObject>())
            {
                a_CatchObj = a_Hit.collider.GetComponent<CatchObject>();
                
                break;
            }
        }

        return a_CatchObj;
    }

    /// <summary>
    /// ステージデータをセーブする
    /// </summary>
    /// <returns></returns>
    public void SaveStageData(int p_nStageNum)
    {
        StringBuilder a_Str = new StringBuilder();
        foreach (CatchObjectData a_Data in m_ObjList)
        {
            if (a_Data != null)
            {
                a_Str.Append(a_Data.GetStringFormat());
            }
        }

        a_Str.Remove(a_Str.Length - 1, 1);

        //  CSVに書き込む
        string a_StageName = Switch_def.GetStageName(p_nStageNum);
        CSVManager.Write(a_StageName, a_Str.ToString());
    }
}
