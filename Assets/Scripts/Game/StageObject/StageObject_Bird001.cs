﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class StageObject_Bird001 : StageObjectBase
{
    [SerializeField]
    private MeshRenderer m_MeshRenderer;

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public StageObject_Bird001()
    {
        id = -1;
    }

    public float angle;
    public float sin;
    public float cos;
    public float speed;

    bool m_bIgnore;
    public bool isIgnore
    {
        get { return m_bIgnore; }
    }

    /// <summary>
    /// 既に役割を果たしたら呼び出す
    /// </summary>
    public void Ignore_On()
    {
        m_bIgnore = true;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="p_Pos"></param>
    /// <param name="p_Parent"></param>
    public override void    Init(eType p_Type, Vector2 p_Pos)
    {
        SetParent(GameCanvas.instance.stageObjView);

        transform.localPosition = p_Pos;

        type    = p_Type;
        id      = 0;

        angle = -90 - Random.Range(-45, 45);
        speed = Random.Range(2f, 4f);

        functionEnd = false;
    }

    /// <summary>
    /// 更新
    /// </summary>
    public override void    Update_Exec()
    {
        //  描画範囲外に存在する時はリストから削除
        if (!RendererCheck())
        {
            Destroy();
            return;
        }

        if(!functionEnd)
        {
            sin = Mathf.Sin((angle) * Mathf.Deg2Rad);
            cos = Mathf.Cos((angle) * Mathf.Deg2Rad);
            transform.localPosition += new Vector3(sin * -speed, cos * speed);
        }
    }

    protected override void TriggerEnter2D_Exec(Object p_Object)
    {
        if (p_Object == null) return;
        //  無視する
        if (m_bIgnore) return;

        if (p_Object.GetType() == typeof(Player))
        {
            Player  a_Player = p_Object as Player;
            //  ダメージ処理
            a_Player.DamageToHitStageObject(creditDamage);

            angle -= 180;
            speed = 15;
            transform.localScale = new Vector3(-1, 1);
            collider.enabled = false;
            functionEnd = true;

            //  無視する
            Ignore_On();
        }
    }

    protected override void TriggerExit2D_Exec(Object p_Object)
    {
        SetCollisionTrigger(false);
    }

    protected override void CollisionEnter2D_Exec(Object p_Object)
    {
        if (p_Object == null)   return;

        if (p_Object.GetType() == typeof(CatchObject) && !m_bIgnore)
        {
            //  キャッチオブジェクトと衝突した時はトリガーをオンにする
            SetCollisionTrigger(true);
        }
    }

    protected override void CollisionExit2D_Exec(Object p_Object)
    {
        if (p_Object == null)   return;
    }
}
