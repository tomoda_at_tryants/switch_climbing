﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class StageObject_Rock001 : StageObjectBase
{
    [SerializeField]
    private MeshRenderer m_MeshRenderer;

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public StageObject_Rock001()
    {
        id = -1;
    }

    bool m_bIgnore;
    public bool isIgnore
    {
        get { return m_bIgnore; }
    }

    /// <summary>
    /// 既に役割を果たしたら呼び出す
    /// </summary>
    public void Ignore_On()
    {
        m_bIgnore = true;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="p_Pos"></param>
    /// <param name="p_Parent"></param>
    public override void    Init(eType p_Type, Vector2 p_Pos)
    {
        SetParent(GameCanvas.instance.stageObjView);

        transform.localPosition = p_Pos;

        type    = p_Type;
        id      = 0;

        functionEnd = false;

        float   a_fTorque = UnityEngine.Random.Range(-300000, 300000);
        a_fTorque += (a_fTorque <= 0) ? -100000 : 100000;

        rigidbody.AddTorque(a_fTorque, ForceMode2D.Impulse);
        transform.GetComponent<RectTransform>().SetAsFirstSibling();

        gravity = UnityEngine.Random.Range(10, 20);

        float   a_fScale = UnityEngine.Random.Range(0.75f, 1.3f);
        transform.localScale = new Vector3(a_fScale, a_fScale, a_fScale);
    }

    /// <summary>
    /// 更新
    /// </summary>
    public override void    Update_Exec()
    {
        //  描画範囲外に存在する時はリストから削除
        if (!RendererCheck())
        {
            Destroy();
            return;
        }

        if(!functionEnd)
        {

        }
    }

    protected override void TriggerEnter2D_Exec(UnityEngine.Object p_Object)
    {
        if (p_Object == null) return;

        if (p_Object.GetType() == typeof(Player) && !m_bIgnore)
        {
            Player  a_Player = p_Object as Player;
            //  ダメージ処理
            a_Player.DamageToHitStageObject(creditDamage);

            //  無視する
            Ignore_On();
        }
    }

    protected override void TriggerExit2D_Exec(UnityEngine.Object p_Object)
    {
    }

    protected override void CollisionEnter2D_Exec(UnityEngine.Object p_Object)
    {
        if (p_Object == null) return;

        //  無視する
        if (m_bIgnore)  return;

        if (p_Object != null)
        {
        }        
    }

    protected override void CollisionExit2D_Exec(UnityEngine.Object p_Object)
    {

    }
}
