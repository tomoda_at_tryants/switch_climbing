﻿using nn.hid;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    static string eTAG
    {
        get { return typeof(Player).ToString(); }
    }


    /// <summary>
    /// 腕の伸びる距離を決めるタイプ
    /// </summary>
    public enum stSwingPowerType
    {
        None = -1,

        Acceleration,   //  加速度
        Gage            //  ゲージ
    }

    /// <summary>
    /// 腕が伸びる距離を決める操作方法
    /// </summary>
    static stSwingPowerType s_SwingPowerType = stSwingPowerType.Gage;

    /// <summary>
    /// 加速度タイプか
    /// </summary>
    public static bool isAcceleration
    {
        get { return (s_SwingPowerType == stSwingPowerType.Acceleration) ? true : false; }
    }

    /// <summary>
    /// ゲージタイプか
    /// </summary>
    public static bool isGage
    {
        get { return (s_SwingPowerType == stSwingPowerType.Gage) ? true : false; }
    }


    public enum stInputType
    {
        None = -1,

        /// <summary>
        /// ボタンを押している間手を離す
        /// </summary>
        PushingToRelease,

        /// <summary>
        /// ボタンを押している間掴まる
        /// </summary>
        PushingToCatch
    }

    /// <summary>
    /// 入力タイプ
    /// </summary>
    static stInputType s_InputType = stInputType.PushingToCatch;

    /// <summary>
    /// 加速度タイプか
    /// </summary>
    public static bool isPushingToRelease
    {
        get { return (s_InputType == stInputType.PushingToRelease) ? true : false; }
    }

    /// <summary>
    /// 加速度タイプか
    /// </summary>
    public static bool isPushingToCatch
    {
        get { return (s_InputType == stInputType.PushingToCatch) ? true : false; }
    }

    public const int c_nHPMax = 1000;
    public static readonly Color c_DefaultColor = Color.green;

    public static readonly Dictionary<Switch_def.eHandle, NpadButton> r_ReleaseObjectButtonDic = new Dictionary<Switch_def.eHandle, NpadButton>()
    {
        { Switch_def.eHandle.Right, NpadButton.ZR },
        { Switch_def.eHandle.Left, NpadButton.ZL }
    };

    public static readonly Dictionary<Switch_def.eHandle, NpadButton> r_TargetPreviousButtonDic = new Dictionary<Switch_def.eHandle, NpadButton>()
    {
        { Switch_def.eHandle.Right, NpadButton.R },
        { Switch_def.eHandle.Left, NpadButton.L }
    };

    public static readonly Dictionary<Switch_def.eHandle, NpadButton> r_CalibrationButtonDic = new Dictionary<Switch_def.eHandle, NpadButton>()
    {
        { Switch_def.eHandle.Right, NpadButton.StickR },
        { Switch_def.eHandle.Left, NpadButton.StickL }
    };

    public static readonly Dictionary<Switch_def.eHandle, NpadButton> r_TargetPreviousObjButtonDic = new Dictionary<Switch_def.eHandle, NpadButton>()
    {
        { Switch_def.eHandle.Right, NpadButton.StickR },
        { Switch_def.eHandle.Left, NpadButton.StickL }
    };

    public Rigidbody2D bodyRoot;

    public PlayerArm arm_l;
    public PlayerArm arm_r;

    public Image head;

    public Image body;

    public Rigidbody2D leg_r;
    public Rigidbody2D leg_l;

    public Image leg_r_image;
    public Image leg_l_image;

    public CapsuleCollider2D bodyCollider;

    private Dictionary<Switch_def.eHandle, PlayerArm> m_ArmDic;

    private List<PlayerArm> m_ArmList;
    public List<PlayerArm> armList
    {
        get { return m_ArmList; }
    }

    private List<Image> m_ImageList;

    public GameObject cameraTrackingTarget;

    int m_nHp;
    public int hp
    {
        get { return m_nHp; }
        set { m_nHp = value; }
    }

    public bool isEmptyHp
    {
        get
        {
            return (m_nHp <= 0) ? true : false;
        }
    }

    float   m_fDamageTime;
    float   m_fDamageInterval;

    /// <summary>
    /// ユーザーの操作が有効か
    /// </summary>
    bool    m_bControlEnable;

    /// <summary>
    /// 無敵
    /// </summary>
    bool    m_bInvincible;

    [SerializeField]
    private Slider m_HpGage;

    [SerializeField]
    private PlayerPowerGage m_PowerGage;

    [SerializeField]
    List<Image> m_BodyImageList;

    public delegate void GameOverDelegate();
    public GameOverDelegate gameOverCallback;

    /// <summary>
    /// スイング用パラメータ
    /// </summary>
    public struct stSwingParam
    {
        /// <summary>
        /// 最大スイングスピードの最低値
        /// </summary>
        public const float c_fSwingSpeedMin = 2.7f;

        /// <summary>
        /// 最大スイングスピードの最大値
        /// </summary>
        public const float c_fSwingSpeedMax = 15f;

        public Switch_def.eHandle handle;
        public float beforeSwingSpeed;
        public Vector2 swingStartPos;
        public float swingEndAngleY;

        float m_fSwingSpeedMax;
        public float swingSpeedMax
        {
            get
            {
                return m_fSwingSpeedMax;
            }

            set
            {
                value = Mathf.Clamp(value, c_fSwingSpeedMin, c_fSwingSpeedMax);
                m_fSwingSpeedMax = value;
            }
        }

        /// <summary>
        /// スイング中か
        /// </summary>
        public bool isSwinging
        {
            get
            {
                return (swingSpeed >= c_fSwingSpeedMin) ? true : false;
            }
        }

        /// <summary>
        /// スイングが終了したか
        /// </summary>
        public bool isSwingEnd
        {
            get
            {
                return (!isSwinging && (beforeSwingSpeed >= c_fSwingSpeedMin)) ? true : false;
            }
        }

        /// <summary>
        /// コントローラーのスイングスピード
        /// </summary>
        public float swingSpeed
        {
            get
            {
                //  スイングスピードはコントローラーのすべての軸に対する加速度値の絶対値の合計で算出する
                return SwitchApiManager.instance.NpadMan.GetAllAddAcceleration(handle);
            }
        }

        /// <summary>
        /// スイングの最大速度に対する距離
        /// </summary>
        public float swingDistance
        {
            get
            {
                return m_fSwingSpeedMax * 60f;
            }
        }

        /// <summary>
        /// スイングの距離に到達するまでの時間
        /// </summary>
        public float swingReachTime
        {
            get
            {
                return swingDistance / 550f;
            }
        }

        public void Update()
        {
            //  スイングが開始していなければ終了
            if (!isSwinging) return;

            //  現在のスイングスピードを登録
            beforeSwingSpeed = swingSpeed;
            //  最大スイング速度を比較
            if (m_fSwingSpeedMax <= swingSpeed)
            {
                m_fSwingSpeedMax = swingSpeed;
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="p_Handle"></param>
        public stSwingParam(Switch_def.eHandle p_Handle)
        {
            handle = p_Handle;
            beforeSwingSpeed = 0;
            swingStartPos = Vector2.zero;
            swingEndAngleY = 0;

            m_fSwingSpeedMax = 0;

            Reset();
        }

        /// <summary>
        /// パラメータのリセット
        /// </summary>
        public void Reset()
        {
            swingEndAngleY = 0;
            beforeSwingSpeed = 0;
            m_fSwingSpeedMax = 0;
            swingStartPos = Vector2.zero;
        }
    }

    float m_fDefaultGravityScale;

    /// <summary>
    /// 両手が離れている
    /// </summary>
    public bool isBothHandsReleasing
    {
        get
        {
            bool a_bRelease = false;
#if MAP_TYPE_STAGEOBJ
            a_bRelease = true;
            foreach (PlayerArm a_Arm in m_ArmDic.Values)
            {
                if (a_Arm.catchObj != null)
                {
                    a_bRelease = false;
                    break;
                }
            }
#endif
            return a_bRelease;
        }
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <returns></returns>
    public static Player Create(System.Action p_InitCompleted)
    {
        GameObject a_Obj = ResourceManager.PrefabLoadAndInstantiate(eTAG, Vector3.zero);
        Player me = a_Obj.GetComponentInChildren<Player>();

        me.Init(p_InitCompleted);

        return me;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init(System.Action p_InitCompleted)
    {
        transform.parent.SetParent(GameCanvas.instance.playerView);
        transform.parent.localPosition = Vector3.zero;

        m_nHp = c_nHPMax;
        m_bControlEnable = false;

        m_fDamageTime       = 0;
        m_fDamageInterval   = 0.02f;

        m_fDefaultGravityScale = bodyRoot.gravityScale;

        //  HPゲージの作成
        CreateHPGage();

        if (s_SwingPowerType == stSwingPowerType.Gage)
        {
            //  パワーゲージ初期化
            m_PowerGage.Init();
        }

        m_ArmDic = new Dictionary<Switch_def.eHandle, PlayerArm>()
        {
            { Switch_def.eHandle.Left, arm_l },
            { Switch_def.eHandle.Right, arm_r }
        };
        //  リストに追加
        m_ArmList = new List<PlayerArm>();
        m_ArmList.Add(arm_l);
        m_ArmList.Add(arm_r);

        m_ImageList = new List<Image>();
        foreach (Image a_Image in transform.GetComponentsInChildren<Image>())
        {
            m_ImageList.Add(a_Image);
            if (a_Image.name != "CatchPivot")
            {
                a_Image.color = c_DefaultColor;
            }
        }

        head.color = new Color(0 / 255f, 255 / 255f, 127f / 255f);

        foreach (PlayerArm a_Arm in m_ArmList)
        {
            a_Arm.image.color = new Color(40f / 255f, 220f / 255f, 50f / 255f);
        }

        leg_l_image.color = new Color(40f / 255f, 220f / 255f, 50f / 255f);
        leg_r_image.color = new Color(40f / 255f, 220f / 255f, 50f / 255f);

        //  コールバックの指定
        foreach (PlayerArm a_Arm in m_ArmDic.Values)
        {
            a_Arm.catchSuccessCallback += CatchSuccessCallback;
            a_Arm.releaseHandCallback += ReleaseHand_Exec;
            a_Arm.getCatchObjIDToReverseHandCallback += GetCatchObjIDToReverseHand;
        }

        //  腕オブジェクトの初期化
        foreach (PlayerArm a_Arm in m_ArmList)
        {
            a_Arm.Init(m_PowerGage);
        }

        //  コールバックの呼び出し
        if (p_InitCompleted != null)
        {
            p_InitCompleted();
        }
    }

    /// <summary>
    /// 操作を無効にする
    /// </summary>
    public void SetControllDisable()
    {
        m_bControlEnable = false;
    }

    public void Shake(PlayerArm p_Arm)
    {
        float   a_fAcceleration = 0;
        //  何かに掴まっている
        if (p_Arm.isInputCatching && !p_Arm.falling)
        {
            //  JoyConの加速度を合計した値を取得
            a_fAcceleration = SwitchApiManager.instance.NpadMan.GetAllAddAcceleration(p_Arm.handle);
        }

        if (a_fAcceleration >= stSwingParam.c_fSwingSpeedMin)
        {
            float a_fPower = 500f * ((p_Arm.handle == Switch_def.eHandle.Right) ? -1f : 1f);
            //  コントローラーを振った方向に体を揺らす
            bodyRoot.AddForce(new Vector2(a_fPower, 0), ForceMode2D.Impulse);

            p_Arm.SwingParamReset();
        }
    }

    public void Falling()
    {
        float   a_fMax          = m_fDefaultGravityScale * 2.5f;     //  デフォルトのGravityScaleの2.5倍が最大
        float   a_fIncreadValue = 1.006f;                     //  毎フレーム０．６％GravityScaleが増加する

        m_PowerGage.EnableView(false);
        if (bodyRoot.gravityScale <= a_fMax)
        {
            bodyRoot.gravityScale *= a_fIncreadValue;   //  毎フレーム0.5%増加
        }
        else
        {
            bodyRoot.gravityScale = a_fMax;
        }
    }

    public void Update_Exec()
    {
       //   操作が有効でない
        if (!m_bControlEnable)  return;

        //  プレイヤーの腕オブジェクトの更新
        foreach (PlayerArm a_Arm in m_ArmList)
        {
            a_Arm.Update_Exec();
            a_Arm.swingParam.Reset();

            Shake(a_Arm);
        }

        //  両手とも掴んでいるオブジェクトが無い
        bool    a_bFalling = isBothHandsReleasing;
        foreach (PlayerArm a_Arm in m_ArmDic.Values)
        {
            a_Arm.falling = a_bFalling;
        }

        //  落下中はパワーゲージを停止する
        if (isBothHandsReleasing)
        {
            Falling();
        }
        else
        {
            if (bodyRoot.gravityScale != m_fDefaultGravityScale)
            {
                bodyRoot.gravityScale = m_fDefaultGravityScale;
            }
        }

        m_fDamageTime += Time.deltaTime;
        if (m_fDamageTime >= m_fDamageInterval)
        {
            m_fDamageTime = 0;
            //  ダメージ
            HP_Damage(1);
        }

        if (isEmptyHp)
        {
        }
    }

    private void FixedUpdate()
    {
    }

    void LateUpdate()
    {
    }

    public void GameOver_Exec()
    {
        StartCoroutine(GameOver_Coroutine());
    }
    IEnumerator GameOver_Coroutine()
    {
        while (true)
        {
            gameOverCallback();

            foreach (PlayerArm a_Arm in m_ArmList)
            {
                a_Arm.handRigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;
            }

            yield return 0;
        }
    }

    public void GameClear_Exec()
    {
        StartCoroutine(GameClear_Coroutine());
    }
    IEnumerator GameClear_Coroutine()
    {
        yield return 0;
    }

    /// <summary>
    /// ゴールに辿り着いた
    /// </summary>
    public bool isGoal
    {
        get
        {
            bool a_bGoal = false;
            foreach(PlayerArm a_Arm in m_ArmList)
            {
                if(a_Arm.catchObj != null)
                {
                    if(a_Arm.catchObj.goal)
                    {
                        a_bGoal = true;
                        break;
                    }
                }
            }

            return  a_bGoal;
        }
    }

    void    CreateHPGage()
    {
        if (m_HpGage != null)   return;

        m_HpGage = ResourceManager.PrefabLoadAndInstantiate<Slider>("PlayerHPBar");
        m_HpGage.transform.SetParent(TemplateCanvas.instance.panelMid);
        m_HpGage.GetComponent<RectTransform>().anchoredPosition = new Vector3(30, 10);
    }

    public float    GetPowerGageValue()
    {
        if (s_SwingPowerType != stSwingPowerType.Gage)  return 0;

        return  m_PowerGage.power;
    }

    /// <summary>
    /// 手を離した時に呼ばれるコールバック
    /// </summary>
    /// <param name="p_ReleaseHandle"></param>
    /// <returns></returns>
    public bool     ReleaseHand_Exec(Switch_def.eHandle p_ReleaseHandle)
    {
#if MAP_TYPE_STAGEOBJ
        //  引数で渡された手とは逆の手を取得
        Switch_def.eHandle  a_Handle    = GetReverseHand(p_ReleaseHandle);
        CatchObject         a_CatchObj  = m_ArmDic[a_Handle].catchObj;
        if (a_CatchObj == null)
        {
            return  false;
        }
        else
        {
            //  手が離れた方のキャッチオブジェクトを掴まっている方のキャッチオブジェクトに合わせる
            m_ArmDic[p_ReleaseHandle].catchObj = m_ArmDic[a_Handle].catchObj;
        }
#endif

        return  true;
    }

    /// <summary>
    /// 指定した手の逆の手が掴まっているキャッチオブジェクトのIDを取得する
    /// </summary>
    /// <returns></returns>
    public int  GetCatchObjIDToReverseHand(Switch_def.eHandle p_Handle)
    {
        //  引数で渡された手とは逆の手を取得
        Switch_def.eHandle  a_Handle    = GetReverseHand(p_Handle);
        CatchObject         a_CatchObj  = m_ArmDic[a_Handle].catchObj;

        if (a_CatchObj == null)  return -1;
        return  a_CatchObj.id;
    }

    /// <summary>
    /// 指定した手と逆の値を返す
    /// </summary>
    Switch_def.eHandle  GetReverseHand(Switch_def.eHandle p_Handle)
    {
        Switch_def.eHandle  a_Handle = (p_Handle == Switch_def.eHandle.Left) ? Switch_def.eHandle.Right : Switch_def.eHandle.Left;
        return  a_Handle;
    }

    /// <summary>
    /// キャッチが成功した時に呼ばれるコールバック
    /// </summary>
    void    CatchSuccessCallback()
    {
        HP_Recovery(100);
    }

    /// <summary>
    /// 回復処理
    /// </summary>
    /// <param name="p_nValue"></param>
    void    HP_Recovery(int p_nValue)
    {
        m_nHp += p_nValue;
        if(m_nHp > c_nHPMax)    m_nHp = c_nHPMax;

        m_HpGage.value = m_nHp;
        m_HpGage.GraphicUpdateComplete();
    }

    /// <summary>
    /// ダメージ処理
    /// </summary>
    /// <param name="p_nValue"></param>
    void HP_Damage(int p_nValue)
    {
        m_nHp -= p_nValue;

        if (m_nHp < 0) m_nHp = 0;

        if (m_HpGage != null)
        {
            m_HpGage.value = m_nHp;
            m_HpGage.GraphicUpdateComplete();
        }
    }

    /// <summary>
    /// キャッチオブジェクトをセットする
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="p_Handle"></param>
    /// <param name="p_CatchObj"></param>
    public void SetCatchObj<T>(Switch_def.eHandle p_Handle, T p_CatchObj) where T : Object
    {
        if(typeof(T) == typeof(CatchObject))
        {
            m_ArmDic[p_Handle].SetCatchObj(p_CatchObj);
        }
    }
    
    /// <summary>
    /// ゲームスタート可能な状態か
    /// </summary>
    public bool isEnableGameStart
    {
        get
        {
            bool    a_bEnableGameStart = true;
            foreach (PlayerArm a_Arm in m_ArmList)
            {
                //  掴んでいない
                if (!a_Arm.isInputCatching)
                {
                    a_bEnableGameStart = false;
                }
            }

            m_bControlEnable = a_bEnableGameStart;

            return  a_bEnableGameStart;
        }
    }

    void Update()
    {
    }

    /// <summary>
    /// オブジェクトに当たった際のダメージ処理
    /// </summary>
    /// <param name="p_nDamage"></param>
    public void DamageToHitStageObject(int p_nDamage)
    {
        if (m_bInvincible)  return;

        m_bInvincible = true;   //  無敵状態にする

        //  振動処理
        SwitchApiManager.instance.vibrationMan.Execute(1, 0, 0.15f, Switch_def.eHandle.Both);

        //  点滅処理
        Blink_On(1.5f, 
            //  処理の完了時に呼ばれる
            ()=>
            {
                m_bInvincible = false;
            }
        );
        HP_Damage(p_nDamage);
    }

    /// <summary>
    /// 点滅処理（ダメージを受けた際等）
    /// </summary>
    void    Blink_On(float p_bTime, System.Action p_BlinkFinished)
    {
        StartCoroutine(Blink_Coroutine(p_bTime, p_BlinkFinished));
    }

    /// <summary>
    /// 点滅
    /// </summary>
    /// <returns></returns>
    IEnumerator  Blink_Coroutine(float p_fTime, System.Action p_BlinkFinished)
    {
        float a_fAlpha          = 1f;
        float a_fElapsedTime    = 0;
        while(a_fElapsedTime < p_fTime)
        {
            a_fElapsedTime += Time.deltaTime;

            a_fAlpha = (a_fAlpha == 1) ? 0 : 1;
            foreach(Image a_Image in m_BodyImageList)
            {
                if (a_Image.name == "PlayerPowerGage") continue;

                Color a_Color = a_Image.color;
                a_Color.a = (a_fAlpha == 1) ? 0 : 1;

                a_Image.color = a_Color;
            }
      
            if (a_fElapsedTime >= p_fTime)  break;

            yield return 0;
        }

        foreach (Image a_Image in m_BodyImageList)
        {
            Color a_Color = a_Image.color;
            a_Color.a = 1;

            a_Image.color = a_Color;
        }

        if(p_BlinkFinished != null)
        {
            p_BlinkFinished();
        }
    }
}

