﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using System;

public class ScrollBgManager : MonoBehaviour
{


    public string eTAG
    {
        get
        {
            System.Type a_Type = GetType();
            return a_Type.ToString();
        }
    }

    /// <summary>
    /// 最大表示背景（4枚まで）
    /// 
    /// </summary>
    public const int c_nBgMax = 4;

    /// <summary>
    /// 背景データ
    /// </summary>
    public ScrollBgData[] bgDatas;

    /// <summary>
    /// 表示対象のカメラ
    /// </summary>
    Camera m_ViewCamera;

    /// <summary>
    /// カメラ座標
    /// </summary>
    Vector2 m_CameraPos
    {
        get
        {
            return m_ViewCamera.transform.localPosition;
        }
    }

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static ScrollBgManager Create(Camera p_ViewCamera)
    {
        ScrollBgManager me = ResourceManager.PrefabLoadAndInstantiate<ScrollBgManager>("ScrollBgManager");

        me.Init(p_ViewCamera);

        return me;
    }

    public void Init(Camera p_ViewCamera)
    {
        transform.SetParent(GameCanvas.instance.backgroundView);
        transform.localPosition = Vector3.zero;

        m_ViewCamera = p_ViewCamera;

        transform.localPosition = new Vector2(-640, -360);


        //  背景画像の設定
        foreach (ScrollBgData a_BgData in bgDatas)
        {
            string a_ImagePath = "bg/bg_stage001";
            a_BgData.Init(a_ImagePath);

            a_BgData.enable = false;
        }
    }

    void Update()
    {
        bool a_bNoView = true;
        foreach (ScrollBgData a_BgData in bgDatas)
        {
            if (a_BgData.enable)
            {
                a_bNoView = false;
                break;
            }
        }
        //  背景が全て表示されていなかったら表示
        if (a_bNoView)
        {
            bgDatas[0].transform.localPosition = m_CameraPos;
            bgDatas[0].enable = true;
        }

        foreach (ScrollBgData a_BgData in bgDatas)
        {
            //  描画されていない背景は処理を行わない
            if (!a_BgData.isView || !a_BgData.enable)   continue;

            //  未使用の背景取得
            ScrollBgData a_UnUseBg = GetUnUseImage();
            if (a_UnUseBg == null) break;

            //  背景：上側
            if (a_BgData.edgeManager.top.viewPort.y < 1)
            {
                Vector3 a_ViewPos = new Vector3(a_BgData.transform.localPosition.x, a_BgData.transform.localPosition.y + ScrollBgData.c_ImageSize.y);

                if (!AlreadyViewToPos(a_ViewPos))
                {
                    a_UnUseBg.transform.localPosition = a_ViewPos;
                    a_UnUseBg.enable = true;
                }
            }

            //  未使用の背景取得
            a_UnUseBg = GetUnUseImage();
            if (a_UnUseBg == null) break;

            //  背景：下側
            if (a_BgData.edgeManager.bottom.viewPort.y > 0)
            {
                Vector3 a_ViewPos = new Vector3(a_BgData.transform.localPosition.x, a_BgData.transform.localPosition.y - ScrollBgData.c_ImageSize.y);

                if (!AlreadyViewToPos(a_ViewPos))
                {
                    a_UnUseBg.transform.localPosition = a_ViewPos;
                    a_UnUseBg.enable = true;
                }
            }

            //  未使用の背景取得
            a_UnUseBg = GetUnUseImage();
            if (a_UnUseBg == null)  break;

            //  背景：左側
            if (a_BgData.edgeManager.left.viewPort.x > 0)
            {
                Vector3 a_ViewPos = new Vector3(a_BgData.transform.localPosition.x - ScrollBgData.c_ImageSize.x, a_BgData.transform.localPosition.y);

                if (!AlreadyViewToPos(a_ViewPos))
                {
                    a_UnUseBg.transform.localPosition = a_ViewPos;
                    a_UnUseBg.enable = true;
                }
            }

            //  未使用の背景取得
            a_UnUseBg = GetUnUseImage();
            if (a_UnUseBg == null) break;

            //  背景：右側
            if (a_BgData.edgeManager.right.viewPort.x < 1)
            {
                Vector3 a_ViewPos = new Vector3(a_BgData.transform.localPosition.x + ScrollBgData.c_ImageSize.x, a_BgData.transform.localPosition.y);

                if (!AlreadyViewToPos(a_ViewPos))
                {
                    a_UnUseBg.transform.localPosition = a_ViewPos;
                    a_UnUseBg.enable = true;
                }
            }
        }

        foreach(ScrollBgData a_BgData in bgDatas)
        {
            //  重なっている背景がある時には非表示にする
            if(AlreadyViewToPos(a_BgData))
            {
                a_BgData.enable = false;
            }

            //  既に画面外にいる背景は非表示にする
            if(a_BgData.enable)
            {
                if(!a_BgData.isView)
                {
                    a_BgData.enable = false;
                }
            }
        }
    }

    /// <summary>
    /// 指定した座標に既に背景が表示されているか調べる
    /// </summary>
    /// <param name="p_Pos"></param>
    /// <returns></returns>
    public bool AlreadyViewToPos(Vector3 p_Pos)
    {
        foreach (ScrollBgData a_BgData in bgDatas)
        {
            if (a_BgData.enable)
            {
                if (((int)a_BgData.transform.localPosition.x == (int)p_Pos.x) &&
                   ((int)a_BgData.transform.localPosition.y == (int)p_Pos.y))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// 指定した背景データの座標に既に背景が表示されているか調べる
    /// </summary>
    /// <param name="p_CheckBgData"></param>
    /// <returns></returns>
    public bool AlreadyViewToPos(ScrollBgData p_CheckBgData)
    {
        foreach (ScrollBgData a_BgData in bgDatas)
        {
            if(a_BgData.enable && p_CheckBgData != a_BgData)
            {
                if(((int)a_BgData.transform.localPosition.x == (int)p_CheckBgData.transform.localPosition.x) &&
                  ((int)a_BgData.transform.localPosition.y == (int)p_CheckBgData.transform.localPosition.y))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// 使用されていない背景データを取得する
    /// </summary>
    ScrollBgData    GetUnUseImage()
    {
        ScrollBgData    a_UnUseBg = null;
        foreach (ScrollBgData a_BgData in bgDatas)
        {
            if (!a_BgData.enable)
            {
                a_UnUseBg = a_BgData;
            }
        }

        return  a_UnUseBg;
    }
}