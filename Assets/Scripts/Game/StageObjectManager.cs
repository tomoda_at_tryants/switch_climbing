﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageObjectManager : MonoBehaviour
{
    public static string    eTAG
    {
        get
        {
            return typeof(StageObjectManager).ToString();
        }
    }

    List<StageObjectBase>           m_ObjList;
    public List<StageObjectBase>    objList
    {
        get { return m_ObjList; }
    }

    bool    m_bEditor;
    Canvas  m_CanvasRoot;

    public static StageObjectManager    Create(bool p_bEditor, Canvas p_CanvasRoot)
    {
        GameObject  a_Obj = new GameObject(eTAG);
        StageObjectManager  me = a_Obj.AddComponent<StageObjectManager>();

        me.Init(p_bEditor, p_CanvasRoot);

        return  me;
    }

    /// <summary>
    /// ステージオブジェクトを生成
    /// </summary>
    public StageObjectBase CreateObject(StageObjectBase.eType p_Type, Vector2 p_Pos)
    {
        string          a_Name  = "StageObject/" + "StageObject_" + p_Type.ToString().ToLower();
        StageObjectBase me      = ResourceManager.PrefabLoadAndInstantiate<StageObjectBase>(a_Name);

        me.Init(p_Type, p_Pos);

        m_ObjList.Add(me);

        return me;
    }

    /// <summary>
    /// タッチ座標から生成
    /// </summary>
    /// <param name="p_Type"></param>
    /// <param name="p_TouchPos"></param>
    public StageObjectBase  CreateObjectToTouch(StageObjectBase.eType p_Type, Vector2 p_TouchPos)
    {
        if (!m_bEditor) return null;

        Vector3 a_CameraPos = m_CanvasRoot.worldCamera.transform.localPosition;
        Vector2 a_CreatePos = new Vector2(p_TouchPos.x + a_CameraPos.x - (Screen.width / 2), p_TouchPos.y + a_CameraPos.y - (Screen.height / 2));

        StageObjectBase a_StageObj = CreateObject(p_Type, a_CreatePos);

        return a_StageObj;
    }

    public delegate void MapEditor_DestroyObjectDelegate(StageObjectBase p_CatchObj);
    public static MapEditor_DestroyObjectDelegate destroyObjectCallback;

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="p_bEditor"></param>
    /// <param name="p_CanvasRoot"></param>
    public void Init(bool p_bEditor, Canvas p_CanvasRoot)
    {
        m_bEditor       = p_bEditor;
        m_CanvasRoot    = p_CanvasRoot;

        m_ObjList = new List<StageObjectBase>();

        destroyObjectCallback += (p_SelectObj) =>
        {
            for (int i = m_ObjList.Count - 1; i >= 0; i--)
            {
                //  削除対象のオブジェクトはリストから削除
                if (m_ObjList[i] == p_SelectObj)
                {
                    DestroyImmediate(m_ObjList[i].gameObject);

                    m_ObjList.Remove(p_SelectObj);
                    return;
                }
            }
        };
    }

    private void Update()
    {
        for (int i = m_ObjList.Count - 1 ; i > 0 ; i--)
        {
            StageObjectBase a_StageObj = m_ObjList[i];
            if(a_StageObj == null)
            {
                m_ObjList.Remove(a_StageObj);
                continue;
            }

            a_StageObj.Update_Exec();
        }
    }
}
