﻿using System;
using System.Text;
using UnityEngine;

public class CatchObjectData : BaseObjectData
{
    public enum eType
    {
        None = -1,

        Rock001,    //  岩
        Branch001,  //  枝
        Goal001,    //  ゴール

        Max
    }

    [SerializeField]
    eType m_Type;
    public eType type
    {
        get { return m_Type; }
        set { m_Type = value; }
    }

    bool m_bStartToCatch;
    /// <summary>
    /// スタート時に掴んでいるオブジェクト
    /// </summary>
    public bool startToCatch
    {
        get { return m_bStartToCatch; }
        set { m_bStartToCatch = value; }
    }

    bool m_bGoal;
    /// <summary>
    /// ゴールフラグ
    /// </summary>
    public bool goal
    {
        get { return m_bGoal; }
        set { m_bGoal = value; }
    }

    [SerializeField]
    stDstID m_DstID;
    public stDstID dstID
    {
        get { return m_DstID; }
        set { m_DstID = value; }
    }

    /// <summary>
    /// 次（前）の行先を登録するID
    /// </summary>
    [System.Serializable]
    public struct stDstID
    {
        [SerializeField]
        int m_nL_Previous;
        public int l_Previous { get { return m_nL_Previous; } }

        [SerializeField]
        int m_nR_Previous;
        public int r_Previous { get { return m_nR_Previous; } }

        [SerializeField]
        int m_nL_Next;
        public int l_next { get { return m_nL_Next; } set { m_nL_Next = value; } }

        [SerializeField]
        int m_nR_Next;
        public int r_next { get { return m_nR_Next; } }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="p_nR_Next"></param>
        /// <param name="p_nR_Previous"></param>
        /// <param name="p_nL_Next"></param>
        /// <param name="p_nL_Previous"></param>
        public stDstID(int p_nL_Previous, int p_nR_Previous, int p_nL_Next, int p_nR_Next)
        {
            m_nL_Previous = p_nL_Previous;
            m_nR_Previous = p_nR_Previous;
            m_nL_Next = p_nL_Next;
            m_nR_Next = p_nR_Next;

        }
    }

    /// <summary>
    /// IDを取得する
    /// </summary>
    /// <param name="p_Handle"></param>
    /// <param name="p_bNext"></param>
    /// <returns></returns>
    public int GetID(Switch_def.eHandle p_Handle, bool p_bNext)
    {
        int a_nID = -1;
        if (p_Handle == Switch_def.eHandle.Left)
        {
            a_nID = (p_bNext) ? dstID.l_next : dstID.l_Previous;
        }
        else
        if (p_Handle == Switch_def.eHandle.Right)
        {
            a_nID = (p_bNext) ? dstID.r_next : dstID.r_Previous;
        }

        return a_nID;
    }

    /// <summary>
    /// IDを取得する
    /// </summary>
    /// <param name="p_Handle"></param>
    /// <param name="p_bNext"></param>
    /// <returns></returns>
    public void SetID(int p_nID, Switch_def.eHandle p_Handle, bool p_bNext)
    {
        if (p_Handle == Switch_def.eHandle.Left)
        {
            if (p_bNext)
            {
                m_DstID = new stDstID(dstID.l_Previous, dstID.r_Previous, p_nID, dstID.r_next);
            }
            else
            {
                m_DstID = new stDstID(p_nID, dstID.r_Previous, dstID.l_next, dstID.r_next);
            }
        }
        else
        if (p_Handle == Switch_def.eHandle.Right)
        {
            if (p_bNext)
            {
                m_DstID = new stDstID(dstID.l_Previous, dstID.r_Previous, dstID.l_next, p_nID);
            }
            else
            {
                m_DstID = new stDstID(dstID.l_Previous, p_nID, dstID.l_next, dstID.r_next);
            }
        }
    }

    /// <summary>
    /// パラメータをセットする
    /// </summary>
    /// <param name="p_nID"></param>
    /// <param name="p_Type"></param>
    /// <param name="p_nPreviousID_Left"></param>
    /// <param name="p_nPreviousID_Right"></param>
    /// <param name="p_nNextID_Left"></param>
    /// <param name="p_nNextID_Right"></param>
    public void ParamSet(int p_nID, eType p_Type, Vector2 p_DefaultPos, Vector2 p_DefaultScale, Vector3 p_DefaultAngle, bool p_bStartToCatch, bool p_bGoal, stDstID p_DstID)
    {
        id                  = p_nID;
        type                = p_Type;
        defaultPos          = p_DefaultPos;
        defaultScale        = p_DefaultScale;
        defaultAngle        = p_DefaultAngle;
        m_bStartToCatch     = p_bStartToCatch;
        m_bGoal             = p_bGoal;
        m_DstID             = p_DstID;
    }

    /// <summary>
    /// パラメータをセットする
    /// </summary>
    /// <param name="p_Pos"></param>
    /// <param name="p_Data"></param>
    public void ParamSet(CatchObjectData p_Data)
    {
        ParamSet(p_Data.id, p_Data.type, p_Data.defaultPos, p_Data.defaultScale, p_Data.defaultAngle, p_Data.startToCatch, p_Data.goal, p_Data.dstID);
    }

    public void ParamSet(string[] p_ParamStr)
    {
        ParamSet(
            int.Parse(p_ParamStr[0]),
            (CatchObjectData.eType)Enum.Parse(typeof(CatchObjectData.eType), p_ParamStr[1]),
            new Vector2(float.Parse(p_ParamStr[2]), float.Parse(p_ParamStr[3])),    //  DefaultPos
            new Vector2(float.Parse(p_ParamStr[4]), float.Parse(p_ParamStr[5])),    //  DefaultScale
            new Vector3(float.Parse(p_ParamStr[6]), float.Parse(p_ParamStr[7]), float.Parse(p_ParamStr[8])),    //  DefaultAngle
            bool.Parse(p_ParamStr[9]),
            bool.Parse(p_ParamStr[10]),
            new stDstID(
                int.Parse(p_ParamStr[11]),
                int.Parse(p_ParamStr[12]),
                int.Parse(p_ParamStr[13]),
                int.Parse(p_ParamStr[14])
            )
        );
    }

    /// <summary>
    /// string型のフォーマットで取得する
    /// </summary>
    /// <returns></returns>
    public string GetStringFormat()
    {
        try
        {
            StringBuilder   a_Data = new StringBuilder();

            a_Data.Append(id.ToString() + ",");
            a_Data.Append(type.ToString() + ",");

            a_Data.Append(transform.localPosition.x.ToString() + ",");
            a_Data.Append(transform.localPosition.y.ToString() + ",");

            a_Data.Append(transform.localScale.x.ToString() + ",");
            a_Data.Append(transform.localScale.y.ToString() + ",");

            a_Data.Append(transform.localEulerAngles.x.ToString() + ",");
            a_Data.Append(transform.localEulerAngles.y.ToString() + ",");
            a_Data.Append(transform.localEulerAngles.z.ToString() + ",");

            a_Data.Append(startToCatch.ToString() + ",");
            a_Data.Append(goal.ToString() + ",");

            a_Data.Append(m_DstID.l_Previous.ToString() + ",");
            a_Data.Append(m_DstID.r_Previous.ToString() + ",");
            a_Data.Append(m_DstID.l_next.ToString() + ",");
            a_Data.Append(m_DstID.r_next.ToString() + ",");

            a_Data.Append("\n");    //  改行

            return a_Data.ToString();
        }
        catch(Exception e)
        {
            Debug.Log("CatchObjectData/GetStringFormat():" + e);
        }

        return  "";
    }
}
