﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class CSVManager : MonoBehaviour
{
    /// <summary>
    /// ファイルが存在するか
    /// </summary>
    /// <param name="p_Bgm"></param>
    /// <returns></returns>
    public static bool isEnableFile(string p_FileName)
    {
        FileInfo    a_FileInfo = GetFileInfo(p_FileName);
        return      a_FileInfo.Exists;
    }

    /// <summary>
    /// FileInfoを取得する
    /// </summary>
    /// <param name="p_Bgm"></param>
    /// <returns></returns>
    static FileInfo GetFileInfo(string p_FileName)
    {
        if (string.IsNullOrEmpty(p_FileName)) return null;

        string      a_Path      = new StringBuilder().Append(Application.streamingAssetsPath)
                                                     .Append("/")
                                                     .Append(def.c_CSVPath)
                                                     .Append(p_FileName)
                                                     .Append(".csv")
                                                     .ToString();
        FileInfo    a_FileInfo  = new FileInfo(a_Path);

        return      a_FileInfo;
    }

    /// <summary>
    /// CSVを書き込む処理
    /// </summary>
    /// <param name="p_StageName"></param>
    /// <param name="p_Text"></param>
    public static void  Write(string p_FileName, string p_Text)
    {
        FileInfo    a_FileInfo = GetFileInfo(p_FileName);
        if (a_FileInfo == null && !a_FileInfo.Exists)
        {
            return;
        }

        StreamWriter    a_StreamWriter    = a_FileInfo.CreateText();
        //  ファイルに書き込みを行い、確定する
        a_StreamWriter.Write(p_Text);
        a_StreamWriter.Flush();
        a_StreamWriter.Close();
    }

    /// <summary>
    /// CSVを読み込む処理
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <returns></returns>
    public static string    Read(string p_FileName)
    {
        //  ファイル情報取得
        FileInfo    a_FileInfo  = GetFileInfo(p_FileName);

        if (a_FileInfo == null && !a_FileInfo.Exists)
        {
            return  "";
        }

        string  a_ReadText = "";
        try
        {
            using (StreamReader a_Reader = new StreamReader(a_FileInfo.OpenRead(), Encoding.UTF8))
            {
                a_ReadText = a_Reader.ReadToEnd();
            }
        }
        catch
        {
        }

        return  a_ReadText;
    }
}
