//#define		_DOWNLOAD_USE_CACHE_

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;

public class ResourceManager : MonoBehaviour
{
    public static string eTAG
    {
        get
        {
            return typeof(ResourceManager).ToString();
        }
    }

    static ResourceManager s_Instance;

    public static ResourceManager instance
    {
        get { return s_Instance; }
    }

    public static ResourceManager Create()
    {
        if (s_Instance != null)
        {
            return s_Instance;
        }

        s_Instance = new GameObject("ResourceManager").AddComponent<ResourceManager>();
        return s_Instance;
    }

    public static void Dispose(ResourceManager p_Man)
    {
        if (p_Man != null)
        {
            Destroy(p_Man.gameObject);
        }
    }

    public static void ColdInit()
    {
        Dispose(s_Instance);
        s_Instance = null;
    }

    // Use this for initialization
    void Start()
    {
        GameObject.DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// 任意のアセットを直接ＰＣのフォルダーから読み込む（Editor Only）
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="p_FilePath"></param>
    /// <param name="p_FileName"></param>
    /// <param name="p_FileExt"></param>
    /// <returns></returns>
    static System.Object LoadFromPCFolder<T>(string p_FilePath, string p_FileName, string p_FileExt)
    {
        System.Object a_Res = null;

        if (p_FilePath.StartsWith("file://"))
        {
            string a_Path = p_FilePath.Substring("file://".Length);
            string a_PrefName = p_FileName;
            int a_nCh;
            if ((a_nCh = a_PrefName.LastIndexOf("/")) != -1)
            {
                a_PrefName = a_PrefName.Substring(a_nCh + 1);
            }

            if (Directory.Exists(a_Path))
            {

                string[] a_FileNames = Directory.GetFiles(a_Path, a_PrefName + p_FileExt, SearchOption.AllDirectories);

                if ((a_FileNames == null) || (a_FileNames.Length == 0) && (p_FileExt == ".txt"))
                {
                    // 拡張子をcsvに変えて試してみる
                    a_FileNames = Directory.GetFiles(a_Path, a_PrefName + ".csv", SearchOption.AllDirectories);
                }

                if ((a_FileNames != null) && (a_FileNames.Length > 0))
                {
                    string a_FileName = a_FileNames[0].Substring(a_FileNames[0].IndexOf("/Assets/") + 1);
#if UNITY_EDITOR
                    a_Res = UnityEditor.AssetDatabase.LoadAssetAtPath(a_FileName, typeof(T));
#endif
                }

            }

        }

        return a_Res;
    }

    /// <summary>
    /// プレハブを読み込む
    /// </summary>
    /// <returns>
    /// The load.
    /// </returns>
    /// <param name='prefabName'>
    /// Prefab name.
    /// </param>
    /// <param name='groupNo'>
    /// Group no.
    /// </param>
    public static GameObject PrefabLoad(string p_PrefabPath)
    {
        GameObject a_Prefab = null;
        if (a_Prefab == null)
        {
            // 内蔵リソースから読み込む
            a_Prefab = (GameObject)Resources.Load(def.c_PrefabPath + p_PrefabPath, typeof(GameObject));
        }

        return a_Prefab;

    }

    /// <summary>
    /// プレハブを読み込む
    /// </summary>
    /// <param name="p_PrefName"></param>
    /// <param name="p_Pos"></param>
    /// <param name="p_Parent"></param>
    /// <returns></returns>
    public static GameObject PrefabLoadAndInstantiate(string p_PrefName, Vector3 p_Pos, Transform p_Parent = null)
    {
        GameObject pref = PrefabLoad(p_PrefName);

        if (pref == null)
        {
//            DebugManager.Log("PrefabLoadAndInstantiate " + p_PrefName);
//            DebugManager.Log("pref == null");

            return null;
        }

        GameObject  obj = (GameObject)Instantiate(pref, p_Pos, Quaternion.identity);
        if (p_Parent != null) obj.transform.parent = p_Parent;

        obj.transform.localPosition = p_Pos;
        obj.transform.localScale = Vector3.one;

        return obj;
    }

    /// <summary>
    /// プレハブを読み込んでインスタンスを生成する
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    /// <param name="prefabName"></param>
    /// <param name="pos"></param>
    /// <param name="parent"></param>
    /// <returns></returns>
    public static Type PrefabLoadAndInstantiate<Type>(string prefabName, Vector3 pos, Transform parent = null)
        where Type : Component
    {
        GameObject obj = PrefabLoadAndInstantiate(prefabName, pos, parent);

        Type a_Res = (Type)obj.GetComponent(typeof(Type).ToString());

        return a_Res;
    }

    /// <summary>
    /// プレハブを読み込んでインスタンスを生成する
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    /// <param name="prefabName"></param>
    /// <returns></returns>
    public static Type PrefabLoadAndInstantiate<Type>(string prefabName)
        where Type : Component
    {
        return PrefabLoadAndInstantiate<Type>(prefabName, Vector3.zero, null);
    }

    /// <summary>
    /// バイナリーデータを読み込む
    /// </summary>
    /// <param name="p_FileNameWithoutExt"></param>
    /// <returns></returns>
    public static byte[] ByteLoad(string p_FilePath)
    {
        byte[] a_Res = null;

        // アセットバンドル内にあるか？
        AssetBundle a_Bundle = null;
        string a_AssetName = null;

        if (a_Res == null)
        {

            // 任意のバンドルから読み込む
            TextAsset asset = (TextAsset)a_Bundle.LoadAsset(a_AssetName, typeof(TextAsset));
            if (asset != null)
            {
                a_Res = asset.bytes;
            }

        }

        if (a_Res == null)
        {
            // ストリーミングアセットから読み込む
            p_FilePath = Path.GetFileName(p_FilePath);

#if UNITY_ANDROID && !UNITY_EDITOR

				string fp = System.IO.Path.Combine(Application.streamingAssetsPath, p_FilePath + ".txt");
//fp = "file://"+fp;

				if (!fp.Contains("://"))
				{
					fp = "file://" + fp;
				}

				WWW www = new WWW(fp);
				while(!www.isDone)
				{
					System.Threading.Thread.Sleep(1);
				}
		
//				_Debug.Log("GameTextureLoader Loaded = " + www.text);
		
				a_Res = www.bytes;
#else
            string fp = System.IO.Path.Combine(Application.streamingAssetsPath, p_FilePath + ".txt");
            //fp = "file://"+fp;
            if (fp.Contains("://"))
            {

                WWW www = new WWW(fp);
                a_Res = www.bytes;

            }
            else
            {

                a_Res = System.IO.File.ReadAllBytes(fp);
            }
#endif
        }

        if (a_Res == null)
        {
            // バンドル指定が無いときは内蔵リソースから読み込む
            TextAsset asset = (TextAsset)Resources.Load(def.c_TextAssetPath + p_FilePath, typeof(TextAsset));
            if (asset != null)
            {
                a_Res = asset.bytes;
            }
        }

        return a_Res;
    }

    /// <summary>
    /// バイナリーデータを読み込む
    /// </summary>
    /// <param name="p_FileNameWithoutExt"></param>
    /// <param name="p_Bundle"></param>
    /// <returns></returns>
    public void ByteLoadAsync(string p_FileNameWithoutExt, System.Action<byte[]> p_Callback, bool p_bEnableAssetBundle)
    {
        StartCoroutine(ByteLoadAsync_Exec(p_FileNameWithoutExt, p_Callback, p_bEnableAssetBundle));
    }

    IEnumerator ByteLoadAsync_Exec(string p_FilePath, System.Action<byte[]> p_Callback, bool p_bEnableAssetBundle)
    {
        byte[] a_Res = null;

        AssetBundle a_Bundle = null;
        string a_AssetName = null;
        if (p_bEnableAssetBundle)
        {
#if false
#if UNITY_EDITOR
			// ダミーバンドルの場合はＰＣのフォルダーから直接読み込む
			if (def.c_bDbgAssetLoadFromFolder)
			{
				TextAsset assetPC = (TextAsset)LoadFromPCFolder<TextAsset>(p_Bundle.name, p_FileNameWithoutExt, ".bytes");
				if (assetPC != null)
				{
					a_Res = assetPC.bytes;
				}
			}
#endif
#endif

            if (a_Res == null)
            {
                // 任意のバンドルから読み込む
                AssetBundleRequest a_RQ = (AssetBundleRequest)a_Bundle.LoadAssetAsync<TextAsset>(a_AssetName);
                yield return a_RQ;

                TextAsset a_Text = (TextAsset)a_RQ.asset;

                if (a_Text != null)
                {
                    a_Res = a_Text.bytes;
                }

            }

        }

        if (a_Res == null)
        {
            // ストリーミングアセットから読み込む
            a_Res = File_LoadFromStreamingAssets(p_FilePath, "bytes");
        }

        if (a_Res == null)
        {
            // バンドル指定が無いときは内蔵リソースから読み込む
            ResourceRequest a_RQ = Resources.LoadAsync(def.c_BytesAssetPath + p_FilePath, typeof(TextAsset));


            yield return a_RQ;

            TextAsset a_Text = (TextAsset)a_RQ.asset;

            if (a_Text != null)
            {
                a_Res = a_Text.bytes;
            }
        }

        if (p_Callback != null)
        {
            p_Callback(a_Res);
        }
    }

/*
    public static UITexture TextureLoadAndInstantiate(string p_Path, bool p_bOriginSize = true)
    {
        //	背景の読み込み（非表示にしておく）
        Texture2D a_Tex = ResourceManager.TextureLoad(p_Path, false);
        GameObject a_Obj = new GameObject();
        UITexture a_Res = a_Obj.AddComponent<UITexture>();
        a_Res.mainTexture = a_Tex;

        if (p_bOriginSize)
        {
            UIWidget a_Widget = a_Res.GetComponent<UIWidget>();
            a_Widget.width = a_Tex.width;
            a_Widget.height = a_Tex.height;
        }

        return a_Res;
    }
*/

    /// <summary>
    /// テクスチャーをロードする
    /// </summary>
    /// <param name="p_FileNameWithoutExt"></param>
    /// <param name="p_Bundle"></param>
    /// <returns></returns>
    public static Texture2D TextureLoad(string p_FileNameWithoutExt, bool p_bEnableAssetBundle)
    {

        Texture2D a_Res = TextureLoad(p_FileNameWithoutExt, "png");
        if (a_Res == null)
        {
            a_Res = TextureLoad(p_FileNameWithoutExt, "jpg");
            if (a_Res == null)
            {
                a_Res = TextureLoad(p_FileNameWithoutExt, null);    // 内部テクスチャーフォーマット
            }
        }

        return a_Res;
    }

    /// <summary>
    /// テクスチャーを読み込む
    /// </summary>
    /// <param name="p_FileNamePathWithoutExt"></param>
    /// <param name="p_sFormat"></param>
    /// <returns></returns>
    public static Texture2D TextureLoad(string p_FileNamePathWithoutExt, string p_sFormat)
    {
        Texture2D a_Res = null;

        // ストリーミングアセットから読み込む
        if ((a_Res == null) && !string.IsNullOrEmpty(p_sFormat))
        {
            byte[] a_Data = File_LoadFromStreamingAssets(p_FileNamePathWithoutExt, p_sFormat);

            if (a_Data != null)
            {
                a_Res = new Texture2D(1, 1, TextureFormat.ARGB32, false);
                a_Res.LoadImage(a_Data);
            }

        }

        // リソースフォルダーから読み込む
        if (a_Res == null)
        {
            a_Res = Resources.Load(p_FileNamePathWithoutExt) as Texture2D;
        }

        if (a_Res == null)
        {
            Debug.LogError("ResourceManager.TextureLoad null = " + p_FileNamePathWithoutExt);
        }
        else
        {
            a_Res.filterMode = FilterMode.Bilinear;
            a_Res.wrapMode = TextureWrapMode.Clamp;
        }

        return a_Res;
    }

    // <OnTextureLoaded>
    public delegate void OnTexturePngLoaded(Texture2D p_Texture, int p_Tag);

    /// <summary>
    /// テクスチャーを非同期で読み込む
    /// </summary>
    /// <param name="p_FileNamePathWithoutExt"></param>
    /// <param name="p_sFormat"></param>
    /// <param name="p_Tag"></param>
    /// <param name="p_Callback"></param>
    public static void TextureLoadAsync(string p_FileNamePathWithoutExt, string p_sFormat, int p_Tag, OnTexturePngLoaded p_Callback, bool p_bEnableAssetBundle)
    {
        s_Instance.StartCoroutine(s_Instance.TextureLoadAsync_Exec(p_FileNamePathWithoutExt, p_sFormat, p_Tag, p_Callback, p_bEnableAssetBundle));
    }

    IEnumerator TextureLoadAsync_Exec(string p_FileNamePathWithoutExt, string p_sFormat, int p_Tag, OnTexturePngLoaded p_Callback, bool p_bEnableAssetBundle)
    {
        Texture2D a_Res = null;
        if ((a_Res == null) && !string.IsNullOrEmpty(p_sFormat))
        {
            // ストリーミングアセットから読み込む
            byte[] a_Data = File_LoadFromStreamingAssets(p_FileNamePathWithoutExt, p_sFormat);

            if (a_Data != null)
            {
                a_Res = new Texture2D(1, 1, TextureFormat.ARGB32, false);
                a_Res.LoadImage(a_Data);
                a_Res.filterMode = FilterMode.Bilinear;
                a_Res.wrapMode = TextureWrapMode.Clamp;
            }

        }

        // リソースフォルダーから読み込む
        if (a_Res == null)
        {
            a_Res = Resources.Load(p_FileNamePathWithoutExt) as Texture2D;
        }

        if (a_Res == null)
        {
            Debug.LogError("ResourceManaget.TextureLoad null" + p_FileNamePathWithoutExt);
        }

        // コールバックを呼ぶ
        p_Callback(a_Res, p_Tag);

        yield return 0;
    }

/*
    /// <summary>
    /// JSONデータを読み込む
    /// </summary>
    /// <param name="p_FilePath">拡張子無しのファイルパス</param>
    /// <param name="p_bEnableAssetBundle">アセットバンドルから読み込む場合はtrue</param>
    /// <returns></returns>
    public static JSONObject JSONLoad(string p_FilePath, bool p_bEnableAssetBundle = false)
    {
        JSONObject a_Res = null;

        string a_Text = TextLoad(p_FilePath, true, p_bEnableAssetBundle);
        if (!string.IsNullOrEmpty(a_Text))
        {
            a_Res = JSONObject.Create(a_Text);
        }

        return a_Res;
    }
*/

    /// <summary>
    /// テキストを読み込む
    /// </summary>
    /// <param name="p_FileNameWithoutExt"></param>
    /// <param name="p_bTrimComment">テキスト中のコメントを削除する場合はtrue</param>
    /// <returns></returns>
    public static string TextLoad(string p_FilePath, bool p_bTrimComment = false, bool p_bEnableAssetBundle = false)
    {
        string a_Res = null;
        if (a_Res == null)
        {
            // ストリーミングアセットから読み込む
            try
            {
                byte[] a_Data = File_LoadFromStreamingAssets(p_FilePath, "txt");
                if (a_Data != null)
                {
                    a_Res = System.Text.Encoding.UTF8.GetString(a_Data);
                }
            }
            catch
            {
                a_Res = null;
            }
        }

        if (a_Res == null)
        {
            // バンドル指定が無いときは内蔵リソースから読み込む
            TextAsset asset = (TextAsset)Resources.Load(def.c_TextAssetPath + p_FilePath, typeof(TextAsset));
            if (asset != null)
            {
                a_Res = asset.text;
            }
        }

        ///コメントを削除する
        if ((a_Res != null) && p_bTrimComment)
        {
            a_Res = String_TrimComment(a_Res);
        }

        return a_Res;
    }

    /// <summary>
    /// テキストを書き込む
    /// </summary>
    public static void TextWrite(string p_Path, string p_Text)
    {
        string fp = Path.Combine(Application.streamingAssetsPath, p_Path + "." + "txt");
//        FileInfo a_Info = new FileInfo(fp);
        StreamWriter sw = new StreamWriter(fp, true); //true=追記 false=上書き

        sw.WriteLine(p_Text);
        sw.Flush();
        sw.Close();
    }

    /// <summary>
    /// Audios the clip load.
    /// </summary>
    /// <returns>
    /// The clip load.
    /// </returns>
    /// <param name='p_FileName'>
    /// P_ file name.
    /// </param>
    /// <param name='p_Bundle'>
    /// AssetBundle
    /// </param>
    public static AudioClip AudioClipLoad(string p_FilePath, bool p_bEnableAssetBundle)
    {
        AudioClip a_AudioClip = null;

        if (a_AudioClip == null)
        {
            // StreamingAssetsから読み込む
            a_AudioClip = (AudioClip)Resources.Load(def.c_SoundAssetPath + p_FilePath, typeof(AudioClip));
        }

        if (a_AudioClip == null)
        {
            // リソースフォルダーから読み込む
            a_AudioClip = (AudioClip)Resources.Load(def.c_SoundAssetPath + p_FilePath, typeof(AudioClip));
        }

        return a_AudioClip;
    }


    /// <summary>
    /// 文字列のコメントを削除する（コメントは行毎に"//"で始まるもの）
    /// </summary>
    /// <param name="p_Str"></param>
    /// <returns></returns>
    public static string String_TrimComment(string p_Str)
    {
        string a_Res = null;

        string[] a_Split = p_Str.Split('\n');
        StringWriter a_SW = new StringWriter();

        foreach (string a_S in a_Split)
        {
            if (!a_S.Trim().StartsWith("//"))
            {
                a_SW.WriteLine(a_S);
            }
        }
        a_SW.WriteLine();

        a_Res = a_SW.ToString();

        return a_Res;

    }


    /// <summary>
    /// StreamingAssetsからファイルを読み込む
    /// </summary>
    /// <param name="p_FilePath"></param>
    /// <param name="p_FileExt"></param>
    /// <returns>byte配列データ</returns>
    public static byte[] File_LoadFromStreamingAssets(string p_FilePath, string p_FileExt)
    {
        byte[] a_Res = null;

#if UNITY_ANDROID && !UNITY_EDITOR

		string fp = System.IO.Path.Combine(Application.streamingAssetsPath, p_FilePath + "." + p_FileExt);

		if (!fp.Contains("://"))
		{
			fp = "file://" + fp;
		}

		WWW www = new WWW(fp);
		while(!www.isDone)
		{
			System.Threading.Thread.Sleep(1);
		}
		
//				_Debug.Log("GameTextureLoader Loaded = " + www.text);
		
		if (string.IsNullOrEmpty(www.error))
		{
			a_Res = www.bytes;
		}
#else
        string fp = System.IO.Path.Combine(Application.streamingAssetsPath, p_FilePath + "." + p_FileExt);
        //fp = "file://"+fp;
        if (fp.Contains("://"))
        {

            WWW www = new WWW(fp);
            a_Res = www.bytes;

        }
        else
        {

            try
            {
                a_Res = System.IO.File.ReadAllBytes(fp);
            }
            catch
            {
                a_Res = null;
            }

        }
#endif

        return a_Res;
    }



    /// <summary>
    /// （ダウンロードした）ファイルなどの一時的に保存するパスを取得する
    /// </summary>
    /// <returns></returns>
    public static string File_TempPath()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
		string path = Application.temporaryCachePath;
/*
		// 最新版では不要なはず 2015/7/4
		int ch = path.LastIndexOf('/');
		if (ch != -1)
		{
			path = path.Substring(0, ch);	// remove Document
		}
		path += "/Library/Caches";
*/
		return path;
#else
        return Application.temporaryCachePath;
#endif
    }


    /// <summary>
    /// （ダウンロードした）ファイルなどの永続的に保存するパスを取得する
    ///　大きいファイルの保存はNG
    /// </summary>
    /// <returns></returns>
    public static string File_DataStorePath()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
		string path = Application.persistentDataPath;
/*
		// 最新版では不要なはず 2015/7/4
		int ch = path.LastIndexOf('/');
		if (ch != -1)
		{
			path = path.Substring(0, ch);	// remove Document
		}
		path += "/Library/Caches";
*/
		return path;
#else
        return Application.persistentDataPath;
#endif
    }

    /// <summary>
    /// （ダウンロードした）ファイルなどの保存フルパスを取得する
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <param name="p_isTemp">一時保存パスの場合はtrue。default=true</param>
    /// <returns></returns>
    public static string File_FullPathMake(string p_FileName, bool p_isTemp = true)
    {
        string filePath;
        if (p_isTemp)
        {
            filePath = File_TempPath() + "/" + p_FileName;
        }
        else
        {
            filePath = File_DataStorePath() + "/" + p_FileName;
        }
        return filePath;
    }


    /// <summary>
    /// （ダウンロードした）ファイルが存在するかチェックする
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <param name="p_isTemp">一時保存パスの場合はtrue。default=true</param>
    public static bool File_Exists(string p_FileNames, bool p_isTemp = true)
    {
        string filePath = File_FullPathMake(p_FileNames, p_isTemp);

        bool fileExists = File.Exists(filePath);

        return fileExists;
    }

    /// <summary>
    /// （ダウンロードした）ファイルが存在するかチェックする
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <param name="p_isTemp">一時保存パスの場合はtrue。default=true</param>
    public static bool File_Exists(List<string> p_FileNames, bool p_isTemp = true)
    {
        bool fileExists = true;
        foreach (string a_FileName in p_FileNames)
        {
            if (!File_Exists(a_FileName, p_isTemp))
            {
                fileExists = false;
            }
        }

        return fileExists;
    }

    /// <summary>
    /// （ダウンロードした）ファイルを削除する
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <param name="p_isTemp">一時保存パスの場合はtrue。default=true</param>
    public static void File_Delete(string p_FileName, bool p_isTemp = true)
    {
        // TODO
    }

    /// <summary>
    /// （ダウンロードした）ファイルを削除する
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <param name="p_isTemp">一時保存パスの場合はtrue。default=true</param>
    public static void File_Deletes(List<string> p_FileName, bool p_isTemp = true)
    {
        foreach (string a_FileName in p_FileName)
        {
            File_Delete(a_FileName, p_isTemp);
        }
    }


    /// <summary>
    /// 任意のファイルを読み込む
    /// </summary>
    /// <param name="p_FileName">ファイル名</param>
    /// <param name="p_bCrypt">暗号化の有無</param>
    /// <param name="p_bIsTemp">一時ファイルならtrue</param>
    /// <returns>読み込んだデータ</returns>
    public static byte[] File_Load(string p_FileName, bool p_bCrypt = false, bool p_bIsTemp = true)
    {
        string a_FullPath = File_FullPathMake(p_FileName, p_bIsTemp);

        byte[] a_Res = null;

        try
        {
            FileStream fs = File.OpenRead(a_FullPath);

            a_Res = new byte[fs.Length];

            fs.Read(a_Res, 0, a_Res.Length);

            fs.Close();

        }
        catch (System.Exception ex)
        {
            a_Res = null;
            Debug.LogError(ex.ToString());
        }

        if (p_bCrypt && (a_Res != null))
        {
            a_Res = File_Decrypt(a_Res);
        }

        return a_Res;
    }


    /// <summary>
    /// 一時的なファイルを書き込む（消される可能性有り）
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <param name="p_Data"></param>
    /// <param name="p_bCrypt"></param>
    /// <param name="p_bIsTemp">一時ファイルの場合はtrue</param>
    /// <returns>ファイルのフルパス</returns>
    public static string File_Save(string p_FileName, byte[] p_Data, bool p_bCrypt = false, bool p_bIsTemp = true)
    {
        string a_FullPath = File_FullPathMake(p_FileName, p_bIsTemp);

        try
        {
//            DebugManager.Log("File_Save:" + a_FullPath);

            if (p_bCrypt)
            {
                p_Data = File_Encrypt(p_Data);
            }

            FileStream fs = File.Create(a_FullPath);
            fs.Write(p_Data, 0, p_Data.Length);
            fs.Close();
        }
        catch (System.Exception ex)
        {
            a_FullPath = null;
            Debug.LogError(ex.ToString());
        }

        return a_FullPath;
    }

    /// <summary>
    /// ファイルを書き込む（Editor用）
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <param name="p_Data"></param>
    /// <param name="p_bCrypt"></param>
    /// <returns></returns>
    public static bool File_SavePC(string p_FilePath, byte[] p_Data)
    {
        bool a_bRes = true;

        try
        {
//            DebugManager.Log("File_SavePC:" + p_FilePath);

            FileStream fs = File.Create(p_FilePath);
            fs.Write(p_Data, 0, p_Data.Length);
            fs.Close();
        }
        catch (System.Exception ex)
        {
            a_bRes = false;
            Debug.LogError(ex.ToString());
        }

        return a_bRes;
    }

    /// <summary>
    /// ファイルを読み込む（Editor用）
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <param name="p_bCrypted"></param>
    /// <returns></returns>
    public static byte[] File_LoadPC(string p_FilePath)
    {
        byte[] a_Res = null;

        try
        {
            FileStream fs = File.OpenRead(p_FilePath);

            a_Res = new byte[fs.Length];

            fs.Read(a_Res, 0, a_Res.Length);

            fs.Close();

        }
        catch (System.Exception ex)
        {
            a_Res = null;
            Debug.LogError(ex.ToString());
        }


        return a_Res;

    }


    /// <summary>
    /// ファイル暗号化時のキー
    /// </summary>
    static string c_FileDesKeyStr = "jR43YgTw0Toam+HDHYpTtx1PCkMuJGa0";
    static string c_FileDesIVStr = "OR3R4sN0eHk=";

    /// <summary>
    /// byte配列を暗号化する
    /// </summary>
    /// <param name="p_Data"></param>
    /// <returns></returns>
    public static byte[] File_Encrypt(byte[] p_Data)
    {

        byte[] cryptData = null;

        try
        {
            // Triple DES のサービス プロバイダを生成します 
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
#if false
			// 暗号化キーを生成する
			byte[] a_DesKey = des.Key;
			byte[] a_DesIV = des.IV;

			string a_FileDesKeyStr = System.Convert.ToBase64String(a_DesKey);
			string a_FileDesIVStr = System.Convert.ToBase64String(a_DesIV);

			TADebug.Log(eTAG, "DesKey="+a_FileDesKeyStr);
			TADebug.Log(eTAG, "DesIV ="+a_FileDesIVStr);
#endif
            byte[] a_DesKey = System.Convert.FromBase64String(c_FileDesKeyStr);
            byte[] a_DesIV = System.Convert.FromBase64String(c_FileDesIVStr);


            // 入出力用のストリームを生成します 
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(a_DesKey, a_DesIV), CryptoStreamMode.Write);

            // ストリームに暗号化するデータを書き込みます 
            cs.Write(p_Data, 0, p_Data.Length);
            cs.Close();

            // 暗号化されたデータを byte 配列で取得します 
            cryptData = ms.ToArray();
            ms.Close();
        }
        catch (System.Exception ex)
        {
            cryptData = null;
            Debug.LogError(ex.ToString());
        }

        return cryptData;
    }

    /// <summary>
    /// 暗号化されたbyte配列を復号化する
    /// </summary>
    /// <param name="p_CryptData"></param>
    /// <returns></returns>
    public static byte[] File_Decrypt(byte[] p_CryptData)
    {

        byte[] decryptData = null;

        try
        {
            // Triple DES のサービス プロバイダを生成します 
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();

            byte[] a_DesKey = System.Convert.FromBase64String(c_FileDesKeyStr);
            byte[] a_DesIV = System.Convert.FromBase64String(c_FileDesIVStr);


            // 入出力用のストリームを生成します 
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(a_DesKey, a_DesIV), CryptoStreamMode.Write);

            // ストリームに暗号化するデータを書き込みます 
            cs.Write(p_CryptData, 0, p_CryptData.Length);
            cs.Close();

            // 複合化されたデータを byte 配列で取得します 
            decryptData = ms.ToArray();
            ms.Close();
        }
        catch (System.Exception ex)
        {
            decryptData = null;
            Debug.LogError(ex.ToString());
        }

        return decryptData;
    }


#if false
	static bool isLock;
	/// <summary>
	/// PngTexture the load from streaming assets.
	/// </summary>
	/// <returns>
	/// The load from streaming assets.
	/// </returns>
	/// <param name='p_FileName'>
	/// P_ file name.
	/// </param>
	public static Texture2D PngTextureLoadFromStreamingAssets(string p_FileNameWithoutExt, AssetBundle p_Bundle = null)
	{
		// アセットバンドル指定がある場合
		if (p_Bundle != null)
		{
			Texture2D tex2d = (Texture2D)p_Bundle.LoadAsset(p_FileNameWithoutExt, typeof(Texture2D));
			return tex2d;
		}


#if UNITY_ANDROID && !UNITY_EDITOR

		string fp = System.IO.Path.Combine(Application.streamingAssetsPath, p_FileNameWithoutExt + ".png");
//fp = "file://"+fp;

		if (!fp.Contains("://"))
		{
			fp = "file://" + fp;
		}

		WWW www = new WWW(fp);
		while(!www.isDone)
		{
			System.Threading.Thread.Sleep(1);
		}
		
	//	_Debug.Log("GameTextureLoader Loaded = " + www.texture);
		
		return www.texture;
#else


		string fp = System.IO.Path.Combine(Application.streamingAssetsPath, p_FileNameWithoutExt + ".png");
//fp = "file://"+fp;
		if (fp.Contains("://"))
		{
		
			WWW www = new WWW(fp);

			Texture2D tex = www.texture;
			if (tex != null)
			{
				tex.Compress(true);
			}
			return www.texture;
			
		}
		else
		{
		
			byte[] bytes = System.IO.File.ReadAllBytes(fp);
			Texture2D tex = new Texture2D(4, 4, TextureFormat.ARGB32, false);
			tex.LoadImage(bytes);
//			tex.Compress(true);
//			tex.filterMode = FilterMode.Point;
			return tex;
		}
#endif
	}

	/// <summary>
	/// PngTexture the load from streaming assets.
	/// </summary>
	/// <returns>
	/// The load from streaming assets.
	/// </returns>
	/// <param name='p_FileName'>
	/// P_ file name.
	/// </param>
	public static void PngTextureLoadFromStreamingAssetsAsync(string p_FileNameWithoutExt, int p_nTag, LoadTextureAsync.OnTextureLoaded p_Delegate)
	{
		LoadTextureAsync.Create(p_FileNameWithoutExt + ".png", p_nTag, p_Delegate, p_Bundle);
	}				
#endif

/*
    static JSONObject s_StringTable;
    /// <summary>
    /// Strings the get.
    /// </summary>
    /// <returns>
    /// The get.
    /// </returns>
    /// <param name='p_Key'>
    /// P_ key.
    /// </param>
    public static string StringGet(string p_Key)
    {

        if (s_StringTable == null)
        {
            string a_FileName = def.c_StringTablePath_Jp;

            if (Localization.language == "UIString_en")
            {
                a_FileName = def.c_StringTablePath_En;
            }
            string jsonString = ResourceManager.TextLoad(a_FileName, false, false);

            // JSON Object
            s_StringTable = new JSONObject(jsonString);
        }

        if (s_StringTable != null)
        {
            string a_Res = s_StringTable[p_Key].str;
            return a_Res;
        }

        return null;

    }
*/
}
