﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class def : defSystem
{
    /// <summary>
    /// ポジション
    /// </summary>
    public enum ePosition
    {
        None = -1,

        Left,
        Right
    }

    /// <summary>
    /// 基準となる画面サイズ
    /// </summary>
    public static readonly Vector2 s_DefaultSize = new Vector2(1280, 720);

    /// <summary>
    /// 必ずシーン起動時の最初に呼ばれる処理
    /// </summary>
    public static void  Initialize()
    {
        //  Switch固有の初期化
        Switch_def.Initialize();

        //  デバッグ用レイアウトマネージャー
        DebugWindow.Create();

        //  UIマネージャー
        TemplateCanvas.Create();

        //  サウンドマネージャー
        SoundManager.Create();
    }
}
