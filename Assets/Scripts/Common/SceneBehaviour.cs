﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class SceneBehaviour<T> : MonoBehaviour where T : struct
{
    private Dictionary<string, T>   s_StateDic;

    protected string    eTAG
    {
        get
        {
            return  SceneManager.GetActiveScene().name;
        }
    }

    T   m_CurrentState;
    /// <summary>
    /// 現在のステート
    /// </summary>
    protected T currentState{ get { return m_CurrentState; } }

    T   m_LastState;
    /// <summary>
    /// 前回のステート
    /// </summary>
    protected T lastState { get { return m_LastState; } }

    private bool isEnum(Type p_Type)
    {
        if (!p_Type.IsEnum)
        {
            Debug.Log("SceneBehaviour <T> is not enum.");
            return false;
        }
        return true;
    }

    // Use this for initialization
    void Start ()
    {
        s_StateDic = new Dictionary<string, T>();

        m_CurrentState  = default(T);
        m_LastState     = default(T);

        if (!isEnum(typeof(T))) return;  

        Array   a_Values = Enum.GetValues(typeof(T));
        foreach(T a_Value in a_Values)
        {
            s_StateDic.Add(a_Value.ToString(), a_Value);
        }

        T   a_FirstState = default(T);
        Start_Exec(
            //  指定された初回に呼び出すステートに切り替える
            (p_FirstState)=>
            {
                if (!isEnum(p_FirstState.GetType()))    return;
                a_FirstState = p_FirstState;

                StateChange(a_FirstState);
            }
        );
	}

    /// <summary>
    /// 指定したステートに切り替える
    /// </summary>
    protected void StateChange(T p_State)
    {
        StartCoroutine(StateChange_Coroutine(p_State));
    }

    private IEnumerator StateChange_Coroutine(T p_State)
    {
        if (!isEnum(typeof(T)) || (m_CurrentState.ToString() == p_State.ToString())) yield break;

        //  「SendMessage()」が呼び出すまでに１フレーム掛かるので、次のStateChangeが呼ばれないように１フレーム待機
        yield return 0;

        string  a_Method = GetMethodName(p_State);

        m_LastState     = m_CurrentState;
        m_CurrentState  = p_State;

        SendMessage(a_Method, gameObject);

        Debug.Log(string.Format("{0}  lastState:{1}　currentState:{2}", eTAG, m_LastState, m_CurrentState));
    }

    /// <summary>
    /// 関数名を取得する
    /// </summary>
    private string  GetMethodName(T p_State)
    {
        if (!isEnum(typeof(T))) return "";

        StringBuilder   a_MethodName = new StringBuilder();
        a_MethodName.Append("State_").Append(p_State.ToString());

        return  a_MethodName.ToString();
    }

    protected abstract void Start_Exec(System.Action<T> p_BeginFirstState);

    /// <summary>
    /// シーンのロードを行う
    /// </summary>
    /// <param name="p_Scene"></param>
    /// <param name="p_LoadSceneMode"></param>
    protected void SceneChange(SceneEnum.eScene p_Scene, LoadSceneMode p_LoadSceneMode = LoadSceneMode.Single)
    {
        SceneManager.LoadScene(p_Scene.ToString(), p_LoadSceneMode);
    }
}
