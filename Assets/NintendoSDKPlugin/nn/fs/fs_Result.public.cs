﻿/*--------------------------------------------------------------------------------*
  Copyright (C)Nintendo All rights reserved.

  These coded instructions, statements, and computer programs contain proprietary
  information of Nintendo and/or its licensed developers and are protected by
  national and international copyright laws. They may not be disclosed to third
  parties or copied or duplicated in any form, in whole or in part, without the
  prior written consent of Nintendo.

  The content herein is highly confidential and should be handled accordingly.
 *--------------------------------------------------------------------------------*/

namespace nn.fs
{
    public static partial class FileSystem
    {
        public static readonly ErrorRange ResultHandledByAllProcess = new ErrorRange(2, 0, 1000);
        public static readonly Result ResultPathNotFound = new Result(2, 1);
        public static readonly Result ResultPathAlreadyExists = new Result(2, 2);
        public static readonly Result ResultTargetLocked = new Result(2, 7);
        public static readonly Result ResultDirectoryNotEmpty = new Result(2, 8);
        public static readonly Result ResultDirectoryStatusChanged = new Result(2, 13);
        public static readonly ErrorRange ResultUsableSpaceNotEnough = new ErrorRange(2, 30, 46);
        public static readonly Result ResultUnsupportedSdkVersion = new Result(2, 50);
        public static readonly Result ResultMountNameAlreadyExists = new Result(2, 60);
    }

    public static partial class SaveData
    {
        public static readonly Result ResultUsableSpaceNotEnoughForSaveData = new Result(2, 31);
    }
}
