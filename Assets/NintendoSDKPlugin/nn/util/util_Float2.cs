﻿using System;
using System.Runtime.InteropServices;

namespace nn.util
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Float2
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public void Set(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return String.Format("({0} {1})", this.x, this.y);
        }
    }
}
